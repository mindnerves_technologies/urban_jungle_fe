import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import AppRouters from './routes';
import { history } from './store';

const App = () => {
    return (
        <div className="App">
            <Router history={history}>
                <Switch>
                    <Route component={AppRouters} />
                </Switch>
            </Router>
        </div>
    );
};

export default App;

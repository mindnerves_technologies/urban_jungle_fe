import axios from 'axios';

const BACKEND_URL = process.env.REACT_APP_BACKEND_URL;

const axiosInstance = axios.create({
    baseURL: BACKEND_URL,
});

axiosInstance.interceptors.request.use(
    (config) => {
        // Do something before request is sent

        config.headers.Authorization = 'Bearer xtbrnuh7qt4wbm3vrgvuy9mk8zfttmlk';
        return config;
    },
    (err) => {
        let errorMsg = 'Unknown error';
        if (err.response && err.response.data && err.response.data.error) {
            errorMsg = err.response.data.error;
        }
        Promise.reject(errorMsg);
    },
);

export default axiosInstance;

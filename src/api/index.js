import api from './apiServices';

//Guest card
const getGuestCard = (payload) => api.post(`/rest/V1/guest-carts`, payload);
//End Guest card

//Home and Menu
const getMenuList = (payload) => api.post(`/rest/V1/app/menu`, payload);
const getHomepage = (payload) => api.post(`/rest/V1/app/home`, payload);
const getNewArriwal = (payload) => api.get(`/rest/V1/app/productsbycategory?${payload}`); //category_id=309&store_id=4&count=6
//End Home and Menu

//Login
const ecomLogin = (payload) => api.post(`/rest/V1/app/login`, payload);
const socialGoogleLogin = (payload) => api.post(`/rest/V1/app/sociallogingoogle`, payload); //google_login
const socialFacebookLogin = (payload) => api.post(`/rest/V1/app/socialloginfb`, payload); //facebook_login
//End Login

//register
const registerUser = (payload) => api.post(`/rest/V1/app/register`, payload);
//end regsiter

//static pages
const getContactUs = (store_id) => api.get(`rest/V1/app/contactus?storeId=${store_id}`);
const getCMSPageData = (payload) => api.post(`/rest/V1/app/cmspage`, payload);
const getFaqData = (store_id) => api.get(`/rest/V1/app/customerservice?store_id=${store_id}`);
const getPrivacy = (store_id) =>
    api.get(`rest/V1/cmsPageIdentifier/privacy-policy/storeId/${store_id}?storeId=${store_id}`);
const getAboutUs = (store_id) =>
    api.get(`rest/V1/cmsPageIdentifier/about/storeId/${store_id}?storeId=${store_id}`);
//end statis pages

//PDP
const getPDPData = (payload) => api.post(`/rest/V1/app/productbyid`, payload);
const addToCartGuestReq = (payload, guestId) => api.post(`/rest/V1/guest-carts/${guestId}/items`, payload);
const checkPDPOrPLPUrl = (payload) => api.post(`/rest/V1/app/getcatalogurl`, payload);
const addToCartLoginReq = (payload) => api.post(`/rest/V1/carts/mine/items`, payload);
//End PDP

//PLP
const getProducts = (payload) => api.post('/index.php/rest/V1/app/productlisting/', payload);
const getPLPSearch = (payload) => api.post(`/index.php/rest/V1/app/searchresult`, payload);
//End PLP

const getAutoSearch = (payload) => api.post(`/index.php/rest/V1/app/searchautosuggest`, payload);

// Brands
const getBrands = (payload) => api.post(`/index.php/rest/V1/app/getbrands`, payload);
//Brand end

const APIList = {
    getMenuList,
    getHomepage,
    getNewArriwal,
    ecomLogin,
    registerUser,
    socialGoogleLogin,
    socialFacebookLogin,
    getContactUs,
    getCMSPageData,
    getFaqData,
    getPrivacy,
    getAboutUs,
    getPDPData,
    getGuestCard,
    addToCartGuestReq,
    checkPDPOrPLPUrl,
    getProducts,
    getPLPSearch,
    getAutoSearch,
    addToCartLoginReq,
    getBrands,
};

export default APIList;

import SimpleBreadcrumbs from "components/common/Breadcrumbs";
import { useEffect, useState } from "react";
import { BsSearch } from "react-icons/bs";
import { FormattedMessage } from "react-intl";
import { useDispatch, useSelector } from "react-redux";
import { actions as brandActions } from "./redux/actions";
import './brands.css';
import InputBase from '@material-ui/core/InputBase';
// import SearchIcon from '@material-ui/icons/Search';
import { fade, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
    border: '1px solid black',
  },
  inputInput: {

    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '100%',
    },
  },
  
}));

function Brands() {

    const classes = useStyles();

    const [search, setSearch] = useState('');
    const dispatch = useDispatch();
    const [searchReasult, setSearchReasult] = useState([]);

    const store_id = useSelector((state) => state && state.storeInfo && state.storeInfo.store_id);
    const store_code = useSelector((state) => state && state.storeInfo && state.storeInfo.store_code);
    const allBrands = useSelector((state) => state && state.brand && state.brand.brands);
    const brandKey = [];
    const brandList = [];

    for(const i in allBrands){
        
         brandKey.push(i);
    
    }
    console.log(brandKey);
    console.log(brandList);

    const breadCrumbs = [
        {
            name: <FormattedMessage id="Common.Home.Text" defaultMessage="Home" />,
            url: `/${store_code}/`,
        },

        {
            name: 'Brands',
        },
    ];         

    useEffect(() => {
        dispatch(
            brandActions.getAllBrandsRequest({
                store_id: store_id,
            }),
        );
    }, [dispatch, store_id]);

    
     
    const alphabets = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

    const autoSearchText = async(e) => {
        
        e.preventDefault();
        await setSearch(e.target.value);     
        console.log(search)
        const filtered = await brandList.filter((item) => {
            return (item.toLowerCase()).includes(search.toLowerCase());
        });

        await setSearchReasult(filtered);  
        console.log('filtered' + filtered) ;
        console.log('searresult' + searchReasult)
        
    }

    // useEffect(async() => {
       
    // }, [search])


    return (
        <div className='brand-container'>
            <div className='breadcrumbs'>
                <SimpleBreadcrumbs breadCrumbs={breadCrumbs} />
            </div>

            <h2 className='main-heading'>Brands</h2>
            <div className='search-container'>

               <div className={classes.search}>
            <div className={classes.searchIcon}>
              <BsSearch />
            </div>
            <InputBase
              placeholder="Search for Brand" 
              value={search} 
              onChange={autoSearchText} 
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }} 
              inputProps={{ 'aria-label': 'search' }}
            />
          </div>
            </div>                
            <div className='alphabets'>
                {
                  alphabets.map(alphabet => {
                  
                      if(brandKey.includes(alphabet)){
                          return(
                               <span style={{margin:"10px"}}>{alphabet}</span>)
                  }else{
                      return(
                            <span style={{margin:"10px",color:"#a1a1a1"}}>{alphabet}</span>
             )     }
                  
                  }
                  )
                }
            </div>

            <div className='brand-names'>
                
                    {!search && allBrands && Object.keys(allBrands).map((item, index) =>   

                           (
                               
                               <div style={{width:'100%'}}>
                               <hr></hr>
                               <h1>{item}</h1>
                               <div className='brand-items'>
                               {allBrands[item].map((brand, index) => {
                                   
                                    brandList.push(brand.title);
                                    
                                   for(const i in brand)
                                   {return <div className='brand-s'> {brand.title} </div>}
                               })}
                               </div>
                               </div>
                           )                     
                    
                    ) }

                    {
                    search && searchReasult && 
                        searchReasult.map(result => {return (<div className='brand-items'><div className='brand-s'>{result}</div></div>)})
                    //   brandList.map(item => {
                    //       if(search !== "" && item.title.indexOf(search) === -1){
                    //           return null;
                    //       }else{
                    //           return (<p>{item.title}</p>);
                    //       }
                    //   })

                    }
                
            </div>
            
        </div>
    )
}

export default Brands

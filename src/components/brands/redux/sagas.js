import { call, put, takeLatest } from 'redux-saga/effects';
import { actions, types } from './actions';
import api from '../../../api';

const getAllBrandsRequest = function* getAllBrandsRequest({ payload }) {
    try {
        const { store_id } = payload;
        const { data } = yield call(api.getBrands, {store_id});
        console.log(`$$$$$$$$$$$$ ${JSON.stringify(data)}`)
        if (data && data.status) {
            // yield put(actions.getAllBrandsRequest(data.data.A));
            yield put(actions.getAllBrandsRequestSuccess(data && data.data));
        } else{
            yield put(actions.getAllBrandsRequestFailed());
        }
    } catch (err) {
        yield put(actions.getAllBrandsRequestFailed(err));
    }
}

export default function* sagas() {
    yield takeLatest(types.GET_ALL_BRANDS_REQUEST, getAllBrandsRequest)
}
import React from 'react';
import Grid from '@material-ui/core/Grid';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import { MdKeyboardArrowRight } from 'react-icons/md';

export default function SimpleBreadcrumbs({ breadCrumbs }) {
    return (
        <Grid container>
            <Grid item xs={12} className="breadcrumbs">
                <Breadcrumbs separator={<MdKeyboardArrowRight />}>
                    {breadCrumbs &&
                        breadCrumbs.map((breadCrum, index) => {
                            if (breadCrum.url) {
                                return (
                                    <Link to={breadCrum.url} key={`breadCrumbs_${index}`}>
                                        {breadCrum.name}
                                    </Link>
                                );
                            }
                            return (
                                <Typography color="textPrimary" key={`breadCrumbs_${index}`}>
                                    {breadCrum.name}
                                </Typography>
                            );
                        })}
                </Breadcrumbs>
            </Grid>
        </Grid>
    );
}

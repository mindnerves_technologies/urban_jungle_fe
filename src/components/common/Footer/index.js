import React, { useState } from 'react';
import { Grid, Collapse, Typography, FormGroup, Checkbox, Hidden } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { MdExpandMore, MdExpandLess } from 'react-icons/md';
import { FaFacebookF, FaInstagram } from 'react-icons/fa';
import { useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import Icon_Payments from '../../../assets/footer/Icon_Payments.svg';

const WhiteCheckbox = withStyles({
    root: {
        color: '#fff',
    },
    checked: {},
})((props) => <Checkbox color="default" {...props} />);

const Footer = () => {
    const history = useHistory();

    const [dropDownValue, setDropDownValue] = React.useState({
        companies: false,
        legalNotes: false,
        categories: false,
    });
    const [firstCheckbox, setFirstCheckbox] = useState(false);
    const [secondCheckbox, setSecondCheckbox] = useState(false);

    const footerSection = useSelector(
        (state) => state && state.menu && state.menu.menuList && state.menu.menuList.footer_section,
    );
    const store_code = useSelector((state) => state && state.storeInfo && state.storeInfo.store_code);

    const redirectToStaticPage = (title) => {
        if (title === 'Terms & Coditions') {
            history.push(`/${store_code}/terms-and-conditions`);
        }

        if (title === 'Shipping & Returns') {
            return history.push(`/${store_code}/shipping-and-return`);
        }

        if (title === 'Cookies Policy') {
            return history.push(`/${store_code}/cookie-policy`);
        }

        if (title === 'FAQ') {
            return history.push(`/${store_code}/faq`);
        }

        if (title.trim() === 'Privacy Policy') {
            return history.push(`/${store_code}/privacy`);
        }
    };

    return (
        <>
            <Hidden only={['xs', 'sm']}>
                <div className="footerContainer">
                    <Grid container spacing={2} justify="space-between">
                        {footerSection && footerSection.categories_blocks && (
                            <Grid item xs={2}>
                                <Typography variant="h6" className="footerTitleText">
                                    {footerSection.categories_blocks &&
                                        footerSection.categories_blocks.categories_header &&
                                        footerSection.categories_blocks.categories_header.categories_header}
                                </Typography>
                                <div>
                                    <List dense={true}>
                                        {footerSection.categories_blocks &&
                                            footerSection.categories_blocks &&
                                            footerSection.categories_blocks.categories_links.map((categori, index) => (
                                                <Link
                                                    to={`${categori.categories_link}`}
                                                    style={{ textDecoration: 'none' }}
                                                >
                                                    <ListItem key={`categori${index}`} disableGutters={true}>
                                                        <ListItemText
                                                            className="listItemText"
                                                            primary={categori.categories_title}
                                                        />
                                                    </ListItem>
                                                </Link>
                                            ))}
                                    </List>
                                </div>
                            </Grid>
                        )}

                        {footerSection && footerSection.company_blocks && (
                            <Grid item xs={2}>
                                <Typography variant="h5" className="footerTitleText">
                                    {footerSection.company_blocks &&
                                        footerSection.company_blocks.company_header &&
                                        footerSection.company_blocks.company_header.company_header}
                                </Typography>
                                <div>
                                    <List dense={true}>
                                        {footerSection.company_blocks &&
                                            footerSection.company_blocks.company_links.map((company, index) => (
                                                <>
                                                    {company.company_link && !company.company_link.includes('/blog') && (
                                                        <Link
                                                            to={`${company.company_link}`}
                                                            style={{ textDecoration: 'none' }}
                                                        >
                                                            <ListItem key={`company${index}`} disableGutters={true}>
                                                                <ListItemText
                                                                    className="listItemText"
                                                                    primary={company.company_title}
                                                                />
                                                            </ListItem>
                                                        </Link>
                                                    )}
                                                    {company.company_link && company.company_link.includes('/blog') && (
                                                        <a
                                                            href={`https://www.urbanjunglestore.com/blog/${store_code}/`}
                                                            style={{ textDecoration: 'none' }}
                                                            target="_blank"
                                                            rel="noreferrer"
                                                        >
                                                            <ListItem key={`company${index}`} disableGutters={true}>
                                                                <ListItemText
                                                                    className="listItemText"
                                                                    primary={company.company_title}
                                                                />
                                                            </ListItem>
                                                        </a>
                                                    )}
                                                </>
                                            ))}
                                    </List>
                                </div>
                            </Grid>
                        )}
                        {footerSection && footerSection.legal_blocks && (
                            <Grid item xs={2}>
                                <Typography variant="h5" className="footerTitleText">
                                    {footerSection.legal_blocks &&
                                        footerSection.legal_blocks.legal_header &&
                                        footerSection.legal_blocks.legal_header.legal_header}
                                </Typography>
                                <div>
                                    <List dense={true}>
                                        {footerSection.legal_blocks &&
                                            footerSection.legal_blocks.legal_links.map((legal, index) => (
                                                <Link to={`${legal.legal_link}`} style={{ textDecoration: 'none' }}>
                                                    <ListItem key={`legal${index}`} disableGutters={true}>
                                                        {' '}
                                                        <ListItemText
                                                            className="listItemText"
                                                            primary={legal.legal_title}
                                                        />
                                                    </ListItem>
                                                </Link>
                                            ))}
                                    </List>
                                </div>
                            </Grid>
                        )}
                        <Grid item xs={2}>
                            <Typography variant="h5" className="footerTitleText">
                                Follow us on
                            </Typography>
                            <div className="logoContain">
                                <div className="logoImg">
                                    <a
                                        href="https://www.facebook.com/urbanjunglestore/"
                                        target="_blank"
                                        rel="noreferrer"
                                    >
                                        <FaFacebookF style={{ color: '#fff' }} />
                                    </a>
                                </div>
                                <div className="logoImg">
                                    <a
                                        href="https://www.instagram.com/urban_jungle_official/"
                                        target="_blank"
                                        rel="noreferrer"
                                    >
                                        <FaInstagram style={{ color: '#fff' }} />
                                    </a>
                                </div>
                            </div>
                        </Grid>
                        <Grid item xs={4}>
                            <Typography variant="h5" className="footerOfferNExclusive">
                                Receive offers and exclusives
                            </Typography>
                            <div>
                                <div className="pt-15">
                                    <input className="emailInput" placeholder="Your Email" style={{ minHeight: 40 }} />
                                    <button className="subscribeBtn" style={{ minHeight: 40 }}>
                                        Subscribe
                                    </button>
                                </div>
                                <div>
                                    <FormGroup>
                                        <Grid container alignItems="center">
                                            <WhiteCheckbox
                                                size="small"
                                                name="checkedB"
                                                checked={firstCheckbox}
                                                style={{ paddingLeft: 0 }}
                                                onChange={(e) => setFirstCheckbox(e.target.checked)}
                                            />
                                            <p className="labelText">
                                                I agree to the{' '}
                                                <span
                                                    style={{ cursor: 'pointer' }}
                                                    onClick={() => redirectToStaticPage('Privacy Policy')}
                                                >
                                                    Privacy Policy
                                                </span>
                                                .
                                            </p>
                                        </Grid>
                                        <Grid container alignItems="center">
                                            <WhiteCheckbox
                                                size="small"
                                                name="checkedB"
                                                style={{ paddingLeft: 0 }}
                                                checked={secondCheckbox}
                                                onChange={(e) => setSecondCheckbox(e.target.checked)}
                                            />
                                            <p className="labelText" style={{ width: '90%' }}>
                                                Consent to the processing of personal data for marketing purposes{' '}
                                                <span
                                                    onClick={() => redirectToStaticPage('Privacy Policy')}
                                                    style={{ cursor: 'pointer' }}
                                                >
                                                    Privacy Policy
                                                </span>
                                                .
                                            </p>
                                        </Grid>
                                    </FormGroup>
                                </div>
                            </div>
                        </Grid>
                    </Grid>

                    <div className="horizontalLine" />

                    <div>
                        <Typography variant="body2" className="descriptionText">
                            Urban jungle is brand owned by time international sport ltd and managed by italian
                            operations limited srl, both companies of the hudson holdings group. Expertise and passion
                            are the spirit of these projects, an enormously valuable resource based on over 30 years of
                            international experience in distribution and retail. <span>Know More</span>
                        </Typography>
                    </div>
                    <Grid container justify="space-between" alignItems="center" className="footer-lastSection">
                        <label>© 2020 - Urban Jungle | Italian Operations Limited srl</label>
                        <label className="sitmap">Sitemap</label>
                        <div className="image">
                            <img src={Icon_Payments} alt="Icon_Payments" />
                        </div>
                    </Grid>
                </div>
            </Hidden>

            <Hidden only={['md', 'lg', 'xl']}>
                <div className="footerContainerMobi">
                    <div>
                        <Typography variant="body2" className="descriptionText">
                            Urban jungle is brand owned by time international sport ltd and managed by italian
                            operations limited srl, both companies of the hudson holdings group. Expertise and passion
                            are the spirit of these projects, an enormously valuable resource based on over 30 years of
                            international experience in distribution and retail. <span>Know More</span>
                        </Typography>
                    </div>

                    <div className="horizontalLine" />
                    <div>
                        <List
                            component="nav"
                            aria-labelledby="nested-list-subheader"
                            // subheader={
                            //     <ListSubheader component="div" id="nested-list-subheader">
                            //         Nested List Items
                            //     </ListSubheader>
                            // }
                            // className={classes.root}
                        >
                            <ListItem
                                disableGutters={true}
                                button
                                onClick={() =>
                                    setDropDownValue({ ...dropDownValue, companies: !dropDownValue.companies })
                                }
                            >
                                <ListItemText className="mobi-list-text" primary="COMPANIES" />
                                {dropDownValue.companies ? (
                                    <MdExpandLess className="mobi-list-expandIcon" />
                                ) : (
                                    <MdExpandMore className="mobi-list-expandIcon" />
                                )}
                            </ListItem>
                            <Collapse in={dropDownValue.companies} timeout="auto" unmountOnExit>
                                <List component="div" disablePadding>
                                    {/* <ListItem button>
                                        
                                        <ListItemText primary="Starred" />
                                    </ListItem> */}
                                    <div>
                                        <List dense={true}>
                                            {footerSection &&
                                                footerSection.company_blocks &&
                                                footerSection.company_blocks.company_links.map((company, index) => (
                                                    <>
                                                        {company.company_link &&
                                                            !company.company_link.includes('/blog') && (
                                                                <Link
                                                                    to={`${company.company_link}`}
                                                                    style={{ textDecoration: 'none' }}
                                                                >
                                                                    <ListItem
                                                                        key={`company${index}`}
                                                                        disableGutters={true}
                                                                    >
                                                                        <ListItemText
                                                                            className="listItemText"
                                                                            primary={company.company_title}
                                                                        />
                                                                    </ListItem>
                                                                </Link>
                                                            )}

                                                        {company.company_link &&
                                                            company.company_link.includes('/blog') && (
                                                                <a
                                                                    href={`https://www.urbanjunglestore.com/blog/${store_code}/`}
                                                                    style={{ textDecoration: 'none' }}
                                                                    target="_blank"
                                                                    rel="noreferrer"
                                                                >
                                                                    <ListItem
                                                                        key={`company${index}`}
                                                                        disableGutters={true}
                                                                    >
                                                                        <ListItemText
                                                                            className="listItemText"
                                                                            primary={company.company_title}
                                                                        />
                                                                    </ListItem>
                                                                </a>
                                                            )}
                                                    </>
                                                ))}
                                        </List>
                                    </div>
                                </List>
                            </Collapse>

                            <ListItem
                                disableGutters={true}
                                button
                                onClick={() =>
                                    setDropDownValue({ ...dropDownValue, legalNotes: !dropDownValue.legalNotes })
                                }
                            >
                                <ListItemText className="mobi-list-text" primary="LEGAL NOTES" />
                                {dropDownValue.legalNotes ? (
                                    <MdExpandLess className="mobi-list-expandIcon" />
                                ) : (
                                    <MdExpandMore className="mobi-list-expandIcon" />
                                )}
                            </ListItem>
                            <Collapse in={dropDownValue.legalNotes} timeout="auto" unmountOnExit>
                                <List component="div" disablePadding>
                                    {/* <ListItem button>
                                        
                                        <ListItemText primary="Starred" />
                                    </ListItem> */}
                                    <div>
                                        <List dense={true}>
                                            {footerSection &&
                                                footerSection.legal_blocks &&
                                                footerSection.legal_blocks.legal_links.map((legal, index) => (
                                                    <Link to={`${legal.legal_link}`} style={{ textDecoration: 'none' }}>
                                                        <ListItem key={`legal${index}`} disableGutters={true}>
                                                            {' '}
                                                            <ListItemText
                                                                className="listItemText"
                                                                primary={legal.legal_title}
                                                            />
                                                        </ListItem>
                                                    </Link>
                                                ))}
                                        </List>
                                    </div>
                                </List>
                            </Collapse>

                            <ListItem
                                disableGutters={true}
                                button
                                onClick={() =>
                                    setDropDownValue({ ...dropDownValue, categories: !dropDownValue.categories })
                                }
                            >
                                <ListItemText className="mobi-list-text" primary="CATEGORIES" />
                                {dropDownValue.categories ? (
                                    <MdExpandLess className="mobi-list-expandIcon" />
                                ) : (
                                    <MdExpandMore className="mobi-list-expandIcon" />
                                )}
                            </ListItem>
                            <Collapse in={dropDownValue.categories} timeout="auto" unmountOnExit>
                                <List component="div" disablePadding>
                                    {/* <ListItem button>
                                        
                                        <ListItemText primary="Starred" />
                                    </ListItem> */}
                                    <div>
                                        <List dense={true}>
                                            {footerSection &&
                                                footerSection.categories_blocks &&
                                                footerSection.categories_blocks &&
                                                footerSection.categories_blocks.categories_links.map(
                                                    (categori, index) => (
                                                        <Link
                                                            to={`${categori.categories_link}`}
                                                            style={{ textDecoration: 'none' }}
                                                        >
                                                            <ListItem key={`categori${index}`} disableGutters={true}>
                                                                <ListItemText
                                                                    className="listItemText"
                                                                    primary={categori.categories_title}
                                                                />
                                                            </ListItem>
                                                        </Link>
                                                    ),
                                                )}
                                        </List>
                                    </div>
                                </List>
                            </Collapse>

                            <ListItem disableGutters={true} button>
                                <ListItemText className="mobi-list-sitemap" primary="SITEMAP" />
                            </ListItem>
                        </List>
                    </div>
                    <div className="horizontalLine" />

                    <div>
                        <Typography variant="h5" className="footerOfferNExclusive">
                            Receive offers and exclusives
                        </Typography>
                        <div>
                            <div className="pt-15">
                                <input className="emailInput" placeholder="Your Email" style={{ minHeight: 40 }} />
                                <button className="subscribeBtn" style={{ minHeight: 40 }}>
                                    Subscribe
                                </button>
                            </div>
                            <div>
                                <FormGroup>
                                    {/* <FormControlLabel
                                        control={
                                            <WhiteCheckbox
                                                size="small"
                                                name="checkedA"
                                                checked={firstCheckbox}
                                                onChange={(e) => setFirstCheckbox(e.target.checked)}
                                            />
                                        }
                                        label={
                                            <p className="labelText">
                                                I agree to the{' '}
                                                <span onClick={() => redirectToStaticPage('Privacy Policy')}>
                                                    Privacy Policy
                                                </span>
                                                .
                                            </p>
                                        }
                                    />

                                    <FormControlLabel
                                        control={
                                            <WhiteCheckbox
                                                size="small"
                                                name="checkedB"
                                                checked={secondCheckbox}
                                                onChange={(e) => setSecondCheckbox(e.target.checked)}
                                            />
                                        }
                                        label={
                                            <p className="labelText">
                                                Consent to the processing of personal data for marketing purposes{' '}
                                                <span onClick={() => redirectToStaticPage('Privacy Policy')}>
                                                    Privacy Policy
                                                </span>
                                                .
                                            </p>
                                        }
                                    /> */}
                                    <Grid container alignItems="center">
                                        <WhiteCheckbox
                                            size="small"
                                            name="checkedB"
                                            checked={firstCheckbox}
                                            style={{ paddingLeft: 0 }}
                                            onChange={(e) => setFirstCheckbox(e.target.checked)}
                                        />
                                        <p className="labelText">
                                            I agree to the{' '}
                                            <span
                                                style={{ cursor: 'pointer' }}
                                                onClick={() => redirectToStaticPage('Privacy Policy')}
                                            >
                                                Privacy Policy
                                            </span>
                                            .
                                        </p>
                                    </Grid>
                                    <Grid container alignItems="center">
                                        <WhiteCheckbox
                                            size="small"
                                            name="checkedB"
                                            style={{ paddingLeft: 0 }}
                                            checked={secondCheckbox}
                                            onChange={(e) => setSecondCheckbox(e.target.checked)}
                                        />
                                        <p className="labelText" style={{ width: '90%' }}>
                                            Consent to the processing of personal data for marketing purposes{' '}
                                            <span
                                                onClick={() => redirectToStaticPage('Privacy Policy')}
                                                style={{ cursor: 'pointer' }}
                                            >
                                                Privacy Policy
                                            </span>
                                            .
                                        </p>
                                    </Grid>
                                </FormGroup>
                            </div>
                        </div>
                    </div>
                </div>
            </Hidden>
        </>
    );
};
export default Footer;

import React from 'react';
import LazyLoad from 'react-lazyload';

const LazyImages = ({
    src,
    alt = 'Product Image',
    srcSet = '',
    ImageClassName = '',
    containerClassName = '',
    once = false,
}) => {
    return (
        <>
            <LazyLoad offset={100} className={containerClassName} once={once}>
                <img src={src} alt={alt} srcset={srcSet} className={ImageClassName} />
            </LazyLoad>
        </>
    );
};

export default LazyImages;

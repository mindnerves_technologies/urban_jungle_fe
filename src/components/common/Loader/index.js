import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

const Loader = () => {
    return (
        <div className="loader">
            <CircularProgress size={100} />
        </div>
    );
};

export default Loader;

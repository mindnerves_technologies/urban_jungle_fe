import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { IoCloseCircle } from 'react-icons/io5';
import { MdEmail } from 'react-icons/md';
import { HiPhone, HiOutlineShoppingBag } from 'react-icons/hi';
import { RiMessage2Fill } from 'react-icons/ri';
import { FaShippingFast } from 'react-icons/fa';
import { FiChevronRight } from 'react-icons/fi';
import { GrCopy } from 'react-icons/gr';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';

const useStyles = makeStyles((theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(2),
    },
    title: {
        fontSize: '1rem',
        fontWeight: '700',
        textAlign: 'center',
    },
    container: {
        padding: '10px',
    },
    closeButton: {
        position: 'absolute',
        right: 0,
        top: 0,
        color: '#000',
    },

    horizontalLine: {
        borderBottom: '1px solid #000',
        width: '95%',
        position: 'relative',
        margin: 'auto',
    },
    dFlex: {
        display: 'flex',
        padding: '10px 0px',
        alignItems: 'flex-end',
    },
    subText: {
        fontSize: '0.6rem',
    },
    optionIcons: {
        fontSize: '20px',
    },
    icons: {
        fontSize: '16px',
    },
    emailText: {
        fontSize: '0.7rem',
        fontWeight: 700,
        textDecoration: 'underline',
    },
    number: {
        fontSize: '0.7rem',
        fontWeight: 700,
    },
    padd_left: {
        paddingLeft: 8,
    },
    optionsTitle: {
        fontSize: '1rem',
        textAlign: 'center',
        fontWeight: 600,
    },
    optionBox: {
        border: '1px solid lightgray',
        padding: '0px 10px 0px 15px',
        borderRadius: '8px',
        display: 'flex',
        justifyContent: 'space-between',
        marginTop: 10,
        cursor: 'pointer',
    },
    optionText: {
        fontSize: '0.7rem',
        paddingLeft: '15px',
        marginBottom: 0,
    },
}));

export default function AssistancePopup({ isOpen, setIsAssistanceOpen }) {
    const classes = useStyles();
    const history = useHistory();

    const store_code = useSelector((state) => state && state.storeInfo && state.storeInfo.store_code);
    const contactus = useSelector((state) => state && state.staticData && state.staticData.contactus);

    const handleClose = () => {
        setIsAssistanceOpen(false);
    };

    const redirectToTermsAndCondition = () => {
        handleClose();
        return history.push(`/${store_code}/terms-and-conditions`);
    };

    const redirectToShippingAndReturn = () => {
        handleClose();
        return history.push(`/${store_code}/shipping-and-return`);
    };

    return (
        <>
            <Dialog
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={isOpen}
                className="country_change_pop"
            >
                <div id="customized-dialog-title" className={classes.root} onClose={handleClose}>
                    <Typography className={classes.title} variant="h4">
                        Assistance
                    </Typography>

                    <IconButton aria-label="close" className={classes.closeButton} onClick={handleClose}>
                        <IoCloseCircle />
                    </IconButton>
                </div>

                <div className={classes.container}>
                    <div className={classes.horizontalLine} />

                    <div className={classes.root}>
                        <div className={classes.dFlex}>
                            <div>
                                <MdEmail className={classes.icons} />
                            </div>
                            <div className={classes.padd_left}>
                                <Typography className={classes.subText} gutterBottom>
                                    Have questions & suggestions?
                                </Typography>
                                <Typography className={classes.emailText} gutterBottom>
                                    <a href={`mailto:${contactus.contact_email}`} style={{ color: '#000' }}>
                                        {contactus.contact_email}
                                    </a>
                                </Typography>
                            </div>
                        </div>
                        <div className={classes.dFlex}>
                            <div>
                                <HiPhone className={classes.icons} />
                            </div>
                            <div className={classes.padd_left}>
                                <Typography className={classes.subText} gutterBottom>
                                    Need Assistance? Give us a call
                                </Typography>
                                <Typography className={classes.number} gutterBottom>
                                    {contactus.contactnumber_it}
                                </Typography>
                            </div>
                        </div>

                        <div className={classes.dFlex}>
                            <div>
                                <RiMessage2Fill className={classes.icons} />
                            </div>
                            <div className={classes.padd_left}>
                                <Typography className={classes.emailText} gutterBottom>
                                    Live Chat
                                </Typography>
                            </div>
                        </div>
                    </div>
                    <div className={classes.horizontalLine} />

                    <div className={classes.root}>
                        <Typography className={classes.optionsTitle} variant="h6" gutterBottom>
                            What do you wish to contact us about?
                        </Typography>

                        <div className={classes.optionBox} onClick={() => redirectToShippingAndReturn()}>
                            <div className={classes.dFlex}>
                                <FaShippingFast className={classes.optionIcons} />
                                <Typography className={classes.optionText} gutterBottom>
                                    {' '}
                                    SHIPPING & RETURNS
                                </Typography>
                            </div>

                            <div className={classes.dFlex}>
                                <FiChevronRight className={classes.optionIcons} />
                            </div>
                        </div>

                        <div className={classes.optionBox}>
                            <div className={classes.dFlex}>
                                <HiOutlineShoppingBag className={classes.optionIcons} />
                                <Typography className={classes.optionText} gutterBottom>
                                    {' '}
                                    MY ORDERS
                                </Typography>
                            </div>

                            <div className={classes.dFlex}>
                                <FiChevronRight className={classes.optionIcons} />
                            </div>
                        </div>

                        <div className={classes.optionBox} onClick={() => redirectToTermsAndCondition()}>
                            <div className={classes.dFlex}>
                                <GrCopy className={classes.optionIcons} />
                                <Typography className={classes.optionText} gutterBottom>
                                    {' '}
                                    TERMS & CONDITIONS
                                </Typography>
                            </div>

                            <div className={classes.dFlex}>
                                <FiChevronRight className={classes.optionIcons} />
                            </div>
                        </div>
                    </div>
                </div>
            </Dialog>
        </>
    );
}

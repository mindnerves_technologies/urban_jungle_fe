import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { IoCloseCircle } from 'react-icons/io5';
import itelyFlag from '../../../../../assets/itely.png';
import world from '../../../../../assets/world.png';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(2),
        '&:hover': {
            backgroundColor: 'transparent',
        },
    },
    title: {
        fontSize: '1rem',
        // fontWeight: '700',
        textAlign: 'center',
    },
    container: {
        padding: '10px',
    },
    closeButton: {
        position: 'absolute',
        right: 0,
        top: 0,
        color: '#000',
    },
    countryFlagdiv: {
        display: 'flex',
        justifyContent: 'center',
        padding: '20px 10px',
    },
    activeImgDiv: {
        width: '50px',
        height: '50px',
        margin: '0px 25px',
        border: '1px solid #000',
        borderRadius: '50%',
    },
    imgDiv: { width: '50px', height: '50px', margin: '0px 25px', borderRadius: '50%' },
    flag: {
        height: '100%',
        width: '100%',
    },
    activeCountryName: {
        textAlign: 'center',
        fontSize: '14px',
        fontWeight: 700,
        color: '#000',
    },
    countryName: {
        textAlign: 'center',
        fontSize: '14px',
        color: 'grey',
    },
    horizontalLine: {
        borderBottom: '1px solid lightgray',
        width: '90%',
        position: 'relative',
        margin: 'auto',
    },

    icon: {
        borderRadius: '50%',
        width: 16,
        height: 16,
        boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
        backgroundColor: '#f5f8fa',
        backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
        '$root.Mui-focusVisible &': {
            outline: '2px auto rgba(19,124,189,.6)',
            outlineOffset: 2,
        },
        'input:hover ~ &': {
            backgroundColor: '#ebf1f5',
        },
        'input:disabled ~ &': {
            boxShadow: 'none',
            background: 'rgba(206,217,224,.5)',
        },
    },
    checkedIcon: {
        backgroundColor: '#000',
        backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
        '&:before': {
            display: 'block',
            width: 16,
            height: 16,
            backgroundImage: 'radial-gradient(#fff,#fff 20%,transparent 32%)',
            content: '""',
        },
        'input:hover ~ &': {
            backgroundColor: '#000',
        },
    },
    selectLanguage: {
        display: 'flex',
        justifyContent: 'center',
    },
    languageLabel: {
        fontSize: 14,
        fontWeight: 700,
        color: '#000',
    },
    submitBtn: {
        border: '0px',
        padding: '11px 1px',
        width: '30%',
        borderRadius: '20px',
        fontSize: '0.8rem',
        color: 'white',
        backgroundColor: '#000',
        outline: 'none',
        cursor: 'pointer',
    },
    clickable: {
        cursor: 'pointer',
    },
}));

function StyledRadio(props) {
    const classes = useStyles();

    return (
        <Radio
            className={classes.root}
            disableRipple
            color="default"
            checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
            icon={<span className={classes.icon} />}
            {...props}
        />
    );
}

export default function CountryPopup({ isOpen, setCountryPopup }) {
    const classes = useStyles();
    const [selectedCountry, setCountry] = React.useState('italy');

    const handleClose = () => {
        setCountryPopup(false);
    };

    return (
        <>
            <Dialog
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={isOpen}
                className="country_change_pop"
            >
                <div id="customized-dialog-title" className={classes.root} onClose={handleClose}>
                    <IconButton aria-label="close" className={classes.closeButton} onClick={handleClose}>
                        <IoCloseCircle />
                    </IconButton>
                </div>
                <div className={classes.container}>
                    <div>
                        <Typography className={classes.title} variant="h4">
                            Select Your Country
                        </Typography>
                    </div>
                    <div className={classes.countryFlagdiv}>
                        <div className={classes.clickable} onClick={() => setCountry('italy')}>
                            <div className={selectedCountry === 'italy' ? classes.activeImgDiv : classes.imgDiv}>
                                <img src={itelyFlag} alt="itely flag" className={classes.flag} />
                            </div>
                            <Typography
                                className={
                                    selectedCountry === 'italy' ? classes.activeCountryName : classes.countryName
                                }
                                variant="body2"
                            >
                                Italy
                            </Typography>
                        </div>
                        {/* <div className={classes.clickable} onClick={() => setCountry('uk')}>
                            <div className={selectedCountry === 'uk' ? classes.activeImgDiv : classes.imgDiv}>
                                <img src={ukFlag} alt="uk flag" className={classes.flag} />
                            </div>
                            <Typography
                                className={selectedCountry === 'uk' ? classes.activeCountryName : classes.countryName}
                                variant="body2"
                            >
                                UK
                            </Typography>
                        </div> */}
                        <div className={classes.clickable} onClick={() => setCountry('international')}>
                            <div
                                className={selectedCountry === 'international' ? classes.activeImgDiv : classes.imgDiv}
                            >
                                <img src={world} alt="world" className={classes.flag} />
                            </div>
                            <Typography
                                className={
                                    selectedCountry === 'international'
                                        ? classes.activeCountryName
                                        : classes.countryName
                                }
                                variant="body2"
                            >
                                International
                            </Typography>
                        </div>
                    </div>

                    <div className={classes.horizontalLine} />

                    <div className={classes.root}>
                        <Typography className={classes.title} variant="h4">
                            Select Language
                        </Typography>
                    </div>
                    <div className={classes.selectLanguage}>
                        <FormControl component="fieldset">
                            <RadioGroup row defaultValue="it" name="customized-radios">
                                <FormControlLabel
                                    className={classes.languageLabel}
                                    value="it"
                                    control={<StyledRadio />}
                                    label="Italian"
                                />
                                <FormControlLabel
                                    className={classes.languageLabel}
                                    value="en"
                                    control={<StyledRadio />}
                                    label="English"
                                />
                            </RadioGroup>
                        </FormControl>
                    </div>
                    <div className={classes.selectLanguage}>
                        <button className={classes.submitBtn}> Submit </button>
                    </div>
                </div>
            </Dialog>
        </>
    );
}

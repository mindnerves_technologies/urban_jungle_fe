import React, { useEffect } from 'react';
import { Typography, Grid } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import {actions as cartAction} from '../../../../main/cart/Basket/redux/actions';

function MiniCart({ history }) {
    const dispatch = useDispatch();

    const store_id = useSelector((state) => state && state.storeInfo && state.storeInfo.store_id);
    const cartData = useSelector((state) => state && state.cart && state.cart.myCartData);

    console.log(JSON.stringify(cartData));
    useEffect(() => {
        dispatch(
            cartAction.getMyCartRequest({
                quote_id: 179594,
                store_id: store_id
            })
        )
    })


    return (
        <div className="miniCartDropDown" id="menuscontainer">
            <div className="shoppingCart">
                <Typography className="shoppingCart-text" variant="h4">
                    Shopping Cart
                </Typography>
                <Typography className="shoppingCart-items" variant="caption">{cartData && cartData.products && cartData.products.length} items</Typography>
            </div>
            {cartData && cartData.products && cartData.products.map((item, index) => (<Grid container key={index} className="miniCart-pv-5-ph-15" justify="center" spacing={2}>
                <Grid item xs={3}>
                    <img className={'miniCart-p-img'} src={item.image} alt="item-1" />
                </Grid>
                <Grid item xs={9} className="miniCart-p-detail">
                    <div className="content-1">
                        <Typography className="brand-name" variant="body2">addidas</Typography>
                        <Typography className="miniCart-p-name" variant="h4">{item.product_name}</Typography>
                        <div className="pt-10">
                            <Typography className="miniCart-p-sizeAndQty" variant="body2"><span>Size:</span> 5(EU)</Typography>
                            <Typography className="miniCart-p-sizeAndQty" variant="body2"><span>Qty:</span> {item.qty}</Typography>
                        </div>
                    </div>
                    <div className="content-2">
                        <Typography className="miniCart-p-name" variant="body2">{item.currency}{item.price}</Typography>
                    </div>
                </Grid>

            </Grid>))}

            <Grid container className="miniCart-pv-5-ph-15">
                <div className="horizontal-line" />
                <Grid item xs={6}>
                    <Typography className="miniCart-p-total" variant="h4">Total</Typography>
                </Grid>
                <Grid item xs={6}>
                    <Typography className="miniCart-p-total text-right" variant="h4">
                        {cartData && cartData.products && cartData.products[0].currency}
                        {' '}
                        {cartData && cartData.products && cartData.products.reduce((a,b)=> a + b.price, 0)}
                    </Typography>
                </Grid>
                <div className="horizontal-line" />
            </Grid>


            <Grid container className="padding-15" spacing={3}>

                <Grid item xs={5}>
                    
                    <button className="miniCart-viewCartBtn"> View Cart </button>
                    
                </Grid>
                <Grid item xs={7}>
                
                    <button className="miniCart-checkoutBtn"> Proceed to checkout </button>
                </Grid>

            </Grid>

        </div>
    );
}

export default MiniCart;

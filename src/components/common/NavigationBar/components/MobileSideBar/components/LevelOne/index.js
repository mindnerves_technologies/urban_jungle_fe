import { Grid } from '@material-ui/core';
import React from 'react';
import { AiFillCaretRight, AiOutlineInbox, AiOutlinePoweroff } from 'react-icons/ai';
import { BiShoppingBag } from 'react-icons/bi';
import { BsHeart, BsPerson } from 'react-icons/bs';
import { FaFacebookF, FaInstagram } from 'react-icons/fa';
import { GrLocation } from 'react-icons/gr';
import { IoIosHelpCircleOutline } from 'react-icons/io';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';

const LevelOneMenu = ({ data, OnSelect, setIsAssistanceOpen, setDrawerState }) => {
    const history = useHistory();

    const store_code = useSelector((state) => state && state.storeInfo && state.storeInfo.store_code);
    const loginUser = useSelector((state) => state && state.storeInfo && state.storeInfo.loginUser);

    const gotoURL = (url, closeMenu = true) => {
        if (!url) return;
        if (closeMenu) setDrawerState(false);
        history.push(`/${store_code}/${url}`);
    };
    const onL1Click = (category) => {
        if (category[1]) {
            OnSelect(category);
        } else {
            gotoURL(category[0].url_key);
        }
    };

    return (
        <Grid>
            <Grid className="category-container">
                {Object.entries(data).map((category, index) => {
                    return (
                        <Grid key={index} onClick={() => onL1Click(category[1])}>
                            <Grid className="category-name">{category[1][0].name}</Grid>
                            {category[1][1] && (
                                <Grid className="category-arrow">
                                    <AiFillCaretRight />
                                </Grid>
                            )}
                        </Grid>
                    );
                })}
            </Grid>
            <Grid className="category-container">
                <Grid className="category-inner-container">
                    <Grid className="category-inner-icon">
                        <BsPerson></BsPerson>
                    </Grid>
                    <Grid className="category-inner-name">
                        <FormattedMessage id="myAccount" defaultMessage="My Account" />
                    </Grid>
                </Grid>
                <Grid className="category-inner-container">
                    <Grid className="category-inner-icon">
                        <BiShoppingBag />
                    </Grid>
                    <Grid className="category-inner-name">
                        <FormattedMessage id="myOrders" defaultMessage="My Orders" />
                    </Grid>
                </Grid>
                <Grid className="category-inner-container">
                    <Grid className="category-inner-icon">
                        <AiOutlineInbox></AiOutlineInbox>
                    </Grid>
                    <Grid className="category-inner-name">
                        <FormattedMessage id="ShippingReturn" defaultMessage={`Shipping & Return`} />
                    </Grid>
                </Grid>
                <Grid className="category-inner-container">
                    <Grid className="category-inner-icon">
                        <GrLocation></GrLocation>
                    </Grid>
                    <Grid className="category-inner-name">
                        <FormattedMessage id="storeLocator" defaultMessage="Store Locator" />
                    </Grid>
                </Grid>
                <Grid
                    className="category-inner-container"
                    onClick={() => {
                        history.push(`/${store_code}/sign-in`);
                        setDrawerState(false);
                    }}
                >
                    <Grid className="category-inner-icon">
                        <BsHeart />
                    </Grid>
                    <Grid className="category-inner-name">
                        <FormattedMessage id="Wishlist" defaultMessage="Wishlist" />
                    </Grid>
                </Grid>
                <Grid
                    className="category-inner-container"
                    onClick={() => {
                        setIsAssistanceOpen(true);
                        setDrawerState(false);
                    }}
                >
                    <Grid className="category-inner-icon">
                        <IoIosHelpCircleOutline></IoIosHelpCircleOutline>
                    </Grid>
                    <Grid className="category-inner-name">
                        <FormattedMessage id="assistance" defaultMessage="Assistance" />
                    </Grid>
                </Grid>
            </Grid>
            <Grid className="category-container">
                <Grid
                    className="category-inner-container"
                    onClick={() => {
                        history.push(`/${store_code}/about-us`);
                        setDrawerState(false);
                    }}
                >
                    <Grid className="category-inner-name">
                        <FormattedMessage id="aboutUJ" defaultMessage="About Urban Jungle" />
                    </Grid>
                    <Grid className="category-arrow">
                        <AiFillCaretRight />
                    </Grid>
                </Grid>
            </Grid>
            {/* <Grid className="category-container">
                <Grid className="category-inner-container">
                    <Grid className="category-inner-name">
                        <FormattedMessage id="blog" defaultMessage="Blog" />
                    </Grid>
                    <Grid className="category-arrow">
                        <AiFillCaretRight />
                    </Grid>
                </Grid>
            </Grid> */}
            <Grid className="category-container">
                <Grid className="category-inner-container">
                    <Grid className="category-inner-name follow-us">
                        <FormattedMessage id="FollowUs" defaultMessage="Follow Us" />
                    </Grid>
                    <Grid className="category-arrow follow-us-icon-container">
                        <a
                            href="https://www.facebook.com/urbanjunglestore/"
                            target="_blank"
                            rel="noreferrer"
                            onClick={() => {
                                setDrawerState(false);
                            }}
                        >
                            <FaFacebookF className="follow-us-icon" style={{ color: '#000' }} />
                        </a>
                        <a
                            href="https://www.instagram.com/urban_jungle_official/"
                            target="_blank"
                            rel="noreferrer"
                            onClick={() => {
                                setDrawerState(false);
                            }}
                        >
                            <FaInstagram style={{ color: '#000' }} />
                        </a>
                    </Grid>
                </Grid>
            </Grid>

            {loginUser && (
                <Grid className="category-container logout-container">
                    <Grid className="category-inner-container">
                        <Grid className="category-inner-icon">
                            <AiOutlinePoweroff></AiOutlinePoweroff>
                        </Grid>
                        <Grid className="category-inner-name">
                            <FormattedMessage id="logout" defaultMessage={`Logout`} />
                        </Grid>
                    </Grid>
                </Grid>
            )}
        </Grid>
    );
};

export default LevelOneMenu;

import { Typography } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import 'react-web-tabs/dist/react-web-tabs.css';
import './style.css';

const ReactTab = ({ category }) => {
    const [tabIndex, setTabIndex] = useState(0);
    const state = useSelector((state) => state);

    const storeInfo = state && state.storeInfo;
    const store_code = storeInfo && storeInfo.store_code;

    const convertL2ToL3 = (childrens) => {
        let categoryObject = {
            by_kind: {
                title: 'By Kind',
                categories: [],
            },
            by_category: {
                title: 'By Category',
                categories: [],
            },
            by_brand: {
                title: 'By Brand',
                categories: [],
            },
            by_trend: {
                title: 'By Trend',
                categories: [],
            },
            null: {
                title: '',
                categories: [],
            },
        };

        childrens &&
            childrens.forEach((cat) => categoryObject[cat.tagging] && categoryObject[cat.tagging].categories.push(cat));

        return (
            <>
                <div className="l3-category-main-container">
                    {Object.values(categoryObject).map((category) => {
                        if (!category.categories || category.categories.length === 0) return null;
                        return (
                            <div className="l3-category-div">
                                {category.title && <h6 className="title">{category.title}</h6>}
                                {category.categories.map((subCat, index) => {
                                    return (
                                        <>
                                            <div className="category-name">
                                                <Link
                                                    key={`${subCat}-${index}`}
                                                    to={`/${store_code}/${subCat.url_key}`}
                                                >
                                                    <Typography variant="span" className=" menu-item-label">
                                                        {subCat.name}
                                                    </Typography>
                                                </Link>
                                            </div>
                                        </>
                                    );
                                })}
                            </div>
                        );
                    })}
                </div>
            </>
        );
    };

    return (
        <Grid container className="h-100">
            <Grid className="yellow-background" item xs={3}>
                {category &&
                    category[1][1] &&
                    Object.entries(category[1][1]).map((L2category, index) => {
                        return (
                            <Link
                                className="tab-links"
                                onMouseOver={() => setTabIndex(index)}
                                key={index}
                                data-active={tabIndex === index}
                                to={`/${store_code}/${L2category[1].children[0].url_key}`}
                            >
                                <span>{L2category[1].children[0].name}</span>
                            </Link>
                        );
                    })}
            </Grid>
            <Grid item xs={9} className="l2-main">
                {category &&
                    category[1][1] &&
                    Object.entries(category[1][1]).map((L2category, index) => {
                        return (
                            <div
                                id={L2category[1].children[0].name}
                                key={index}
                                className={tabIndex === index ? 'show' : 'hide'}
                            >
                                <Grid container spacing={2} className="l2-conatiner">
                                    <Grid item xs={7}>
                                        <div className="menu-item">
                                            <Grid container spacing={2} className="h-100">
                                                {L2category[1].sub_children && L2category[1].sub_children.length > 0 ? (
                                                    <Grid item xs={12}>
                                                        {convertL2ToL3(L2category[1].sub_children)}
                                                    </Grid>
                                                ) : (
                                                    <Grid
                                                        item
                                                        xs={12}
                                                        className="d-flex justify-center align-items-center"
                                                    >
                                                        <Typography variant="span" className="No-category-msg">
                                                            No sub categories available for&nbsp;
                                                            {L2category[1].children[0].name}
                                                        </Typography>
                                                    </Grid>
                                                )}
                                            </Grid>
                                        </div>
                                    </Grid>
                                    <Grid item xs={5}>
                                        <div className="images-item">
                                            <Grid container spacing={2}>
                                                <Grid item xs={6}>
                                                    <div className="product-image">
                                                        <img
                                                            src={
                                                                L2category[1].children[0].cat_desk_banner_one ||
                                                                'https://source.unsplash.com/400x300/?nike'
                                                            }
                                                            alt="category banner one"
                                                        />
                                                        {/* <Typography className="product-title">Up to 50% off</Typography> */}
                                                    </div>
                                                </Grid>
                                                <Grid item xs={6}>
                                                    <div className="product-image">
                                                        <img
                                                            src={
                                                                L2category[1].children[0].cat_desk_banner_two ||
                                                                'https://source.unsplash.com/400x300/?addidas'
                                                            }
                                                            alt="category banner two"
                                                            once={true}
                                                        />
                                                        {/* <Typography className="product-title">Addidas classic </Typography> */}
                                                    </div>
                                                </Grid>
                                                <Grid item xs={6}>
                                                    <div className="product-image">
                                                        <img
                                                            src={
                                                                L2category[1].children[0].cat_desk_banner_three ||
                                                                'https://source.unsplash.com/400x300/?shoes'
                                                            }
                                                            alt="category banner three"
                                                            once={true}
                                                        />
                                                        {/* <Typography className="product-title">Addidas classic</Typography> */}
                                                    </div>
                                                </Grid>
                                                <Grid item xs={6}>
                                                    <div className="product-image">
                                                        <img
                                                            src={
                                                                L2category[1].children[0].cat_desk_banner_four ||
                                                                'https://source.unsplash.com/400x300/?jackets'
                                                            }
                                                            alt="category banner four"
                                                            once={true}
                                                        />
                                                        {/* <Typography className="product-title">Addidas classic</Typography> */}
                                                    </div>
                                                </Grid>
                                            </Grid>
                                        </div>
                                    </Grid>
                                </Grid>
                            </div>
                        );
                    })}
            </Grid>
        </Grid>
    );
};
export default ReactTab;

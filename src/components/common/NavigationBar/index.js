import { AppBar, Grid, Hidden, IconButton, InputBase, Toolbar, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import React, { useEffect, useState } from 'react';
import { BsSearch } from 'react-icons/bs';
import { FcMenu } from 'react-icons/fc';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import AssistanceIcon from '../../../assets/header/Icon_Assistance_White_Web.svg';
import Icon_Cart_Bag_White_Web from '../../../assets/header/Icon_Cart_Bag_White_Web.svg';
import Icon_User_White_Web from '../../../assets/header/Icon_User_White_Web.svg';
import Icon_Wishlist_White_Web from '../../../assets/header/Icon_Wishlist_White_Web.svg';
import itelyFlage from '../../../assets/itely.png';
import UjLogo from '../../../assets/uj_logo.png';
import AssistancePopup from './components/AssistancePopup/index';
import CountryPopup from './components/CountryPopup/index';
import MiniCart from './components/MiniCart/index';
import MobileSideBar from './components/MobileSideBar';
import ReactTab from './components/reactTab';
import { actions as MenuActions } from './redux/actions';

const useStyles = makeStyles((theme) => ({
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(0.5),
    },
    navBar: {
        backgroundColor: '#000',
        position: 'relative',
    },
    iconButton: {
        display: 'flex',
        flexDirection: 'column',
    },
    search: {
        position: 'relative',
        borderRadius: 20,
        backgroundColor: '#fff',
        color: '#000',
        marginRight: theme.spacing(2),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
        padding: '0px 3px',
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        top: 0,
        right: 0,
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        paddingRight: `calc(1em + ${theme.spacing(4)}px)`,
        paddingLeft: `calc(1em + ${theme.spacing(1)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        borderRadius: 10,
    },
    links: {
        textDecoration: 'none',
    },
}));

const NavBar = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const history = useHistory();
    const pathname = window.location.pathname;
    const path = pathname.split('/');

    const menuList = useSelector((state) => state && state.menu && state.menu.menuList && state.menu.menuList.data);
    const store_code = useSelector((state) => state && state.storeInfo && state.storeInfo.store_code);
    const loginUser = useSelector((state) => state && state.storeInfo && state.storeInfo.loginUser);

    const store_id = useSelector((state) => state && state.storeInfo && state.storeInfo.store_id);
    let AutoSuggestionData = useSelector(
        (state) =>
            state &&
            state.menu &&
            state.menu.autoSearchList &&
            state.menu.autoSearchList.data &&
            state.menu.autoSearchList.data.product_data,
    );

    useEffect(() => {
        dispatch(
            MenuActions.getMenuRequest({
                store_id: store_id,
            }),
        );
    }, [dispatch, store_id]);

    const [drawerState, setDrawerState] = useState(false);
    const [showMiniCart, setShowMiniCart] = React.useState(false);
    const [isAssistanceOpen, setIsAssistanceOpen] = React.useState(false);
    const [isCountryPopup, setCountryPopup] = React.useState(false);
    const [search, setSearch] = React.useState('');

    const onClickLogin = () => {
        if (!loginUser) {
            return history.push(`/${store_code}/sign-in`);
        }
    };

    const onClickLogo = () => {
        return history.push(`/${store_code}`);
    };

    const autoSearchText = (e) => {
        let searchResult = e.target.value;
        setSearch(searchResult);

        if (searchResult.length > 3) {
            dispatch(
                MenuActions.getAutoSearchRequest({
                    q: searchResult,
                    storeid: store_id,
                }),
            );
        } else {
            dispatch(
                MenuActions.clearSearchResult({
                    q: searchResult,
                    storeid: store_id,
                }),
            );
        }
    };

    const handleKeyPress = (e) => {
        let searchResult = e.target.value;
        if (e.keyCode === 13) {
            dispatch(
                MenuActions.clearSearchResult({
                    q: searchResult,
                    storeid: 4,
                }),
            );
            history.push(`/${store_code}/search?query=${searchResult}`);
            setSearch('');
        }
    };

    const gotoSearchResult = () => {
        dispatch(
            MenuActions.clearSearchResult({
                q: search,
                storeid: 4,
            }),
        );
        setSearch('');
        history.push(`/${store_code}/search?query=${search}`);
    };

    const gotoProductPage = (product) => {
        dispatch(
            MenuActions.clearSearchResult({
                q: search,
                storeid: 4,
            }),
        );
        setSearch('');
        history.push(`/${store_code}/${product.url_key}`);
    };

    const handleMiniCart = (value) => {
        setTimeout(() => {
            setShowMiniCart(value);
        }, 800);
    };

    return (
        <div className={classes.grow}>
            <AppBar position="static" className={classes.navBar}>
                <Toolbar style={{ height: '80px', position: 'static' }}>
                    <Hidden only={['lg', 'md']}>
                        <IconButton
                            edge="start"
                            className={classes.menuButton}
                            color="inherit"
                            aria-label="open drawer"
                            onClick={() => setDrawerState(true)}
                        >
                            <FcMenu style={{ color: '#fff' }} />
                        </IconButton>
                    </Hidden>
                    <MobileSideBar
                        drawerState={drawerState}
                        setDrawerState={setDrawerState}
                        setCountryPopup={setCountryPopup}
                        setIsAssistanceOpen={setIsAssistanceOpen}
                    ></MobileSideBar>
                    <img src={UjLogo} alt="Urban Jungle" className="header-logo" onClick={() => onClickLogo()} />
                    <Hidden only={['xs', 'sm', 'md']}>
                        <div className="nav-link">
                            <ul>
                                {menuList &&
                                    Object.entries(menuList).map((category, index) => {
                                        if (category[1][0].url_key === 'blog')
                                            return (
                                                <li key={index} className="nav-link-dropdown">
                                                    <a
                                                        href={`https://www.urbanjunglestore.com/blog/${store_code}/`}
                                                        style={{ textDecoration: 'none' }}
                                                        target="_blank"
                                                        rel="noreferrer"
                                                        className={classes.links}
                                                    >
                                                        <Typography className="nav-link-name" component="a">
                                                            {category[1][0].name}
                                                        </Typography>
                                                    </a>
                                                </li>
                                            );

                                        return (
                                            <li key={index} className="nav-link-dropdown">
                                                <Link
                                                    to={`/${store_code}/${category[1][0].url_key}`}
                                                    className={classes.links}
                                                >
                                                    <Typography className="nav-link-name" component="a">
                                                        {category[1][0].name}
                                                    </Typography>
                                                </Link>
                                                {category[1].length > 1 && (
                                                    <div
                                                        className="dropdown-menu"
                                                        aria-labelledby="navbarDropdownMenuLink"
                                                    >
                                                        <ReactTab category={category} />
                                                    </div>
                                                )}
                                            </li>
                                        );
                                    })}
                               
                                {/* Brands */}
                                <li key='brands' className="nav-link-dropdown">
                                    <Link to={`/${store_code}/brands`} className={classes.links}>
                                        <Typography className="nav-link-name" component="a">
                                            Brands
                                </Typography>

                                    </Link>
                                </li>
                                {/* Brand end */}
                                
                                </ul>
                        </div>
                    </Hidden>
                    <Hidden only={['lg']}>
                        <div className={classes.grow} />
                    </Hidden>
                    <Hidden only={['xs', 'sm', 'md']}>
                        <div className={classes.search}>
                            <input
                                placeholder="What are you looking for?"
                                className="header-search"
                                // inputProps={{ 'aria-label': 'search' }}
                                onChange={(e) => autoSearchText(e)}
                                onKeyDown={(e) => handleKeyPress(e)}
                                value={search}
                            />
                            <div className={classes.searchIcon}>
                                <BsSearch />
                            </div>
                            {search && search.length > 3 && AutoSuggestionData && AutoSuggestionData.length > 0 && (
                                <Grid className="auto-suggestion-products">
                                    {AutoSuggestionData &&
                                        AutoSuggestionData.map((product, index) => {
                                            return (
                                                <Grid
                                                    onClick={() => gotoProductPage(product)}
                                                    key={index}
                                                    container
                                                    className="product-data"
                                                >
                                                    <Grid item md={4}>
                                                        <img
                                                            className="product-image"
                                                            src={product.imageUrl}
                                                            alt="product"
                                                        />
                                                    </Grid>
                                                    <Grid item className="product-name-container" md={8}>
                                                        <Typography className="product-name">{product.name}</Typography>
                                                        {/* <Typography className="product-name">{product.name}</Typography> */}
                                                    </Grid>
                                                </Grid>
                                            );
                                        })}
                                    <Grid onClick={() => gotoSearchResult()} className="show-all-button">
                                        <Typography className="show-all-text">All Search Result</Typography>
                                    </Grid>
                                </Grid>
                            )}
                            {search && search.length > 3 && (!AutoSuggestionData || AutoSuggestionData.length === 0) && (
                                <Grid className="auto-suggestion-products">
                                    <Grid className="show-all-button">
                                        <Typography className="show-all-text">No Products Found</Typography>
                                    </Grid>
                                </Grid>
                            )}
                        </div>
                    </Hidden>
                    <div className="icon-container">
                        <Hidden only={['xs', 'sm']}>
                            <div className="icon-item-container">
                                <IconButton
                                    className="icon-item country-container"
                                    onClick={() => setCountryPopup(true)}
                                >
                                    <img className="flag-image" src={itelyFlage} alt="" />
                                </IconButton>
                                <Typography className="icon-title">Italy</Typography>
                            </div>
                        </Hidden>
                        <div className="icon-item-container">
                            <IconButton className="icon-item" onClick={() => setIsAssistanceOpen(true)} color="inherit">
                                <img src={AssistanceIcon} alt="assistanceIcon" />
                            </IconButton>
                            <Hidden only={['xs', 'sm']}>
                                <Typography className="icon-title">Assistance</Typography>
                            </Hidden>
                        </div>
                        <Hidden only={['xs', 'sm']}>
                            <div className="icon-item-container">
                                <IconButton className="icon-item" onClick={() => onClickLogin()} color="inherit">
                                    <img src={Icon_Wishlist_White_Web} alt="userIcon" />
                                </IconButton>

                                <Typography className="icon-title">Wishlist</Typography>
                            </div>
                        </Hidden>
                        <Hidden only={['xs', 'sm']}>
                            <div className="icon-item-container">
                                <IconButton className="icon-item" onClick={() => onClickLogin()} color="inherit">
                                    <img src={Icon_User_White_Web} alt="userIcon" />
                                </IconButton>
                                <Typography className="icon-title">
                                    {loginUser && loginUser.customer_id
                                        ? `${loginUser.firstname} ${loginUser.lastname}`
                                        : 'Login'}
                                </Typography>
                            </div>
                        </Hidden>
                        <div
                            className="flex-align-center"
                            onMouseEnter={() => setShowMiniCart(true)}
                            onMouseLeave={() => handleMiniCart(false)}
                        >
                            <div className="icon-item-container" style={{ margin: `6px 6px 6px 0px` }}>
                                <IconButton className="icon-item" color="inherit">
                                    <img src={Icon_Cart_Bag_White_Web} alt="cartIcon" />
                                </IconButton>
                                <Hidden only={['xs', 'sm']}>
                                    <Typography className="icon-title">Cart</Typography>
                                </Hidden>
                            </div>
                            <div className="Cart-count-container">
                                <Typography className="cart-count">10</Typography>
                            </div>
                            {showMiniCart && (
                                <div>
                                    <MiniCart />
                                </div>
                            )}
                        </div>
                    </div>
                    <AssistancePopup isOpen={isAssistanceOpen} setIsAssistanceOpen={setIsAssistanceOpen} />
                    <CountryPopup isOpen={isCountryPopup} setCountryPopup={setCountryPopup} />
                </Toolbar>
            </AppBar>
            <Hidden only={['lg']}>
                <div className="mobile-nav">
                    <div className={classes.search}>
                        <InputBase
                            placeholder="What are you looking for?"
                            classes={{
                                root: classes.inputRoot,
                                input: classes.inputInput,
                            }}
                            inputProps={{ 'aria-label': 'search' }}
                        />
                        <div className={classes.searchIcon}>
                            <BsSearch />
                        </div>
                    </div>
                    {path && path.length === 3 && path[2] !== 'home' && (
                        <Hidden only={['md', 'lg', 'xl']}>
                            <div style={{ paddingTop: 20 }}>
                                <div className="line" />
                            </div>
                        </Hidden>
                    )}

                    {path && (path.length === 2 || (path.length === 3 && (path[2] === 'home' || path[2] === ''))) && (
                        <div className="nav-link" style={{ marginBottom: '1rem' }}>
                            <Grid container style={{ flexWrap: 'inherit', overflow: 'auto' }}>
                                {menuList &&
                                    Object.entries(menuList).map((category, index) => {
                                        return (
                                            <div key={index} className="nav-link-dropdown">
                                                <Link
                                                    to={`/${store_code}/${category[1][0].url_key}`}
                                                    className={classes.links}
                                                    style={{ color: '#000' }}
                                                >
                                                    <Typography
                                                        className="nav-link-name"
                                                        component="a"
                                                        style={{ color: '#000' }}
                                                    >
                                                        {category[1][0].name}
                                                    </Typography>
                                                </Link>
                                            </div>
                                        );
                                    })}
                            </Grid>
                        </div>
                    )}
                </div>
            </Hidden>
        </div>
    );
};
export default NavBar;

import { call, put, takeLatest } from 'redux-saga/effects';
import api from '../../../../api';
import { actions, types } from './actions';

const getMenuListReq = function* getMenuListReq({ payload }) {
    try {
        const { data } = yield call(api.getMenuList, payload);
        if (data.status) {
            yield put(actions.getMenuRequestSuccess(data || []));
        } else {
            yield put(actions.getMenuRequestFailed());
        }
    } catch (err) {
        yield put(actions.getMenuRequestFailed());
    }
};

const getAutoSearchReq = function* getAutoSearchReq({ payload }) {
    try {
        const { q, storeid } = payload;
        const { data } = yield call(api.getAutoSearch, { q, storeid });

        if (data.status) {
            yield put(actions.getAutoSearchRequestSuccess(data || {}));
        } else {
            yield put(actions.getAutoSearchRequestFailed());
        }
    } catch (err) {
        yield put(actions.getAutoSearchRequestFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.GET_MENU_REQUEST, getMenuListReq);
    yield takeLatest(types.GET_AUTOSEARCH_REQUEST, getAutoSearchReq);
}

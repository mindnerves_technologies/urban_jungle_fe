import React, { useState } from 'react';
import FacebookLogin from 'react-facebook-login';
import { GoogleLogin } from 'react-google-login';
import Grid from '@material-ui/core/Grid';
import { Button, Radio, Typography, TextField, FormHelperText, CircularProgress, Backdrop } from '@material-ui/core';
import { isEmail } from './utils';
import { actions as loginActions } from './redux/actions';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import SimpleBreadcrumbs from '../common/Breadcrumbs';
import { FormattedMessage } from 'react-intl';
import { FcGoogle } from 'react-icons/fc';
import { FaFacebookF } from 'react-icons/fa';

const Login = () => {
    const dispatch = useDispatch();
    const history = useHistory();

    const ecomLogin = useSelector((state) => state.login && state.login.loading);
    const store_code = useSelector((state) => state && state.storeInfo && state.storeInfo.store_code);
    const store_id = useSelector((state) => state && state.storeInfo && state.storeInfo.store_id);
    const fb_loading = useSelector((state) => state.login && state.login.fb_loading);
    const gl_loading = useSelector((state) => state.login && state.login.gl_loading);

    const [loginData, setLoginData] = useState({
        email: undefined,
        password: undefined,
    });
    const [isValidate, setIsValidate] = useState(false);

    const responseFacebook = (response) => {
        if (response && response.accessToken) {
            if (response.email) {
                dispatch(
                    loginActions.socialFacebookLoginReq({
                        firstname: response.name && response.name.split(' ')[0],
                        lastname: response.name && response.name.split(' ')[1],
                        email: response.email,
                        store_id: store_id,
                        quest_quote: '',
                        subscribe_to_newsletter: 1,
                        fb_access_token: response.accessToken,
                        register_type: 'facebook',
                    }),
                );
            } else {
                return toast.error(`Getting email is undefined`);
            }
        }
    };

    const responseGoogle = (response) => {
        if (response && response.accessToken) {
            dispatch(
                loginActions.socialGoogleLoginReq({
                    firstname: response && response.profileObj && response.profileObj.givenName,
                    lastname: response && response.profileObj && response.profileObj.familyName,
                    email: response && response.profileObj && response.profileObj.email,
                    store_id: store_id,
                    quest_quote: '',
                    subscribe_to_newsletter: 0,
                    fb_access_token: response.accessToken,
                    register_type: 'google',
                }),
            );
        }
    };

    const onChangeInput = (name, event) => {
        setLoginData({ ...loginData, [name]: event.target.value });
    };

    const onClickSignIn = () => {
        setIsValidate(true);
        if (!isEmail(loginData.email) || !loginData.password || loginData.password === '') {
            return;
        }
        setIsValidate(false);
        dispatch(
            loginActions.ecomLoginReq({
                email: loginData.email,
                password: loginData.password,
                guestquote: '',
            }),
        );
    };

    const redirectToRegister = () => {
        history.push(`/${store_code}/sign-up`);
    };

    const breadCrumbs = [
        {
            name: <FormattedMessage id="Common.Home.Text" defaultMessage="Home" />,
            url: `/${store_code}`,
        },

        {
            name: 'Sign in',
        },
    ];

    return (
        <Grid container justify="center">
            <Grid item xs={11}>
                <div className="signin">
                    <SimpleBreadcrumbs breadCrumbs={breadCrumbs} />
                    <Grid container direction="row" justify="space-between" alignItems="center" className="mainTitle">
                        <Grid item xs={12} md={12}>
                            <Typography>Sign in</Typography>
                        </Grid>
                    </Grid>

                    <Grid container direction="row" justify="center" alignItems="flex-start">
                        <Grid item xs={12} md={4} className="signinCode">
                            <div className="titleGroup">
                                <div>
                                    <Radio
                                        checked={true}
                                        value="a"
                                        name="radio-button-demo"
                                        inputProps={{ 'aria-label': 'A' }}
                                    />
                                    <Grid container>
                                        <Typography className="title">Sign In</Typography>
                                        <Grid item xs={12}>
                                            <Typography className="info">Checkout faster with saved details</Typography>
                                        </Grid>
                                    </Grid>
                                </div>
                            </div>
                            <div className="descGroup">
                                <Grid container>
                                    <Grid item xs={12}>
                                        <Typography className="titleInput">Email*</Typography>
                                        <TextField
                                            variant="outlined"
                                            value={loginData.email}
                                            onChange={(e) => onChangeInput('email', e)}
                                            onKeyPress={(ev) => {
                                                if (ev.key === 'Enter') {
                                                    onClickSignIn();
                                                }
                                            }}
                                            error={isValidate && !isEmail(loginData.email)}
                                        />
                                        {isValidate && !isEmail(loginData.email) && (
                                            <FormHelperText className="error_message">
                                                {' '}
                                                Please enter valid email
                                            </FormHelperText>
                                        )}
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Typography className="titleInput">Password*</Typography>
                                        <TextField
                                            variant="outlined"
                                            type="password"
                                            value={loginData.password}
                                            onChange={(e) => onChangeInput('password', e)}
                                            onKeyPress={(ev) => {
                                                if (ev.key === 'Enter') {
                                                    onClickSignIn();
                                                }
                                            }}
                                            error={isValidate && (!loginData.password || loginData.password === '')}
                                        />
                                        {isValidate && (!loginData.password || loginData.password === '') && (
                                            <FormHelperText className="error_message">
                                                Please enter password
                                            </FormHelperText>
                                        )}
                                    </Grid>

                                    <Typography className="forgotPassword">Forgot pasword?</Typography>

                                    <Button
                                        className="signin-button"
                                        variant="contained"
                                        disabled={ecomLogin}
                                        startIcon={ecomLogin ? <CircularProgress size={30} /> : ''}
                                        onClick={() => onClickSignIn()}
                                    >
                                        {!ecomLogin && `Sign In`}
                                    </Button>
                                </Grid>
                                <div className="orOuter">
                                    <hr />
                                    <Typography className="or">OR</Typography>
                                </div>

                                <Grid container direction="row" justify="center" alignItems="center" className="social">
                                    <Grid item xs={6} md={5} className="outer">
                                        <div className="google">
                                            <GoogleLogin
                                                clientId="506112648176-7jb404urqvso1lprrqkpna2cq728cq2g.apps.googleusercontent.com"
                                                render={(renderProps) => (
                                                    <Button
                                                        onClick={renderProps.onClick}
                                                        disabled={renderProps.disabled}
                                                        startIcon={<FcGoogle />}
                                                    >
                                                        Google
                                                    </Button>
                                                )}
                                                buttonText="Login"
                                                onSuccess={responseGoogle}
                                                onFailure={responseGoogle}
                                                cookiePolicy={'single_host_origin'}
                                            />
                                        </div>
                                    </Grid>
                                    <Grid item xs={6} md={6} className="outer">
                                        <div className="facebook">
                                            <FacebookLogin
                                                appId="883178962258767"
                                                fields="name,email,picture"
                                                icon={<FaFacebookF style={{ color: 'blue', marginRight: 8 }} />}
                                                render={(renderProps) => (
                                                    <button onClick={renderProps.onClick}>Facebook</button>
                                                )}
                                                callback={responseFacebook}
                                            />
                                        </div>
                                    </Grid>
                                </Grid>
                            </div>
                        </Grid>
                    </Grid>

                    <Grid container direction="row" justify="center" alignItems="center">
                        <Grid item xs={12} md={4} className="register">
                            <div className="titleGroup" style={{ borderBottom: 'none' }}>
                                <div>
                                    <Radio
                                        checked={false}
                                        value="a"
                                        name="radio-button-demo"
                                        inputProps={{ 'aria-label': 'A' }}
                                        onClick={() => redirectToRegister()}
                                    />
                                    <Grid container>
                                        <Typography className="title">Register</Typography>
                                        <Grid item xs={12}>
                                            <Typography className="info">
                                                Register with us to track and manage your orders
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </div>
                            </div>
                        </Grid>
                    </Grid>
                </div>
            </Grid>
            <Backdrop open={fb_loading} style={{ zIndex: 1000000 }}>
                <CircularProgress color="inherit" style={{ color: '#ffffff' }} />
            </Backdrop>
            <Backdrop open={gl_loading} style={{ zIndex: 1000000 }}>
                <CircularProgress color="inherit" style={{ color: '#ffffff' }} />
            </Backdrop>
        </Grid>
    );
};

export default Login;

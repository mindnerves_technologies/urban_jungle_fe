import { createAction } from 'redux-actions';

const ECOM_LOGIN_REQ = 'UJ/ECOM_LOGIN_REQ';
const ECOM_LOGIN_REQ_SUCCESS = 'UJ/ECOM_LOGIN_REQ_SUCCESS';
const ECOM_LOGIN_REQ_FAILED = 'UJ/ECOM_LOGIN_REQ_FAILED';

const SOCIAL_FACEBOOK_LOGIN_REQ = 'UJ/SOCIAL_FACEBOOK_LOGIN_REQ';
const SOCIAL_FACEBOOK_LOGIN_REQ_SUCCESS = 'UJ/SOCIAL_FACEBOOK_LOGIN_REQ_SUCCESS';
const SOCIAL_FACEBOOK_LOGIN_REQ_FAILED = 'UJ/SOCIAL_FACEBOOK_LOGIN_REQ_FAILED';

const SOCIAL_GOOGLE_LOGIN_REQ = 'UJ/SOCIAL_GOOGLE_LOGIN_REQ';
const SOCIAL_GOOGLE_LOGIN_REQ_SUCCESS = 'UJ/SOCIAL_GOOGLE_LOGIN_REQ_SUCCESS';
const SOCIAL_GOOGLE_LOGIN_REQ_FAILED = 'UJ/SOCIAL_GOOGLE_LOGIN_REQ_FAILED';

const ecomLoginReq = createAction(ECOM_LOGIN_REQ);
const ecomLoginReqSuccess = createAction(ECOM_LOGIN_REQ_SUCCESS);
const ecomLoginReqFailed = createAction(ECOM_LOGIN_REQ_FAILED);

const socialFacebookLoginReq = createAction(SOCIAL_FACEBOOK_LOGIN_REQ);
const socialFacebookLoginReqSuccess = createAction(SOCIAL_FACEBOOK_LOGIN_REQ_SUCCESS);
const socialFacebookLoginReqFailed = createAction(SOCIAL_FACEBOOK_LOGIN_REQ_FAILED);

const socialGoogleLoginReq = createAction(SOCIAL_GOOGLE_LOGIN_REQ);
const socialGoogleLoginReqSuccess = createAction(SOCIAL_GOOGLE_LOGIN_REQ_SUCCESS);
const socialGoogleLoginReqFailed = createAction(SOCIAL_GOOGLE_LOGIN_REQ_FAILED);

export const actions = {
    ecomLoginReq,
    ecomLoginReqSuccess,
    ecomLoginReqFailed,

    socialFacebookLoginReq,
    socialFacebookLoginReqSuccess,
    socialFacebookLoginReqFailed,

    socialGoogleLoginReq,
    socialGoogleLoginReqSuccess,
    socialGoogleLoginReqFailed,
};

export const types = {
    ECOM_LOGIN_REQ,
    ECOM_LOGIN_REQ_SUCCESS,
    ECOM_LOGIN_REQ_FAILED,

    SOCIAL_FACEBOOK_LOGIN_REQ,
    SOCIAL_FACEBOOK_LOGIN_REQ_SUCCESS,
    SOCIAL_FACEBOOK_LOGIN_REQ_FAILED,

    SOCIAL_GOOGLE_LOGIN_REQ,
    SOCIAL_GOOGLE_LOGIN_REQ_SUCCESS,
    SOCIAL_GOOGLE_LOGIN_REQ_FAILED,
};

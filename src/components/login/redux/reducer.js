import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandlers = {
    [types.ECOM_LOGIN_REQ]: (state) => ({
        ...state,
        loading: true,
    }),
    [types.ECOM_LOGIN_REQ_SUCCESS]: (state) => ({
        ...state,
        loading: false,
    }),
    [types.ECOM_LOGIN_REQ_FAILED]: (state) => ({
        ...state,
        loading: false,
    }),
    [types.SOCIAL_FACEBOOK_LOGIN_REQ]: (state) => ({
        ...state,
        fb_loading: true,
    }),
    [types.SOCIAL_FACEBOOK_LOGIN_REQ_SUCCESS]: (state) => ({
        ...state,
        fb_loading: false,
    }),
    [types.SOCIAL_FACEBOOK_LOGIN_REQ_FAILED]: (state) => ({
        ...state,
        fb_loading: false,
    }),
    [types.SOCIAL_GOOGLE_LOGIN_REQ]: (state) => ({
        ...state,
        gl_loading: true,
    }),
    [types.SOCIAL_GOOGLE_LOGIN_REQ_SUCCESS]: (state) => ({
        ...state,
        gl_loading: false,
    }),
    [types.SOCIAL_GOOGLE_LOGIN_REQ_FAILED]: (state) => ({
        ...state,
        gl_loading: false,
    }),
};

export default handleActions(actionHandlers, {
    loading: false,
    fb_loading: false,
    gl_loading: false,
});

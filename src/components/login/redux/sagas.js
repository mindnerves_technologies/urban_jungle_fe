import { call, put, takeLatest } from 'redux-saga/effects';
import APIList from '../../../api';
import { actions, types } from './actions';
import { toast } from 'react-toastify';
import { push } from 'connected-react-router';
import { actions as storeAction } from '../../storeInfo/redux/actions';

const ecomLoginReq = function* ecomLoginReq({ payload }) {
    try {
        const { data } = yield call(APIList.ecomLogin, payload);
        if (data.status) {
            toast.success(`Login Successfully!`);
            yield put(push(`/en/home`));
            yield put(storeAction.setLoginUserDetails(data.customer_details));
        } else {
            toast.error(data.message || 'Something wrong, Please try after some time');
        }
        yield put(actions.ecomLoginReqSuccess());
    } catch (err) {
        yield put(actions.ecomLoginReqFailed());
        toast.error(
            (err.response && err.response.data && err.response.data.error) ||
                'Something wrong, Please try after some time',
        );
    }
};

const socialGoogleLoginReq = function* socialGoogleLoginReq({ payload }) {
    try {
        const { data } = yield call(APIList.socialGoogleLogin, payload);
        if (data.status) {
            toast.success(`Login Successfully!`);
            yield put(actions.socialGoogleLoginReqSuccess());
            yield put(push(`/en/home`));
            yield put(storeAction.setLoginUserDetails(data.customer_details));
        } else {
            toast.error(data.message || 'Something wrong, Please try after some time');
        }
        yield put(actions.socialGoogleLoginReqSuccess());
    } catch (err) {
        yield put(actions.socialGoogleLoginReqSuccess());
        toast.error(
            (err.response && err.response.data && err.response.data.error) ||
                'Something wrong, Please try after some time',
        );
    }
};

const socialFacebookLoginReq = function* socialFacebookLoginReq({ payload }) {
    try {
        const { data } = yield call(APIList.socialFacebookLogin, payload);
        if (data.status) {
            yield put(actions.socialFacebookLoginReqSuccess());
            toast.success(`Login Successfully!`);
            yield put(push(`/en/home`));
            yield put(storeAction.setLoginUserDetails(data.customer_details));
        } else {
            toast.error(data.message || 'Something wrong, Please try after some time');
        }
        yield put(actions.socialFacebookLoginReqSuccess());
    } catch (err) {
        yield put(actions.socialFacebookLoginReqFailed());
        toast.error(
            (err.response && err.response.data && err.response.data.error) ||
                'Something wrong, Please try after some time',
        );
    }
};

export default function* sagas() {
    yield takeLatest(types.ECOM_LOGIN_REQ, ecomLoginReq);
    yield takeLatest(types.SOCIAL_GOOGLE_LOGIN_REQ, socialGoogleLoginReq);
    yield takeLatest(types.SOCIAL_FACEBOOK_LOGIN_REQ, socialFacebookLoginReq);
}

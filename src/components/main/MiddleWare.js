import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import APIList from '../../api';
import Loader from '../common/Loader';
import PLP from '../plp';
import PDP from './pdp';

const Middleware = () => {
    const url_key = useParams();

    const store_id = useSelector((state) => state && state.storeInfo && state.storeInfo.store_id);

    const [loading, setLoading] = useState(true);
    const [isPDP, setIsPDP] = useState(false);
    const [isPLP, setIsPLP] = useState(false);

    useEffect(() => {
        if (
            url_key.category !== 'faq' ||
            url_key.category !== 'contact-us' ||
            url_key.category !== 'terms-and-conditions' ||
            url_key.category !== 'shipping-and-return' ||
            url_key.category !== 'cookie-policy' ||
            url_key.category !== 'privacy' ||
            url_key.category !== 'not-found' ||
            url_key.category !== 'sign-in' ||
            url_key.category !== 'sign-up' ||
            url_key.category !== 'cart' ||
            url_key.category !== 'home' ||
            url_key.category !== 'about-us'
        ) {
            setLoading(true);
            setIsPDP(false);
            setIsPLP(false);
            APIList.checkPDPOrPLPUrl({
                store_id: store_id,
                url_key: url_key.category,
            })
                .then(({ data }) => {
                    if (data.status) {
                        if (data.urltype === 'category') {
                            setIsPLP(true);
                        } else {
                            setIsPDP(true);
                        }
                    }
                })
                .finally(() => {
                    setLoading(false);
                });
        } else {
            setLoading(false);
        }
    }, [url_key.category, store_id]);

    if (
        url_key.category === 'contact-us' ||
        url_key.category === 'faq' ||
        url_key.category === 'terms-and-conditions' ||
        url_key.category === 'shipping-and-return' ||
        url_key.category === 'cookie-policy' ||
        url_key.category === 'privacy' ||
        url_key.category === 'not-found' ||
        url_key.category === 'sign-in' ||
        url_key.category === 'sign-up' ||
        url_key.category === 'cart' ||
        url_key.category === 'checkout-login' ||
        url_key.category === 'home' ||
        url_key.category === 'about-us'
    ) {
        return null;
    }

    return (
        <div>
            {!loading && (
                <>
                    {isPDP && <PDP url_key={url_key.category} />}
                    {isPLP && <PLP url_key={url_key.category} />}
                </>
            )}
            {loading && <Loader />}
        </div>
    );
};

export default Middleware;

import api from '../../../../../api/apiServices';

const getCart = (payload) => api.post(`/index.php/rest/V1/app/orob/mycart`, payload);
const updateCartQty = (payload) => api.post(`/index.php/rest/V1/app/orob/updatecart`, payload) ;
const removeCartItem = (payload) => api.post(`/index.php/rest/V1/app/deletecart`, payload);
const getCartCount = (payload) => api.post('/index.php/rest/V1/app/orob/mycartcount', payload);
const getMyCart = (payload) => api.post(`/index.php/rest/V1/app/mycart/`, payload);

export default {
    getCart,
    updateCartQty,
    removeCartItem,
    getCartCount,
    getMyCart,
}
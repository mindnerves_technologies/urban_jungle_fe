import React from 'react';
import { Grid, Typography, Button, TextField } from '@material-ui/core';
import './cartPayment.css';
import { FormattedMessage } from 'react-intl';
import { withRouter } from 'react-router';
import CartData from '../../data.json';

function CartPaymentDetails({ history }) {
    const cartData = CartData && CartData.data;

    return (
        <Grid container justify="flex-end" className="paymentPadding">
            <Grid item xs={12} md={9} className="CartPaymentContainer">
                <Grid container>
                    <Grid item xs={12}>
                        <Typography className="OrderSummaryText">
                            <FormattedMessage id="Cart.OrderSummary" defaultMessage="Cart Summary" />
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        {cartData && cartData.subtotal && (
                            <Grid container justify="space-between">
                                <div>
                                    <Typography className="cartpayment-label">
                                        <FormattedMessage id="Cart.Subtotal" defaultMessage="Subtotal" />
                                    </Typography>
                                </div>
                                <div>
                                    <Typography className="cartpayment-value">
                                        {cartData && `${cartData.currency} ${cartData.subtotal}`}
                                    </Typography>
                                </div>
                            </Grid>
                        )}
                        <Grid container justify="space-between">
                            <div>
                                <Typography className="cartpayment-label">
                                    <FormattedMessage id="Cart.Saving" defaultMessage="Saving" />
                                </Typography>
                            </div>
                            <div>
                                <Typography className="cartpayment-value">
                                    {cartData && `${cartData.currency} ${cartData.discount_amount}`}
                                </Typography>
                            </div>
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <div className="line" />
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container justify="space-between">
                            <div>
                                <Typography className="cartpayment-total-label">
                                    <FormattedMessage id="Cart.Total" defaultMessage="Total" />
                                </Typography>
                            </div>
                            <div>
                                <Typography className="cartpayment-total-value">
                                    {cartData && `${cartData.currency} ${cartData.grand_total}`}
                                </Typography>
                            </div>
                        </Grid>
                    </Grid>

                    <Grid item xs={12}>
                        <Button className="paymentCheckout">
                            <FormattedMessage id="Cart.Checkout" defaultMessage="Checkout" />
                        </Button>
                    </Grid>
                </Grid>
            </Grid>

            <Grid item xs={12} md={9} className="CartPaymentContainer" style={{ marginTop: '0.6rem' }}>
                <Grid container>
                    <div>
                        <Typography className="cartpayment-total-label">
                            <FormattedMessage id="Cart.Total" defaultMessage="Apply Your Voucher" />
                        </Typography>
                    </div>
                    <Grid item xs={12}>
                        <Grid container style={{ marginTop: '0.6rem' }} className="voucher-cnt">
                            {/* <div> */}
                            <TextField variant="outlined" className="voucher-input" placeholder="Enter Code" />
                            {/* </div> */}
                            {/* <div> */}
                            <Button variant="outlined" className="voucher-apply-btn">
                                Apply
                            </Button>
                            {/* </div> */}
                        </Grid>
                        <Typography style={{ paddingTop: '0.6rem' }}>Welcome2021New - Applied</Typography>
                    </Grid>
                </Grid>
            </Grid>

            {/* <Grid item xs={12} md={9} style={{marginBottom: '30px'}}>
                <Button className="continue-shopping-button"
                    onClick={() => gotoHomePage()}
                    startIcon={language === 'en' ? <IoIosArrowBack /> : <CgChevronRight className="right-icon"/>}
                >
                    <FormattedMessage id="Cart.ContinueShopping" defaultMessage="Continue Shopping" />
                </Button>
            </Grid> */}
        </Grid>
    );
}

export default withRouter(CartPaymentDetails);

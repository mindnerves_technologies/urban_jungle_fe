import React from 'react';
import {
    Grid,
    Typography,
    TextField,
    InputAdornment,
    IconButton,
    ListItem,
    ListItemText,
    List,
    Divider,
    Hidden,
} from '@material-ui/core';
import './item.css';
import { FormattedMessage } from 'react-intl';
import { MdFavoriteBorder } from 'react-icons/md';
import { IoRemoveOutline, IoAddOutline } from 'react-icons/io5';
import { IoMdClose } from 'react-icons/io';
import { getSubTotalAmount, getCartAmount, getCartAmountMobile } from '../../../../utils';
import { withRouter } from 'react-router';
import { useSelector } from 'react-redux';

function CartItem({ item, history }) {
    const state = useSelector((state) => state);

    const store_locale = state && state.auth && state.auth.store_locale;

    const gotoPDPPage = (url_key) => {
        return history.push(`/${store_locale}/products-details/${url_key}`);
    };

    const reduceQty = (data) => {
        if (data && data.qty > 1) {
            // dispatch(
            //     basketActions.updateCartItemQtyRequest({
            //         product_id: data.id,
            //         quote_id: quote_id,
            //         qty: parseInt(data.qty) - 1,
            //         sku: data.sku,
            //         store_id: store_id,
            //         delivery_type: 'DELIVERY',
            //     }),
            // );
        }
    };

    const addQty = (data) => {
        if (data.qty < parseInt(data && data.is_in_stock && data.is_in_stock.stock)) {
            // dispatch(
            //     basketActions.updateCartItemQtyRequest({
            //         product_id: data.id,
            //         quote_id: quote_id,
            //         qty: parseInt(data.qty) + 1,
            //         sku: data.sku,
            //         store_id: store_id,
            //         delivery_type: 'DELIVERY',
            //     }),
            // );
        }
    };

    const removeProducts = (data) => {
        // dispatch(
        //     basketActions.removeCartItemRequest({
        //         quote_id: quote_id,
        //         sku: data.sku,
        //         store_id: store_id,
        //     }),
        // );
    };

    return (
        <div>
            <Hidden only={['xs', 'sm']}>
                <Grid container className="CartItem">
                    <Grid item xs={5}>
                        <Grid container>
                            <Grid
                                item
                                xs={6}
                                className="textAlignCenter cursor-pointer"
                                onClick={() => gotoPDPPage(item && item.url_key)}
                            >
                                <img
                                    src={item && item.image && item.image[0]}
                                    alt="cartImage"
                                    className="cartItemImage"
                                    style={{ background: '#e1e1e1', padding: '0.4rem' }}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <Grid container alignItems="center">
                                    <Grid item xs={12}>
                                        <Typography className="cartItemName">{item && item.product_name}</Typography>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Typography className="cartItemOtherDetailText">
                                            <FormattedMessage id="PDP.Age" defaultMessage="Size" />: 5 (EU)
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={12}>
                                        <Grid container>
                                            <Typography className="cartItemWishlist">
                                                <MdFavoriteBorder />
                                                <FormattedMessage
                                                    id="Cart.MoveToWishlist"
                                                    defaultMessage="Move to Wishlist"
                                                />
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={7}>
                        <Grid container justify="space-between">
                            <Grid item xs={3} className="textAlignCenter">
                                {getCartAmount(item)}
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    className="addAndRemove flex alignItemCenter justifyCenter"
                                    variant="outlined"
                                    disabled
                                    value={item.qty}
                                    margin="normal"
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment
                                                position="start"
                                                classes={{ positionStart: 'centerAdornmentR' }}
                                            >
                                                <IconButton
                                                    size="small"
                                                    disabled={item.qty <= 1}
                                                    onClick={() => reduceQty(item)}
                                                >
                                                    <IoRemoveOutline />
                                                </IconButton>
                                            </InputAdornment>
                                        ),
                                        endAdornment: (
                                            <InputAdornment
                                                position="start"
                                                classes={{ positionStart: 'centerAdornmentL' }}
                                            >
                                                <IconButton
                                                    size="small"
                                                    disabled={
                                                        item.qty >=
                                                        parseInt(item && item.is_in_stock && item.is_in_stock.stock)
                                                    }
                                                    onClick={() => addQty(item)}
                                                >
                                                    <IoAddOutline />
                                                </IconButton>
                                            </InputAdornment>
                                        ),
                                    }}
                                />
                            </Grid>
                            <Grid item xs={3} className="textAlignCenter">
                                {getSubTotalAmount(item, removeProducts)}
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Hidden>

            <Hidden only={['md', 'lg', 'xl']}>
                <Grid container className="CartItemM" justify="space-between">
                    <Grid
                        item
                        xs={4}
                        className="textAlignCenter cursor-pointer displayFlexAlignEnd"
                        onClick={() => gotoPDPPage(item && item.url_key)}
                    >
                        <img src={item && item.image && item.image[0]} alt="cartImage" className="cartItemImageM" />
                    </Grid>
                    <Grid item xs={7}>
                        <Grid item xs={12}>
                            <Typography className="cartItemName">{item && item.product_name}</Typography>
                            <Grid item xs={12}>
                                <Typography className="cartItemOtherDetailText">
                                    <FormattedMessage id="PDP.Age" defaultMessage="Size" />
                                    :5 (EU)
                                </Typography>
                            </Grid>

                            <Grid item xs={12} className="CartAvailableFor">
                                <Grid container justify="flex-start">
                                    <Typography className="availableFor-text">
                                        <FormattedMessage id="PDP.AvailableFor" defaultMessage="Available for" />
                                    </Typography>
                                    <Grid container justify="flex-start">
                                        <div>
                                            <List component="nav" aria-label="main mailbox folders" className="p-t-b">
                                                <ListItem className="p-t-b p-l-zero">
                                                    <ListItemText
                                                        primary={
                                                            <FormattedMessage
                                                                id="PDP.HomeDelivery"
                                                                defaultMessage="Home Delivery"
                                                            />
                                                        }
                                                    />
                                                </ListItem>
                                            </List>
                                        </div>
                                        <div className="flex alignItemCenter">
                                            <Divider orientation="vertical" className="line" />
                                        </div>
                                        <div>
                                            <List component="nav" aria-label="main mailbox folders" className="p-t-b">
                                                <ListItem className="padding-right-zero p-t-b">
                                                    <ListItemText
                                                        primary={
                                                            <FormattedMessage
                                                                id="PDP.PickupFromStore"
                                                                defaultMessage="Pickup from store"
                                                            />
                                                        }
                                                    />
                                                </ListItem>
                                            </List>
                                        </div>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={4}>
                        <TextField
                            className="addAndRemove flex alignItemCenter justifyCenter"
                            variant="outlined"
                            disabled
                            value={item.qty}
                            margin="normal"
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start" classes={{ positionStart: 'centerAdornmentR' }}>
                                        <IconButton
                                            size="small"
                                            disabled={item.qty <= 1}
                                            onClick={() => reduceQty(item)}
                                        >
                                            <IoRemoveOutline />
                                        </IconButton>
                                    </InputAdornment>
                                ),
                                endAdornment: (
                                    <InputAdornment position="start" classes={{ positionStart: 'centerAdornmentL' }}>
                                        <IconButton
                                            size="small"
                                            disabled={
                                                item.qty >= parseInt(item && item.is_in_stock && item.is_in_stock.stock)
                                            }
                                            onClick={() => addQty(item)}
                                        >
                                            <IoAddOutline />
                                        </IconButton>
                                    </InputAdornment>
                                ),
                            }}
                        />
                    </Grid>
                    <Grid item xs={7}>
                        {getCartAmountMobile(item)}
                    </Grid>
                    <Grid item xs={12}>
                        <Grid
                            container
                            justify="space-around"
                            alignItems="center"
                            className="removeAndWishListContainer"
                        >
                            <div>
                                <Typography className="removeItem" onClick={() => removeProducts(item)}>
                                    <IoMdClose />
                                    Remove
                                </Typography>
                            </div>
                            <div className="lineVertical"></div>
                            <div>
                                <Typography className="WishListM">
                                    <MdFavoriteBorder />
                                    <FormattedMessage id="Cart.MoveToWishlist" defaultMessage="Move to Wishlist" />
                                </Typography>
                            </div>
                        </Grid>
                    </Grid>
                </Grid>
            </Hidden>
        </div>
    );
}

export default withRouter(CartItem);

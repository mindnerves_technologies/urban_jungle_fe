import React from 'react';
import { Grid } from '@material-ui/core';
import CartItem from './Item';
import { useSelector } from 'react-redux';

export default function MobileCartItemDetails () {

    const state = useSelector(
        state => state
    );

    const cartDetails = state && state.basket && state.basket.cartDetails;
    const cartProducts = cartDetails && cartDetails.products;
    
    return (
        <Grid container>
            {cartProducts && cartProducts.map((item, index) => (
                <Grid container key={`basket_item_${index}`}>
                    <Grid item xs={12}>
                        <CartItem item={item}/>
                    </Grid>
                </Grid>
            ))}
        </Grid>
    )
}
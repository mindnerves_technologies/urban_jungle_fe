import React, { useEffect } from 'react';
import { Grid, Typography } from '@material-ui/core';
import CartItem from './Item';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import {actions as cartActions} from '../../../redux/actions';

export default function WebItemDetails() {
    const dispatch = useDispatch();

    const store_id = useSelector((state) => state && state.storeInfo && state.storeInfo.store_id);
    const cartData = useSelector((state) => state && state.cart && state.cart.myCartData);

    useEffect(() => {
        dispatch(
            cartActions.getMyCartRequest({
                quote_id: 179594,
                store_id: store_id
            })
        )
    })

    return (
        <>
            <Grid container>
                <Grid item xs={12}>
                    <Grid container className="cart-table-container">
                        <Grid item xs={5}>
                            <Typography className="WebItemTitle-text">
                                <FormattedMessage id="Cart.ItemNumberStyle" defaultMessage="Item" />
                            </Typography>
                        </Grid>
                        <Grid item xs={7}>
                            <Grid container justify="space-between">
                                <Grid item xs={3} className="textAlignCenter">
                                    <Typography className="WebItemTitle-text textAlignCenter">
                                        <FormattedMessage id="Cart.Price" defaultMessage="Price" />
                                    </Typography>
                                </Grid>
                                <Grid item xs={4} className="textAlignCenter">
                                    <Typography className="WebItemTitle-text textAlignCenter">
                                        <FormattedMessage id="Cart.Quantity" defaultMessage="Quantity" />
                                    </Typography>
                                </Grid>
                                <Grid item xs={3} className="textAlignCenter">
                                    <Typography className="WebItemTitle-text textAlignCenter">
                                        <FormattedMessage id="Cart.Subtotal" defaultMessage="Subtotal" />
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            {cartData && cartData.products &&
                cartData.products.map((item, index) => (
                    <Grid container key={`basket_item_${index}`}>
                        <Grid item xs={12}>
                            <CartItem item={item} />
                        </Grid>
                    </Grid>
                ))}
        </>
    );
}

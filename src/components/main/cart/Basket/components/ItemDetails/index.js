import React from 'react';
import { Grid, Hidden } from '@material-ui/core';
import WebItemDetails from './components/web';
import MobileCartItemDetails from './components/mobile';

export default function ItemDetails() {

    return (
        <Grid container>
            {/** Web design */}
            <Hidden only={['sm', 'xs']}>
                <WebItemDetails />
            </Hidden>
            {/** Mobile design */}
            <Hidden only={['md', 'lg', 'xl']}>
                <MobileCartItemDetails />
            </Hidden>
        </Grid> 
    )
}
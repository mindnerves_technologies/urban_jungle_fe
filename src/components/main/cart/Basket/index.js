import React, { lazy, Suspense } from 'react';
import { Button, Grid, Hidden, Typography } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import './css/basket.css';
import CartData from './data.json';
import SimpleBreadcrumbs from '../../../common/Breadcrumbs';

const ItemDetails = lazy(() => import('./components/ItemDetails'));
const CartPaymentDetails = lazy(() => import('./components/CartDetails'));

export default function Basket() {
    const store_code = useSelector((state) => state && state.storeInfo && state.storeInfo.store_code);

    const breadCrumbs = [
        {
            name: <FormattedMessage id="Common.Home.Text" defaultMessage="Home" />,
            url: `/${store_code}/`,
        },
        {
            name: <FormattedMessage id="Cart.MyBasket" defaultMessage="Cart" />,
        },
    ];

    // React.useEffect(() => {
    //     try {
    //         dispatch(basketActions.getCartItemRequest({
    //             quote_id: global.quote_id,
    //             store_id: global.currentStore
    //         }));
    //     }catch(err){}
    // }, [dispatch, global.currentStore, global.quote_id])

    return (
        <Suspense fallback={''}>
            <Grid container justify="center" className="Cart">
                {CartData && (
                    <Grid item xs={11}>
                        <Grid container>
                            {/** Bread crumbs */}
                            <Grid item xs={12} md={12}>
                                <SimpleBreadcrumbs breadCrumbs={breadCrumbs} />
                            </Grid>

                            {CartData && CartData.data && (
                                <Grid item xs={12} className="textAlignCenter" style={{ textAlign: 'center' }}>
                                    <Typography className="title-cart">
                                        <FormattedMessage id="Cart.MyBasket" defaultMessage="My Basket" />
                                    </Typography>
                                </Grid>
                            )}

                            {CartData && CartData.data && (
                                <Grid item xs={12} md={12}>
                                    <Grid container>
                                        <Grid item xs={12} md={8}>
                                            <ItemDetails />
                                        </Grid>
                                        <Grid item xs={12} md={4}>
                                            <CartPaymentDetails />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            )}

                            {CartData && CartData.data && CartData.data.length > 0 && (
                                <Grid container justify="center">
                                    <Typography className="title-empty">
                                        <FormattedMessage id="Cart.empty" defaultMessage="Your basket is empty" />
                                    </Typography>
                                </Grid>
                            )}

                            <Hidden only={['xs', 'sm']}>
                                <Grid item xs={12} style={{ marginBottom: '20px' }}>
                                    <Button className="continue-shopping-button" style={{ width: '300px' }}>
                                        <FormattedMessage
                                            id="Cart.ContinueShopping"
                                            defaultMessage="Continue Shopping"
                                        />
                                    </Button>
                                </Grid>
                            </Hidden>
                        </Grid>
                    </Grid>
                )}
                {/* {CartData && CartData.data && <Grid item xs={12}>
                    <YouMayAlsoLike />
                </Grid>}
                {state && state.basket && state.basket.loader && 
                    <Spinner />
                } */}
            </Grid>
        </Suspense>
    );
}

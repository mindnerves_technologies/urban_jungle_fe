import { createAction } from 'redux-actions';

//action types
const GET_CART_ITEM_REQUEST = 'UJ/GET_CART_ITEM_REQUEST';
const GET_CART_ITEM_REQUEST_SUCCESS = 'UJ/GET_CART_ITEM_REQUEST_SUCCESS';
const GET_CART_ITEM_REQUEST_FAILED = 'UJ/GET_CART_ITEM_REQUEST_FAILED';

const UPDATE_CART_ITEM_QTY_REQUEST = 'UJ/UPDATE_CART_ITEM_QTY_REQUEST';
const UPDATE_CART_ITEM_QTY_REQUEST_SUCCESS = 'UJ/UPDATE_CART_ITEM_QTY_REQUEST_SUCCESS';
const UPDATE_CART_ITEM_QTY_REQUEST_FAILED = 'UJ/UPDATE_CART_ITEM_QTY_REQUEST_FAILED';

const REMOVE_CART_ITEM_REQUEST = 'UJ/REMOVE_CART_ITEM_REQUEST';
const REMOVE_CART_ITEM_REQUEST_SUCCESS = 'UJ/REMOVE_CART_ITEM_REQUEST_SUCCESS';
const REMOVE_CART_ITEM_REQUEST_FAILED = 'UJ/REMOVE_CART_ITEM_REQUEST_FAILED';

const SAVE_CART_COUNT_REQUEST = 'UJ/SAVE_CART_COUNT_REQUEST';
const GET_CART_COUNT_REQUEST = 'UJ/GET_CART_COUNT_REQUEST';

const GET_MY_CART = 'UJ/GET_MY_CART';
const GET_MY_CART_SUCCESS = 'UJ/GET_MY_CART_SUCCESS';
const GET_MY_CART_FAILED = 'UJ/GET_MY_CART_FAILED';

//action methods
const getCartItemRequest = createAction(GET_CART_ITEM_REQUEST);
const getCartItemRequestSuccess = createAction(GET_CART_ITEM_REQUEST_SUCCESS);
const getCartItemRequestFailed = createAction(GET_CART_ITEM_REQUEST_FAILED);

const updateCartItemQtyRequest = createAction(UPDATE_CART_ITEM_QTY_REQUEST);
const updateCartItemQtyRequestSuccess = createAction(UPDATE_CART_ITEM_QTY_REQUEST_SUCCESS);
const updateCartItemQtyRequestFailed = createAction(UPDATE_CART_ITEM_QTY_REQUEST_FAILED);

const removeCartItemRequest = createAction(REMOVE_CART_ITEM_REQUEST);
const removeCartItemRequestSuccess = createAction(REMOVE_CART_ITEM_REQUEST_SUCCESS);
const removeCartItemRequestFailed = createAction(REMOVE_CART_ITEM_REQUEST_FAILED);

const saveCartCountRequest = createAction(SAVE_CART_COUNT_REQUEST);
const getCartCountReq = createAction(GET_CART_COUNT_REQUEST);

const getMyCartRequest = createAction(GET_MY_CART);
const getMyCartRequestSuccess = createAction(GET_MY_CART_SUCCESS);
const getMyCartRequestFailed = createAction(GET_MY_CART_FAILED);

export const actions = {
    getCartItemRequest,
    getCartItemRequestSuccess,
    getCartItemRequestFailed,

    updateCartItemQtyRequest,
    updateCartItemQtyRequestSuccess,
    updateCartItemQtyRequestFailed,

    removeCartItemRequest,
    removeCartItemRequestSuccess,
    removeCartItemRequestFailed,

    saveCartCountRequest,
    getCartCountReq,

    getMyCartRequest,
    getMyCartRequestSuccess,
    getMyCartRequestFailed,
};

export const types = {
    GET_CART_ITEM_REQUEST,
    GET_CART_ITEM_REQUEST_SUCCESS,
    GET_CART_ITEM_REQUEST_FAILED,

    UPDATE_CART_ITEM_QTY_REQUEST,
    UPDATE_CART_ITEM_QTY_REQUEST_SUCCESS,
    UPDATE_CART_ITEM_QTY_REQUEST_FAILED,

    REMOVE_CART_ITEM_REQUEST,
    REMOVE_CART_ITEM_REQUEST_SUCCESS,
    REMOVE_CART_ITEM_REQUEST_FAILED,

    SAVE_CART_COUNT_REQUEST,
    GET_CART_COUNT_REQUEST,

    GET_MY_CART,
    GET_MY_CART_SUCCESS,
    GET_MY_CART_FAILED,
};

import { call, put, takeLatest } from 'redux-saga/effects';
import { actions, types } from './actions';
import api from '../api';
import { toast } from 'react-toastify';

const getCartDetailsReq = function* getCartDetailsReq({ payload }) {
    try {
        if(payload && payload.quote_id){
            const { data } = yield call(api.getCart, payload);
            if(data && data.status) {
                yield put(actions.saveCartCountRequest((data.data && data.data.cart_count) || 0 ));
                yield put(actions.getCartItemRequestSuccess(data.data));
            } else {
                toast.error((data && data.message) || "Something Went to wrong, Please try after sometime");
                yield put(actions.getCartItemRequestFailed());
            }
        } else {
            yield put(actions.getCartItemRequestSuccess(null));
        }
    } catch (err) {
        toast.error("Something Went to wrong, Please try after sometime");
        yield put(actions.getCartItemRequestFailed());
    }
}

const updateCartQtyReq = function* updateCartQtyReq({ payload }) {
    try {
        const { quote_id, store_id } = payload;
        const { data } = yield call(api.updateCartQty, payload);
        if(data && data.status) {
            yield put(actions.getCartItemRequest({
                quote_id,
                store_id
            }));
            toast.success(data.message);
        } else {
            toast.error((data && data.message) ||"Something Went to wrong, Please try after sometime");
            yield put(actions.updateCartItemQtyRequestFailed())
        }
    } catch (err) {
        toast.error("Something Went to wrong, Please try after sometime");
        yield put(actions.updateCartItemQtyRequestFailed())
    }
}

const removeCartItemReq = function* removeCartItemReq({ payload }) {
    try {
        const { quote_id, store_id, sku } = payload;
        const { data } = yield call(api.removeCartItem, { quote_id, sku });
        if(data && data.status) {
            yield put(actions.getCartItemRequest({
                quote_id,
                store_id
            }));
            toast.success(data.message);
        } else {
            toast.error((data && data.message) ||"Something Went to wrong, Please try after sometime");
            yield put(actions.removeCartItemRequestFailed())
        }
    } catch (err) {
        toast.error("Something Went to wrong, Please try after sometime");
        yield put(actions.removeCartItemRequestFailed())
    }
}

const getCartCountReq = function* getCartCountReq({ payload }) {
    try {
        if(payload.quote_id){
            const { data } = yield call(api.getCartCount, payload);
            if(data && data.status) {
                yield put(actions.saveCartCountRequest(data.count));
            }
        }
    } catch(err) {
        toast.error("Something Went to wrong, Please try after sometime");  
    }
}

const getMyCartReq = function* getMyCartReq({ payload }) {
    try {
        const { quote_id, store_id } = payload;
        const data = yield call(api.getMyCart, {
            quote_id: quote_id,
            store_id: store_id
        });
        console.log('!!!!!!!!!!!' + JSON.stringify(data));
        if(data && data.status){
            yield put(actions.getMyCartRequestSuccess(data.data.data));
        } else {
            yield put(actions.getMyCartRequestSuccess(null));
        }
    } catch (err) {
        toast.error("Something Went to wrong, Please try after sometime");
        yield put(actions.getCartItemRequestFailed());
    }
}

export default function* sagas() {
    yield takeLatest(types.GET_CART_ITEM_REQUEST, getCartDetailsReq)
    yield takeLatest(types.UPDATE_CART_ITEM_QTY_REQUEST, updateCartQtyReq)
    yield takeLatest(types.REMOVE_CART_ITEM_REQUEST, removeCartItemReq)
    yield takeLatest(types.GET_CART_COUNT_REQUEST, getCartCountReq)
    yield takeLatest(types.GET_MY_CART, getMyCartReq)
}
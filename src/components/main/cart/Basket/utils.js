import React from 'react';
import { Typography } from '@material-ui/core';
import {IoMdClose} from 'react-icons/io';

export const getSubTotalAmount = (item, removeProducts) => {
    return (
        <>
            {item && item.special_price ? 
                <Typography className="cartSubTotal">
                    {`${item && item.currency} ${item && item.special_price && parseFloat(item.special_price * item.qty).toFixed(2)}`}
                    <IoMdClose onClick={() => removeProducts(item)}/>
                </Typography>
                :
                <Typography className="cartSubTotal">
                    {`${item && item.currency} ${item && item.price && parseFloat(item.price * item.qty).toFixed(2)}`}
                    <IoMdClose />
                </Typography>
            }
        </>
    )
}

export const getCartAmount = (item) => {
    return (
        <>
            {item && item.special_price ?
                <> 
                    <Typography className="cart-showing-price">
                        {`${item && item.currency} ${item && item.special_price}`}
                    </Typography>
                    <Typography className="cart-showing-price-strik">
                        {`${item && item.currency} ${item && item.price}`}
                    </Typography>
                </>
                :
                <Typography className="cart-showing-price">
                    {`${item && item.currency} ${item && item.price}`}
                </Typography>
            }
        </>
    )

}

export const getCartAmountMobile = (item) => {
    return (
        <>
            {item && item.special_price ?
                <> 
                    <Typography className="ar-right">
                        <span className="cart-showing-price">{`${item && item.currency} ${item && item.special_price}`}</span>
                        <span className="cart-showing-price-strik">{`${item && item.currency} ${item && item.price}`}</span>
                    </Typography>
                </>
                :
                <Typography className="cart-showing-price ar-right">
                    {`${item && item.currency} ${item && item.price}`}
                </Typography>
            }
        </>
    )
}

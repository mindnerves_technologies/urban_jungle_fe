import React from 'react';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import { Link } from 'react-router-dom';
import Slider from 'react-slick';
// import '../css/home.css';

export default function HomeBlock1({ homeDetails }) {
    const BannerSettings = {
        autoplay: true,
        lazyLoad: true,
        autoplaySpeed: 5000,
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 500,
        vertical: false,
    };

    return (
        <Grid item xs={12} className="webBanner">
            <Slider {...BannerSettings}>
                {homeDetails &&
                    homeDetails.banners &&
                    homeDetails.banners.map((item, index) => (
                        <Link to={item.banner_url} key={`homepage_banner_${index}`}>
                            <div>
                                <Hidden only={['xs', 'sm', 'md']}>
                                    <img src={item.banner_image} className="banner-image-hight" alt="" />
                                </Hidden>
                                <Hidden only={['lg', 'xl']}>
                                    <img src={item.banner_mobile_image} className="banner-image-hight" alt="" />
                                </Hidden>
                            </div>
                        </Link>
                    ))}
            </Slider>
        </Grid>
    );
}

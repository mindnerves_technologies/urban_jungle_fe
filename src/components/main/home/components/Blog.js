import React from 'react';
import Grid from '@material-ui/core/Grid';
import { Typography, Hidden } from '@material-ui/core';
// import { Link } from 'react-router-dom';
export default function Blog({ homeDetails }) {
    const blog_blocks = homeDetails && homeDetails.blog_blocks;

    return (
        <Grid item xs={12}>
            <div className="blog">
                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        {blog_blocks && blog_blocks.blog_header && blog_blocks.blog_header.blog_header && (
                            <Typography className="subtitle-text">{blog_blocks.blog_header.blog_header}</Typography>
                        )}
                    </Grid>
                    <Grid item xs={6} className="center-right">
                        <a href="/" className="view-all">
                            View all
                        </a>
                    </Grid>
                </Grid>
                <Grid container direction="row" justify="space-between" alignItems="center" spacing={3}>
                    {blog_blocks &&
                        blog_blocks.blog_list &&
                        blog_blocks.blog_list.length > 0 &&
                        blog_blocks.blog_list.map((blog, index) => (
                            <Grid item xs={6} md={3} key={`blog${index}`}>
                                <div className="circle-image-div">
                                    <div className="digit">
                                        <Typography className="digit-num border-bt">
                                            {blog.blog_date.split(':')[0]}
                                        </Typography>
                                        <Typography className="digit-num">{blog.blog_date.split(':')[1]}</Typography>
                                    </div>
                                    <a href={blog.blog_url}>
                                        <div className="image-div">
                                            <Hidden only={['sm', 'xs']}>
                                                <img src={blog.blog_image} alt="wishlistCircle" />
                                            </Hidden>
                                            <Hidden only={['md', 'lg', 'xl']}>
                                                <img src={blog.blog_mobile_image} alt="wishlistCircle" />
                                            </Hidden>
                                        </div>
                                        <Typography className="image-title">{blog.blog_title}</Typography>
                                    </a>
                                </div>
                            </Grid>
                        ))}
                </Grid>
            </div>
        </Grid>
    );
}

import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import { Hidden } from '@material-ui/core';
import { Button } from '@material-ui/core';

export default function LogoSection({ homeDetails }) {
    let logos = homeDetails && homeDetails.brand_logo_list;

    const [viewAll, setViewAll] = useState(false);

    return (
        <Grid item xs={12}>
            <div className="logo-section">
                <Grid container direction="row" justify="space-between" alignItems="center">
                    <Grid item xs={12} md={12}>
                        <Hidden only={['xs', 'sm']}>
                            <div className="logo-div">
                                {logos &&
                                    logos.map((logo, index) => {
                                        return (
                                            <div key={index}>
                                                <a href={logo.brand_logo_url}>
                                                    <img alt="" src={logo.brand_logo_image} />
                                                </a>
                                            </div>
                                        );
                                    })}
                            </div>
                        </Hidden>
                        <Hidden only={['md', 'lg', 'xl']}>
                            <div className="logo-div">
                                {logos &&
                                    logos.map((logo, index) => {
                                        if (!viewAll && index > 11) {
                                            return null;
                                        }
                                        return (
                                            <div key={index}>
                                                <a href={logo.brand_logo_url}>
                                                    <img alt="" src={logo.brand_logo_image} />
                                                </a>
                                            </div>
                                        );
                                    })}
                            </div>
                        </Hidden>
                    </Grid>
                </Grid>
                <Hidden only={['md', 'lg', 'xl']}>
                    {!viewAll && (
                        <Grid container justify="center" alignItems="center">
                            <Button className="view-button" onClick={() => setViewAll(true)}>
                                View all
                            </Button>
                        </Grid>
                    )}
                </Hidden>
            </div>
        </Grid>
    );
}

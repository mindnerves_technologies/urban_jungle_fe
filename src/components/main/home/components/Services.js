import React from 'react';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';

export default function ServicesSection({ homeDetails }) {
    let services = homeDetails && homeDetails.facility_block_list;

    return (
        <Grid item xs={12}>
            <div className="logo-section services-section">
                <Grid container direction="row" justify="space-between" alignItems="center">
                    {services &&
                        services.map((service, index) => {
                            return (
                                <Grid item xs={4} md={2} key={index}>
                                    <div
                                        className={
                                            (index === services.length - 1 ? 'last' :  index === 2 ? 'third first' : index < 2 ? "first" : "") + ' section-inner '
                                        }
                                    >
                                        <a href={service.facility_block_url}>
                                            <img alt="" src={service.facility_block_image} className="img-65" />
                                        </a>
                                        <Typography className="brandTitle">{service.facility_block_title}</Typography>
                                        <div className="extraBlock"></div>
                                    </div>
                                </Grid>
                            );
                        })}
                </Grid>

                <Grid container direction="row" justify="space-between" alignItems="center">
                    <Grid item xs={12} md={12}>
                        <div className="title center">
                            <Typography className="freeShipping">*Free Shipping</Typography>
                        </div>
                    </Grid>
                </Grid>
                <Grid container direction="row" justify="center" alignItems="center">
                    <Grid item xs={4} md={3}>
                        <div className="spec">
                            <Typography className="text">*In europe for orders of at least $ 120</Typography>
                        </div>
                    </Grid>
                    <Grid item xs={4} md={3}>
                        <div className="spec">
                            <Typography className="text">*In Italy for orders of at least $ 50</Typography>
                        </div>
                    </Grid>
                    <Grid item xs={4} md={3}>
                        <div className="spec">
                            <Typography className="text">*For smaller amounts the cost is $ 4.99</Typography>
                        </div>
                    </Grid>
                </Grid>
            </div>
        </Grid>
    );
}

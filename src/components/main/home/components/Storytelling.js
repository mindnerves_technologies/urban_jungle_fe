import React from 'react';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import { Link } from 'react-router-dom';
import Slider from 'react-slick';
// import '../css/home.css';

export default function StorytellingSlider({ homeDetails }) {
    const BannerSettings = {
        autoplay: true,
        lazyLoad: true,
        autoplaySpeed: 5000,
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 500,
        vertical: false,
    };

    return (
        <Grid item xs={12} className="webBanner">
            <Slider {...BannerSettings}>
                {homeDetails &&
                    homeDetails.storytelling_blocks &&
                    homeDetails.storytelling_blocks.storytelling_list &&
                    homeDetails.storytelling_blocks.storytelling_list.map((item, index) => (
                        <Link to={item.storytelling_url} key={`storytelling_banner_${index}`}>
                            <div key={index}>
                                <Hidden only={['xs', 'sm', 'md']}>
                                    <img src={item.storytelling_image} className="banner-image-hight" alt="" />
                                </Hidden>
                                <Hidden only={['lg', 'xl']}>
                                    <img src={item.storytelling_mobile_image} className="banner-image-hight" alt="" />
                                </Hidden>
                            </div>
                        </Link>
                    ))}
            </Slider>
        </Grid>
    );
}

import React from 'react';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';

export default function VideoBlock({ homeDetails }) {
    const brand_blocks = homeDetails && homeDetails.brand_blocks;

    return (
        <Grid item xs={12}>
            {brand_blocks && brand_blocks.length > 0 && (
                <div className="video-block">
                    <Grid container direction="row" justify="space-between" alignItems="center">
                        <Grid item xs={12} md={4}>
                            <div className="image-box">
                                <Link to={`${brand_blocks[0].brand_url}`}>
                                    <img alt="" src={brand_blocks[0].brand_image} className="img-box-item" />
                                </Link>
                            </div>
                        </Grid>
                        <Grid item xs={12} md={4}>
                            <div className="image-box">
                                <Link to={`${brand_blocks[1].brand_url}`}>
                                    <img alt="" src={brand_blocks[1].brand_image} className="img-box-item" />
                                </Link>
                            </div>
                        </Grid>
                        <Grid item xs={12} md={4}>
                            <div className="image-box">
                                <Link to={`${brand_blocks[2].brand_url}`}>
                                    <img alt="" src={brand_blocks[2].brand_image} className="img-box-item" />
                                </Link>
                            </div>
                        </Grid>
                    </Grid>
                </div>
            )}
        </Grid>
    );
}

import React from 'react';
import { Grid, IconButton, Typography } from '@material-ui/core';
import { IoCopyOutline } from 'react-icons/io5';
import { useHistory } from 'react-router';
import { useSelector } from 'react-redux';

const Card = ({ setIsOpen, data, setProductDetail }) => {
    const history = useHistory();

    const store_code = useSelector((state) => state && state.storeInfo && state.storeInfo.store_code);

    const onProductPopupOpen = (ProductDetail) => {
        setProductDetail(ProductDetail);
        setIsOpen(true);
    };

    const redirectToPDP = (url) => {
        return history.push(`/${store_code}/${url}`);
    };

    const getPrices = () => {
        if (
            data &&
            data.price &&
            data.price.specialprice &&
            data.price.specialprice !== 0 &&
            data.price.specialprice !== ''
        ) {
            return (
                <>
                    <label className="price">
                        {`${data && data.currency} ${data && parseFloat(data.price.specialprice).toFixed(2)}`}{' '}
                    </label>
                    <label className="price_stickout">{`${data && data.currency} ${
                        data && parseFloat(data.price.price).toFixed(2)
                    }`}</label>
                    <label className="offer">
                        {Math.round(
                            (parseFloat(data.price.price - data.price.specialprice) / parseFloat(data.price.price)) *
                                100,
                        )}
                        % off
                    </label>
                </>
            );
        } else {
            return (
                <>
                    <label className="product-price">
                        {`${data && data.currency} ${
                            (data && data.price && parseFloat(data.price.price).toFixed(2)) || 0
                        }`}{' '}
                    </label>
                </>
            );
        }
    };

    return (
        <Grid className="card-container">
            <Grid style={{ margin: '8px', cursor: 'pointer' }}>
                <IconButton
                    edge="end"
                    color="default"
                    aria-label="open drawer"
                    style={{ right: '20px', position: 'absolute' }}
                    onClick={() => onProductPopupOpen(data)}
                >
                    <IoCopyOutline style={{ width: '18px', height: '18px', color: '#000000' }} />
                </IconButton>
                <Grid className="image-container" onClick={() => redirectToPDP(data.url_key)}>
                    {data &&
                        data.productImageUrl &&
                        data.productImageUrl &&
                        data.productImageUrl.thumbnail &&
                        data.productImageUrl.thumbnail.length > 0 && (
                            <img
                                src={
                                    data.productImageUrl &&
                                    data.productImageUrl.thumbnail &&
                                    data.productImageUrl.thumbnail[0]
                                }
                                alt={data.name}
                            />
                        )}
                </Grid>
                <Grid className="product-detail-container" onClick={() => redirectToPDP(data.url_key)}>
                    <Typography className="product-name" component="p">
                        {data.name}
                    </Typography>
                    <Typography className="product-description" component="p">
                        {data.name}
                    </Typography>
                    {getPrices()}
                </Grid>
            </Grid>
        </Grid>
    );
};
export default Card;

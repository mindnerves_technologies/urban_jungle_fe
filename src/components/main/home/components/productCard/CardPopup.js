import React from 'react';
import { Button, Dialog, Grid, IconButton, Typography } from '@material-ui/core';
import Slider from 'react-slick';
import { IoCloseCircle } from 'react-icons/io5';
import { useHistory } from 'react-router';
import { useSelector } from 'react-redux';

const CardPopup = ({ isOpen, setIsOpen, productDetail }) => {
    const history = useHistory();
    const store_code = useSelector((state) => state && state.storeInfo && state.storeInfo.store_code);
    const settings = {
        dots: true,
        arrows: false,
        speed: 800,
        slidesToShow: 1,
        lazyLoad: true,
        slidesToScroll: 1,
        centerMode: false,
        autoplay: true,
        autoplaySpeed: 2000,
    };
    const iconButton = {
        position: 'absolute',
        top: 0,
        right: 0,
        zIndex: 10,
        color: '#000',
    };

    const getPrices = () => {
        const data = productDetail;
        if (
            data &&
            data.price &&
            data.price.specialprice &&
            data.price.specialprice !== 0 &&
            data.price.specialprice !== ''
        ) {
            return (
                <p style={{ textAlign: 'center' }}>
                    <label className="price">
                        {`${data && data.currency} ${data && parseFloat(data.price.specialprice).toFixed(2)}`}{' '}
                    </label>
                    <label className="price_stickout">{`${data && data.currency} ${
                        data && parseFloat(data.price.price).toFixed(2)
                    }`}</label>
                    <label className="offer">
                        {Math.round(
                            (parseFloat(data.price.price - data.price.specialprice) / parseFloat(data.price.price)) *
                                100,
                        )}
                        % off
                    </label>
                </p>
            );
        } else {
            return (
                <>
                    <p className="product-price" style={{ textAlign: 'center' }}>
                        {`${data && data.currency} ${
                            (data && data.price && parseFloat(data.price.price).toFixed(2)) || 0
                        }`}{' '}
                    </p>
                </>
            );
        }
    };
    return (
        <Dialog
            onClose={() => setIsOpen(false)}
            className="product-cart-Popup"
            aria-labelledby="customized-dialog-title"
            open={isOpen}
        >
            <IconButton aria-label="close" style={iconButton} onClick={() => setIsOpen(false)}>
                <IoCloseCircle />
            </IconButton>
            <Grid container justify="center">
                <div style={{ width: '80%' }}>
                    <Slider {...settings}>
                        {productDetail &&
                            productDetail.productImageUrl &&
                            productDetail.productImageUrl.thumbnail &&
                            productDetail.productImageUrl.thumbnail.map((url, index) => (
                                <img key={`img${index}`} src={url} alt={productDetail.name} />
                            ))}
                    </Slider>
                </div>
            </Grid>
            <Grid container className="card-container" justify="center">
                <Grid className="product-detail-container">
                    <Typography component="p" className="product-name" style={{ textAlign: 'center' }}>
                        {productDetail.name}
                    </Typography>
                    <Typography component="p" className="product-description" style={{ textAlign: 'center' }}>
                        {productDetail.name}
                    </Typography>
                    {getPrices()}
                    <Button
                        className="view-product-button"
                        onClick={() => {
                            history.push(`/${store_code}/${productDetail.url_key}`);
                        }}
                    >
                        View product
                    </Button>
                </Grid>
            </Grid>
        </Dialog>
    );
};
export default CardPopup;

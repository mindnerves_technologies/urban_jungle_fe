import React, { useState } from 'react';
import Card from './Card';
import Slider from 'react-slick';
import CardPopup from './CardPopup';

const ProductSlider = ({ data }) => {
    const [width, setWidth] = useState(window.innerWidth);
    const [isOpen, setIsOpen] = useState(false);
    const [productDetail, setProductDetail] = useState({});

    const settings = {
        rows: 1,
        dots: false,
        arrows: false,
        infinite: true,
        speed: 800,
        slidesToShow: 5,
        lazyLoad: true,
        slidesToScroll: 1,
        centerMode: false,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    rows: 1,
                    slidesToShow: 4,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 1024,
                settings: {
                    rows: 1,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    rows: 1,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    rows: 1,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true,
                },
            },
        ],
    };
    const slider2Settings = {
        rows: 2,
        dots: false,
        arrows: true,
        infinite: true,
        speed: 800,
        slidesToShow: 2,
        slidesToScroll: 1,
        lazyLoad: true,
        // centerMode: true,
        autoplay: true,
        centerPadding: '0',
        autoplaySpeed: 5000,
        // responsive: [
        //     {
        //         breakpoint: 600,
        //         settings: {
        //             rows: 2,
        //             slidesToShow: 2,
        //             slidesToScroll: 1,
        //         },
        //     },
        //     {
        //         breakpoint: 400,
        //         settings: {
        //             rows: 2,
        //             slidesToShow: 2,
        //             slidesToScroll: 1,
        //         },
        //     },
        // ],
    };
    window.addEventListener('resize', () => {
        setWidth(window.innerWidth);
    });
    return (
        <div style={{ width: '100%' }} className="mobile_slider_height">
            {width > 600 ? (
                <Slider {...settings}>
                    {data &&
                        data.map((product, index) => (
                            <div key={`product${index}`}>
                                <Card data={product} setIsOpen={setIsOpen} setProductDetail={setProductDetail} />
                            </div>
                        ))}
                </Slider>
            ) : (
                <Slider {...slider2Settings}>
                    {data &&
                        data.map((product, index) => (
                            <div key={`product-min-width${index}`}>
                                <Card data={product} setIsOpen={setIsOpen} setProductDetail={setProductDetail} />
                            </div>
                        ))}
                </Slider>
            )}
            <CardPopup isOpen={isOpen} productDetail={productDetail} setIsOpen={setIsOpen} />
        </div>
    );
};
export default ProductSlider;

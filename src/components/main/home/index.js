import { Button, Grid, Typography } from '@material-ui/core';
import React, { Suspense, useLayoutEffect } from 'react';
import ProductSlider from './components/productCard';
import HomeBlock1 from './components/Block1';
import Blog from './components/Blog';
import StorytellingSlider from './components/Storytelling';
import VideoBlock from './components/VideoBlock';
import Services from './components/Services';
import LogoSection from './components/LogoSection';
import { actions as HomepageActions } from './redux/actions';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import Loader from '../../common/Loader';
import InstagramFeed from 'react-ig-feed';
import 'react-ig-feed/dist/index.css';

const instagramToken = process.env.REACT_APP_INSTAGRAM_ACCESS_TOKEN;
const HomePage = () => {
    const dispatch = useDispatch();

    const homeDetails = useSelector((state) => state && state.home && state.home.homepage && state.home.homepage);
    const bannerCardData = useSelector(
        (state) => state && state.home && state.home.homepage && state.home.homepage && state.home.homepage.blocks,
    );
    const newArriwalArr = useSelector((state) => state && state.home && state.home.newArriwal && state.home.newArriwal);
    const loader = useSelector((state) => state && state.home && state.home.loading);
    const store_id = useSelector((state) => state && state.storeInfo && state.storeInfo.store_id);

    useLayoutEffect(() => {
        dispatch(
            HomepageActions.getHomepageRequest({
                store_id: store_id,
            }),
        );
        dispatch(HomepageActions.getNewArriwalRequest(`category_id=${309}&store_id=${store_id}&count=${6}`));
    }, [dispatch, store_id]);

    return (
        <div id="homepage">
            {!loader && (
                <>
                    <Grid container>
                        <>
                            <Suspense
                                fallback={
                                    <Grid container justify="center">
                                        Loading....
                                    </Grid>
                                }
                            >
                                <HomeBlock1 homeDetails={homeDetails} />

                                <Grid className="carousel-container" style={{ width: '85%', margin: '30px auto' }}>
                                    <Grid className="new-arrival-section">
                                        <Grid container justify="space-between">
                                            <Typography className="subtitle-text" variant="h3">
                                                new
                                                <br />
                                                arrivals
                                            </Typography>
                                            <div className="new-arrival">
                                                <Typography className="view-all-btn" component="a">
                                                    View all
                                                </Typography>
                                            </div>
                                        </Grid>
                                    </Grid>
                                    <ProductSlider data={newArriwalArr} />
                                </Grid>

                                <Grid container spacing={3} className="banner-card-container">
                                    {bannerCardData &&
                                        bannerCardData.map((item, index) => (
                                            <Grid key={index} item className="inner-container">
                                                <Grid className="image-container">
                                                    <img src={item.block_image} alt="blockImage" />
                                                </Grid>
                                                <Grid className="product-detail-container">
                                                    <Typography className="banner-card-title" variant="h4">
                                                        {item.block_title}
                                                    </Typography>
                                                    <Typography className="banner-card-discription" component="p">
                                                        {item.block_description}
                                                    </Typography>
                                                    <Grid>
                                                        <Link to={item.block_url}>
                                                            <Button className="view-button" variant="contained">
                                                                View
                                                            </Button>
                                                        </Link>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        ))}
                                </Grid>

                                <Grid className="storytelling">
                                    <Typography
                                        className="subtitle-text"
                                        variant="h3"
                                        style={{ marginBottom: '1.2rem' }}
                                    >
                                        {homeDetails &&
                                            homeDetails.storytelling_blocks &&
                                            homeDetails.storytelling_blocks['storytelling_header'].storytelling_header}
                                    </Typography>

                                    <StorytellingSlider homeDetails={homeDetails} />
                                </Grid>
                            </Suspense>
                        </>
                    </Grid>
                    <VideoBlock homeDetails={homeDetails} />
                    <Blog homeDetails={homeDetails} />
                    <LogoSection homeDetails={homeDetails} />
                    <Grid item xs={12}>
                        <div className="blog">
                            <Typography className="subtitle-text">Instagram</Typography>
                            <InstagramFeed token={instagramToken} counter="12" />
                        </div>
                    </Grid>
                    <Services homeDetails={homeDetails} />
                </>
            )}
            {loader && <Loader />}
        </div>
    );
};

export default HomePage;

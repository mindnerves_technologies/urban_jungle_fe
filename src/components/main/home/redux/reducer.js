import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandlers = {
    [types.GET_HOMEPAGE_REQUEST]: (state) => ({
        ...state,
        loading: true,
    }),
    [types.GET_HOMEPAGE_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        loading: false,
        homepage: payload || {},
    }),
    [types.GET_HOMEPAGE_REQUEST_FAILED]: (state) => ({
        ...state,
        loading: false,
    }),

    [types.GET_NEW_ARRIWAL_REQUEST]: (state) => ({
        ...state,
        loading: true,
    }),
    [types.GET_NEW_ARRIWAL_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        loading: false,
        newArriwal: payload || [],
    }),
    [types.GET_NEW_ARRIWAL_REQUEST_FAILED]: (state) => ({
        ...state,
        loading: false,
    }),
};

export default handleActions(actionHandlers, {
    loading: false,
    homepage: {},
    newArriwal: []
});

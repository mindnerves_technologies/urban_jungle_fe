import { call, put, takeLatest } from 'redux-saga/effects';
import { actions, types } from './actions';
import api from '../../../../api';

const getHomePageReq = function* getHomePageReq({ payload }) {
    try {
        const { data } = yield call(api.getHomepage, payload);
        if (data && data.status) {
            yield put(actions.getHomepageRequestSuccess(data && data.data));
        } else {
            yield put(actions.getHomepageRequestFailed());
        }
    } catch (err) {
        yield put(actions.getHomepageRequestFailed());
    }
};

const getNewArriwal = function* getNewArriwalReq({ payload }) {
    try {
        const { data } = yield call(api.getNewArriwal, payload);
        if (data && data.status) {
            yield put(actions.getNewArriwalRequestSuccess(data && data.product));
        } else {
            yield put(actions.getNewArriwalRequestFailed());
        }
    } catch (err) {
        yield put(actions.getNewArriwalRequestFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.GET_HOMEPAGE_REQUEST, getHomePageReq);
    yield takeLatest(types.GET_NEW_ARRIWAL_REQUEST, getNewArriwal);
}

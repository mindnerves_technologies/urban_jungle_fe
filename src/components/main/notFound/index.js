import { Button, Grid, Hidden } from '@material-ui/core';
import React from 'react';
import Web_b from '../../../assets/notFound/web_b1.png';
import mobile_b from '../../../assets/notFound/mobile_b.png';
import userIcon from '../../../assets/notFound/user.png';
import { useHistory } from 'react-router';
import { useSelector } from 'react-redux';

const NotFoundPage = () => {
    const history = useHistory();

    const store_code = useSelector((state) => state && state.storeInfo && state.storeInfo.store_code);

    const redirectToHome = () => {
        return history.push(`/${store_code}`);
    };
    return (
        <Grid container className="not_found_page">
            <Hidden only={['sm', 'xs']}>
                <Grid item xs={12} style={{ backgroundImage: `url(${Web_b})` }} className="not_found_page_background">
                    <Grid container justify="center">
                        <label className="top_label">Look like you’re lost in space</label>
                        <Grid item xs={12}>
                            <h1>
                                404
                                <img src={userIcon} alt="user" />
                            </h1>
                        </Grid>
                        <Grid item xs={12}>
                            <Button onClick={redirectToHome}>Go to Homepage</Button>
                        </Grid>
                    </Grid>
                </Grid>
            </Hidden>
            <Hidden only={['md', 'lg', 'xl']}>
                <Grid
                    item
                    xs={12}
                    style={{ backgroundImage: `url(${mobile_b})` }}
                    className="not_found_page_background"
                >
                    <Grid container justify="center">
                        <label className="top_label">Look like you’re lost in space</label>
                        <Grid item xs={12}>
                            <h1>
                                404
                                <img src={userIcon} alt="user" />
                            </h1>
                        </Grid>
                        <Grid item xs={12}>
                            <Button onClick={redirectToHome}>Go to Homepage</Button>
                        </Grid>
                    </Grid>
                </Grid>
            </Hidden>
        </Grid>
    );
};

export default NotFoundPage;

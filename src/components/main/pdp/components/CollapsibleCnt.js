import React from 'react';
import Collapsible from 'react-collapsible';
import { IoMdArrowDropdown, IoMdArrowDropup } from 'react-icons/io';
import ReactHtmlParser from 'react-html-parser';

const CollapsibleCnt = (props) => {
    const { cName, product, keyName, open } = props;

    return (
        <div className="pdp-desc-section" id={`${keyName}_pdp`}>
            <div className="pdp-collapse">
                <Collapsible
                    trigger={
                        <div className="pdp-collapse-top">
                            <div className="desc-heading">{cName}</div>
                            <IoMdArrowDropdown />
                        </div>
                    }
                    triggerWhenOpen={
                        <div className="pdp-collapse-top">
                            <div className="desc-heading">{cName}</div>
                            <IoMdArrowDropup />
                        </div>
                    }
                    open={open}
                >
                    <div className="answer-text pdp-desc-text">
                        {keyName === 'description' && (
                            <>
                                <p>{product && product.short_description}</p>
                                <p className="title">Product ID</p>
                                <p className="subValue">{product && product.sku}</p>
                            </>
                        )}
                        {keyName === 'Shipping' && <>{ReactHtmlParser(product && product.shipping_return)}</>}
                        {keyName === 'Support' && <>{ReactHtmlParser(product && product.support)}</>}
                    </div>
                </Collapsible>
            </div>
        </div>
    );
};

export default CollapsibleCnt;

import React from 'react';
import Slider from 'react-slick';
import { FiCopy } from 'react-icons/fi';
import p1 from '../../../../assets/pdp/p1.png';
import { Grid, Hidden } from '@material-ui/core';

function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return <div className={className} style={{ ...style, display: 'block', background: 'white' }} onClick={onClick} />;
}

function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={className}
            style={{ ...style, display: 'block', background: 'white', color: '#00000' }}
            onClick={onClick}
        />
    );
}

const MightLink = (props) => {
    const { productData } = props;
    var settings = {
        rows: 1,
        dots: false,
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 4,
        autoplaySpeed: 2000,
        initialSlide: 0,
        cssEase: 'linear',
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    rows: 1,
                    slidesToShow: 4,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 1024,
                settings: {
                    rows: 1,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    rows: 1,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: false,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    rows: 1,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: false,
                },
            },
        ],
    };

    var settings1 = {
        rows: 1,
        dots: false,
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplaySpeed: 2000,
        initialSlide: 0,
        cssEase: 'linear',
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />,
    };

    return (
        <>
            <Hidden only={['xs', 'sm']}>
                <Slider {...settings}>
                    {productData &&
                        productData.length > 0 &&
                        productData.map((product, index) => {
                            return (
                                <Grid container className="click-card-cnt">
                                    <div className="slick-card">
                                        <div className="cpy-i">
                                            <FiCopy />
                                        </div>
                                        <img src={p1} alt="" />
                                    </div>
                                    <div className="slick-card-info">
                                        <p className="product_name">{product.title}</p>
                                        <p className="sub-title">{product.subtitle}</p>
                                        <p className="product_price">$ {product.price}</p>
                                    </div>
                                </Grid>
                            );
                        })}
                </Slider>
            </Hidden>
            <Hidden only={['md', 'lg', 'xl']}>
                <Slider {...settings1}>
                    {productData &&
                        productData.length > 0 &&
                        productData.map((product, index) => {
                            return (
                                <div className="product_image_container1">
                                    <div className="product_image_container1_div">
                                        <img src={p1} alt="productImage" />
                                    </div>
                                    <div className="slick-card-info">
                                        <p className="product_name">{product.title}</p>
                                        <p className="sub-title">{product.subtitle}</p>
                                        <p className="product_price">$ {product.price}</p>
                                    </div>
                                </div>
                            );
                        })}
                </Slider>
            </Hidden>
        </>
    );
};

export default MightLink;

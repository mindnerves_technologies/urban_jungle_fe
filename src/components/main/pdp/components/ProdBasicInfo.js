import React, { useState } from 'react';
import { AiOutlineHeart } from 'react-icons/ai';
import { HiShare } from 'react-icons/hi';
import { Button, CircularProgress, Drawer, Grid, Hidden, IconButton } from '@material-ui/core';
import CollapsibleCnt from './CollapsibleCnt';
import ShareModal from '../modal/ShareModal';
import SizeSection from './SizeSection';
import UJAdvantage from './ujAdvantage';
import { toast } from 'react-toastify';
import { actions as PDPActions } from '../redux/actions';
import { useDispatch, useSelector } from 'react-redux';

const ProdBasicInfo = ({ product }) => {
    const dispatch = useDispatch();

    const guest_user = useSelector((state) => state && state.storeInfo && state.storeInfo.guest_user);
    const loginUser = useSelector((state) => state && state.storeInfo && state.storeInfo.loginUser);
    const cartLoading = useSelector((state) => state && state.PDP && state.PDP.cartLoading);

    const [isShare, setIsShare] = useState(false);
    const [size, setSize] = useState(null);
    const [sizeType, setSizeType] = useState(null);
    const [sizeData, setSizeData] = useState(null);
    const [openDrawer, setOpenDrawer] = useState(false);

    const handleOpen = () => {
        setIsShare(true);
    };
    const handleShare = (resp) => {
        resp ? setIsShare(true) : setIsShare(false);
    };

    const getPrices = () => {
        if (product && product.special_price && product.special_price !== 0 && product.special_price !== '') {
            return (
                <>
                    <label className="price">
                        {`${product && product.currency} ${product && parseFloat(product.special_price).toFixed(2)}`}{' '}
                    </label>
                    <label className="price_stickout">{`${product && product.currency} ${
                        product && parseFloat(product.price).toFixed(2)
                    }`}</label>
                    <label className="offer">
                        {Math.round(
                            (parseFloat(product.price - product.special_price) / parseFloat(product.price)) * 100,
                        )}
                        % off
                    </label>
                </>
            );
        } else {
            return (
                <>
                    <label className="price">
                        {`${product && product.currency} ${product && parseFloat(product.price).toFixed(2)}`}{' '}
                    </label>
                </>
            );
        }
    };

    const addToCart = () => {
        if (!size || size === '' || !sizeType || sizeType === '') {
            toast.warning('Please select size');
            return;
        }

        dispatch(
            PDPActions.addToCartReq({
                payloadData: {
                    cartItem: {
                        sku: product.sku,
                        qty: 1,
                        quote_id: loginUser ? loginUser.quote_id : guest_user && guest_user.guestId,
                        product_option: {
                            extension_attributes: {
                                configurable_item_options: [
                                    {
                                        option_id: sizeData && sizeData.sizeData && sizeData.sizeData.option_id,
                                        option_value: sizeData && sizeData.sizeData && sizeData.sizeData.option_value,
                                    },
                                ],
                            },
                        },
                        extension_attributes: { size_type: sizeType, size_value: size },
                    },
                },
                guestId: guest_user && guest_user.guestId,
                loginUser: loginUser,
            }),
        );
        toggleDrawer(false);
    };

    const onClicklink = () => {
        const divPosition = document.getElementById('description_pdp');
        window.scrollTo({
            top: divPosition && divPosition.offsetTop,
            behavior: 'smooth',
        });
    };

    const toggleDrawer = (open) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setOpenDrawer(open);
    };

    return (
        <Grid item xs={12} md={5} className="product_info">
            <ShareModal
                press={(value) => {
                    handleShare(value);
                }}
                open={isShare}
            />

            <Grid container justify="space-between" alignItems="center" className="product_info_up">
                <label>{product && product.brand_name}</label>
                <div>
                    <IconButton onClick={handleOpen}>
                        <HiShare />
                    </IconButton>
                    <IconButton>
                        <AiOutlineHeart />
                    </IconButton>
                </div>
            </Grid>
            <Grid container className="product_info_details">
                <Grid item xs={12}>
                    <label className="product_info_details_title">{product && product.name}</label>
                </Grid>
                <Grid container justify="space-between" alignItems="center" className="price_and_stock">
                    <div>{getPrices()}</div>
                    <label className="stock">In stock</label>
                </Grid>
                <Hidden only={['xs', 'sm']}>
                    <Grid container className="size_section">
                        <SizeSection
                            product={product}
                            onClick={(key, data, value) => {
                                setSizeType(key);
                                setSize(value);
                                setSizeData(data);
                            }}
                        />
                    </Grid>
                </Hidden>
                <Hidden only={['xs', 'sm']}>
                    <Grid item xs={12} className="add_to_cart">
                        {!cartLoading && (
                            <Button className="btn" onClick={addToCart}>
                                Add To Cart
                            </Button>
                        )}
                        {cartLoading && (
                            <Grid container justify="center">
                                <CircularProgress />
                            </Grid>
                        )}
                    </Grid>
                </Hidden>
                <Grid container className="product_info_details_description">
                    <Hidden only={['md', 'lg', 'xl']}>
                        <Grid item xs={12}>
                            <div className="line" style={{ marginTop: 0 }} />
                        </Grid>
                    </Hidden>
                    <CollapsibleCnt cName="Description" product={product} keyName="description" open={true} />
                    <Grid item xs={12}>
                        <div className="line" />
                    </Grid>
                    <CollapsibleCnt cName="Shipping & Returns" product={product} keyName="Shipping" />
                    <Grid item xs={12}>
                        <div className="line" />
                    </Grid>
                    <CollapsibleCnt cName="Support" product={product} keyName="Support" />
                    <Grid item xs={12}>
                        <div className="line" />
                    </Grid>
                </Grid>
                <Grid container justify="center">
                    <Grid item xs={12} md={10}>
                        <UJAdvantage product={product} />
                    </Grid>
                </Grid>
            </Grid>

            <Hidden only={['md', 'lg', 'xl']}>
                <Grid container className="mobile_add_and_more" spacing={2}>
                    <Grid item xs={6} onClick={onClicklink}>
                        <Button className="more_btn">More Information</Button>
                    </Grid>
                    <Grid item xs={6}>
                        <Button className="buy_btn" onClick={toggleDrawer(true)}>
                            Buy
                        </Button>
                    </Grid>
                </Grid>
            </Hidden>
            <Drawer anchor={'bottom'} open={openDrawer} onClose={toggleDrawer(false)}>
                <Grid container justify="center" className="product_info_details" style={{ zIndex: 100000 }}>
                    <Grid container className="size_section" style={{ padding: `10px 1rem` }}>
                        <SizeSection
                            product={product}
                            onClick={(key, data, value) => {
                                setSizeType(key);
                                setSize(value);
                                setSizeData(data);
                            }}
                            toggleDrawer={toggleDrawer(false)}
                        />
                    </Grid>
                    <Grid item xs={11} className="add_to_cart" style={{ paddingBottom: 20 }}>
                        {!cartLoading && (
                            <Button
                                className="btn"
                                onClick={(e) => {
                                    addToCart();
                                    setOpenDrawer(false);
                                }}
                            >
                                Add To Cart
                            </Button>
                        )}
                        {cartLoading && (
                            <Grid container justify="center">
                                <CircularProgress />
                            </Grid>
                        )}
                    </Grid>
                </Grid>
            </Drawer>
        </Grid>
    );
};

export default ProdBasicInfo;

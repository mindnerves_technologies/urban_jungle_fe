import { Grid, Hidden } from '@material-ui/core';
import React from 'react';
import { SideBySideMagnifier } from 'react-image-magnifiers';
import Slider from 'react-slick';

const ProdMainImg = ({ product }) => {
    var settings = {
        rows: 1,
        dots: true,
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplaySpeed: 2000,
        initialSlide: 0,
        cssEase: 'linear',
    };
    const images = (product && product.imageUrl && product.imageUrl.thumbnail) || [];
    return (
        <Grid item xs={12} md={12}>
            <Hidden only={['sm', 'xs']}>
                <Grid container spacing={2} className="product_image_container">
                    {images &&
                        images.map((url, index) => (
                            <Grid item md={6} key={`product_url_${index}_${url}`}>
                                <div className="product_image_container_div">
                                    <SideBySideMagnifier
                                        className="input-position"
                                        style={{ order: false ? '1' : '0' }}
                                        imageSrc={url}
                                        largeImageSrc={url}
                                        alwaysInPlace={false}
                                        overlayOpacity={0.1}
                                        switchSides={false}
                                        zoomPosition="left"
                                        inPlaceMinBreakpoint={641}
                                        fillAvailableSpace={false}
                                        fillAlignTop={false}
                                        fillGapTop={0}
                                        fillGapRight={10}
                                        fillGapBottom={10}
                                        fillGapLeft={10}
                                        zoomContainerBorder="1px solid #ccc"
                                        zoomContainerBoxShadow="0 4px 5px rgba(0,0,0,.5)"
                                    />
                                </div>
                            </Grid>
                        ))}
                </Grid>
            </Hidden>
            <Hidden only={['md', 'xl', 'lg']}>
                <Slider {...settings}>
                    {images &&
                        images.map((url, index) => (
                            <div className="product_image_container_div" key={`product_url_${index}_${url}`}>
                                <img src={url} alt="productImage" />
                            </div>
                        ))}
                </Slider>
            </Hidden>
        </Grid>
    );
};

export default ProdMainImg;

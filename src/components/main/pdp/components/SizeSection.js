import { Button, ButtonGroup, Grid, Hidden, IconButton } from '@material-ui/core';
import React, { useMemo, useState } from 'react';
import { MdClose } from 'react-icons/md';
import SizeChartModel from '../modal/SizeChartModel';

const SizeSection = ({ product, onClick, toggleDrawer }) => {
    const [open, setOpen] = useState(false);
    const [activeTab, setActiveTab] = useState('');
    const [selectedSize, setSelectedSize] = useState('');

    const header = useMemo(() => {
        const size = (product && product.sizemaster && JSON.parse(product.sizemaster)) || [];
        let list = [];
        if (size && size.length > 0) {
            Object.keys(size[0]).forEach((key, index) => {
                if (index === 0) {
                    setActiveTab(key);
                }
                list.push(key);
            });
        }
        return list;
    }, [product]);

    const size = useMemo(() => {
        let list = [];
        const size = (product && product.sizemaster && JSON.parse(product.sizemaster)) || [];
        product &&
            product.simpleproducts &&
            product.simpleproducts.forEach((p) => {
                let type = '';
                if (p.size) {
                    type = p.size.size_type;
                    size &&
                        size.forEach((s) => {
                            if (s[type] === p.size.size_val) {
                                list.push({
                                    sizeData: p.size,
                                    size: s,
                                    qty: p.qty,
                                    stockstatus: p.stockstatus,
                                });
                            }
                        });
                }
            });

        return list;
    }, [product]);

    return (
        <Grid container justify="space-between" alignItems="center">
            {product.is_sizeoption === 'yes' && (
                <>
                    <Hidden only={['xs', 'sm']}>
                        <SizeChartModel product={product} open={open} setOpen={setOpen} />
                        <label className="size_section_title">Select Size</label>
                        <div>
                            <label
                                className="size_section_chart"
                                onClick={() => setOpen(true)}
                                style={{ cursor: 'pointer' }}
                            >
                                Size chart
                            </label>
                            {header &&
                                header.map((key, index) => (
                                    <button
                                        style={{ cursor: 'pointer' }}
                                        onClick={() => setActiveTab(key)}
                                        className={`size_button ${activeTab === key && 'active_size'}`}
                                        key={`${key}_${index}`}
                                    >
                                        {key}
                                    </button>
                                ))}
                        </div>
                        <Grid item xs={12}>
                            <div className="line" />
                        </Grid>
                        <Grid item xs={12}>
                            <Grid container>
                                {size &&
                                    size.map((s, i) => (
                                        <Grid
                                            item
                                            xs={3}
                                            style={{ cursor: 'pointer' }}
                                            className={`size_number`}
                                            key={i}
                                        >
                                            <Button
                                                onClick={() => {
                                                    setSelectedSize(s.size[activeTab]);
                                                    onClick(activeTab, s, s.size[activeTab]);
                                                }}
                                                disabled={s.qty <= 0 || !s.stockstatus}
                                                className={`${
                                                    selectedSize === s.size[activeTab] && 'size_number_active'
                                                }`}
                                            >
                                                <label style={{ cursor: 'pointer' }}>{s.size[activeTab]}</label>
                                            </Button>
                                        </Grid>
                                    ))}
                            </Grid>
                        </Grid>
                    </Hidden>
                    <Hidden only={['md', 'lg', 'xl']}>
                        <SizeChartModel product={product} open={open} setOpen={setOpen} />
                        <Grid container justify="space-between" alignItems="center" style={{ paddingBottom: 20 }}>
                            <label className="size_section_title">Select Size</label>
                            <IconButton onClick={toggleDrawer}>
                                <MdClose />
                            </IconButton>
                        </Grid>
                        <Grid container justify="space-between" alignItems="center">
                            <div>
                                <label
                                    className="size_section_chart"
                                    onClick={() => setOpen(true)}
                                    style={{ cursor: 'pointer' }}
                                >
                                    Size chart
                                </label>
                            </div>
                            <div>
                                {header &&
                                    header.map((key, index) => (
                                        <button
                                            style={{ cursor: 'pointer' }}
                                            onClick={() => setActiveTab(key)}
                                            className={`size_button ${activeTab === key && 'active_size'}`}
                                            key={`${key}_${index}`}
                                        >
                                            {key}
                                        </button>
                                    ))}
                            </div>
                        </Grid>
                        <Grid item xs={12}>
                            <div className="line" />
                        </Grid>
                        <Grid item xs={12}>
                            <Grid container>
                                {size &&
                                    size.map((s, i) => (
                                        <Grid
                                            item
                                            xs={3}
                                            style={{ cursor: 'pointer' }}
                                            className={`size_number`}
                                            key={i}
                                        >
                                            <Button
                                                onClick={() => {
                                                    setSelectedSize(s.size[activeTab]);
                                                    onClick(activeTab, s, s.size[activeTab]);
                                                }}
                                                disabled={s.qty <= 0 || !s.stockstatus}
                                                className={`${
                                                    selectedSize === s.size[activeTab] && 'size_number_active'
                                                }`}
                                            >
                                                <label style={{ cursor: 'pointer' }}>{s.size[activeTab]}</label>
                                            </Button>
                                        </Grid>
                                    ))}
                            </Grid>
                        </Grid>
                    </Hidden>
                </>
            )}
            {product.is_sizeoption !== 'yes' && (
                <>
                    <Hidden only={['xs', 'sm']}>
                        <Grid item xs={12}>
                            <label className="size_section_title">Select Size</label>
                        </Grid>
                        <ButtonGroup
                            size="large"
                            color="primary"
                            aria-label="outlined primary button group"
                            style={{ marginTop: 10, marginBottom: '1rem' }}
                        >
                            {product &&
                                product.simpleproducts &&
                                product.simpleproducts.map((p, index) => (
                                    <Button
                                        key={`normal_size_${index}`}
                                        onClick={() => {
                                            setSelectedSize(p.size && p.size.originalName);
                                            onClick(
                                                p.size && p.size.size_type,
                                                { ...p, sizeData: p.size },
                                                p.size && p.size.size_val,
                                            );
                                        }}
                                        disabled={!p.stockstatus}
                                        className={`${
                                            selectedSize === (p.size && p.size.originalName) && 'size_number_active'
                                        }`}
                                    >
                                        {p.size && p.size.originalName}
                                    </Button>
                                ))}
                        </ButtonGroup>
                    </Hidden>
                    <Hidden only={['md', 'lg', 'xl']}>
                        <Grid container justify="space-between" alignItems="center" style={{ paddingBottom: 20 }}>
                            <label className="size_section_title">Select Size</label>
                            <IconButton onClick={toggleDrawer}>
                                <MdClose />
                            </IconButton>
                        </Grid>
                        <ButtonGroup
                            size="large"
                            color="primary"
                            aria-label="outlined primary button group"
                            style={{ marginTop: 10, marginBottom: '1rem' }}
                        >
                            {product &&
                                product.simpleproducts &&
                                product.simpleproducts.map((p, index) => (
                                    <Button
                                        key={`normal_size_${index}`}
                                        onClick={() => {
                                            setSelectedSize(p.size && p.size.originalName);
                                            onClick(
                                                p.size && p.size.size_type,
                                                { ...p, sizeData: p.size },
                                                p.size && p.size.size_val,
                                            );
                                        }}
                                        disabled={!p.stockstatus}
                                        className={`${
                                            selectedSize === (p.size && p.size.originalName) && 'size_number_active'
                                        }`}
                                    >
                                        {p.size && p.size.originalName}
                                    </Button>
                                ))}
                        </ButtonGroup>
                    </Hidden>
                </>
            )}
        </Grid>
    );
};

export default SizeSection;

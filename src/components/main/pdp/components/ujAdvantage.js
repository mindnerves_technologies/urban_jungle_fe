import { Grid, Hidden } from '@material-ui/core';
import React from 'react';
import ReactHtmlParser from 'react-html-parser';

const UJAdvantage = ({ product }) => {
    return (
        <Grid container className="uj_advantage" spacing={2} justify="space-between">
            <Grid item xs={12} className="uj_advantage_up">
                <label className="uj_advantage_up_title">The Urban Jungle Advantage</label>
            </Grid>
            <Grid item xs={5} className="uj_advantage_image_and_text">
                <div>
                    <img
                        src="https://ujdev.urbanjunglestore.com/pub/media/banners/default/Icon_Advantage_2_Safe.jpg"
                        alt="advantag"
                    />
                    <p>Authorized Retailer Of Premium Brands</p>
                </div>
            </Grid>
            <div className="vertical_line"></div>
            <Grid item xs={5} className="uj_advantage_image_and_text">
                <div>
                    <img
                        src="https://ujdev.urbanjunglestore.com/pub/media/banners/default/Icon_Advantage_2_Safe.jpg"
                        alt="advantag"
                    />
                    <p>Safe And Secure Checkout</p>
                </div>
            </Grid>
            <Grid item xs={5} className="uj_advantage_image_and_text">
                <div>
                    <img
                        src="https://ujdev.urbanjunglestore.com/pub/media/banners/default/Icon_Advantage_3_Club.jpg"
                        alt="advantag"
                    />
                    <p>Explore Privilege Club!</p>
                    <span className="line" />
                </div>
            </Grid>
            <div className="vertical_line"></div>
            <Grid item xs={5} className="uj_advantage_image_and_text">
                <div>
                    <img
                        src="https://ujdev.urbanjunglestore.com/pub/media/banners/default/Icon_Advantage_4_Shipping.jpg"
                        alt="advantag"
                    />
                    <p>Free Standard Shipping*</p>
                </div>
            </Grid>
            <Grid item xs={5} className="uj_advantage_image_and_text">
                <div>
                    <img
                        src="https://ujdev.urbanjunglestore.com/pub/media/banners/default/Icon_Advantage_5_Return.jpg"
                        alt="advantag"
                    />
                    <p>Easy And Hassle Free Return</p>
                </div>
            </Grid>
            <div className="vertical_line"></div>
            <Grid item xs={5} className="uj_advantage_image_and_text">
                <div>
                    <img
                        src="https://ujdev.urbanjunglestore.com/pub/media/banners/default/Icon_Advantage_6_Chat.jpg"
                        alt="advantag"
                    />
                    <p>Live Chat</p>
                </div>
            </Grid>
            <Hidden only={['xs', 'sm']}>
                <Grid container justify="center" style={{ paddingTop: '1.5rem' }}>
                    <div className="free_text">
                        <label>* Free Shipping</label>
                        {product && product.free_shipping && ReactHtmlParser(product.free_shipping)}
                    </div>
                </Grid>
            </Hidden>
            <Hidden only={['md', 'lg', 'xl']}>
                <Grid container justify="center" style={{ paddingTop: '1.5rem' }}>
                    <Grid item xs={12} md={6} className="free_text">
                        <label>* Free Shipping</label>
                    </Grid>
                </Grid>
                <Grid container justify="center" className="free_text" spacing={2}>
                    {product && product.free_shipping && ReactHtmlParser(product.free_shipping)}
                </Grid>
            </Hidden>
        </Grid>
    );
};

export default UJAdvantage;

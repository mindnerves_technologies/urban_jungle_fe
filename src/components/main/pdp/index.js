import React, { useLayoutEffect } from 'react';
import ProdBasicInfo from './components/ProdBasicInfo';
import ProdMainImg from './components/ProdMainImg';
import { Grid } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { actions as PDPActions } from './redux/actions';
import Loader from '../../common/Loader';
import { FormattedMessage } from 'react-intl';
import SimpleBreadcrumbs from '../../common/Breadcrumbs';
import { MagnifierContainer } from 'react-image-magnifiers';

const PDP = ({ url_key }) => {
    const dispatch = useDispatch();

    const product = useSelector((state) => state && state.PDP && state.PDP.product);
    const loading = useSelector((state) => state && state.PDP && state.PDP.loading);
    const store_id = useSelector((state) => state && state.storeInfo && state.storeInfo.store_id);
    const store_code = useSelector((state) => state && state.storeInfo && state.storeInfo.store_code);

    useLayoutEffect(() => {
        dispatch(
            PDPActions.getPDPDataReq({
                store_id,
                url_key: url_key,
            }),
        );
    }, [dispatch, store_id, url_key]);

    let breadCrumbs = [
        {
            name: <FormattedMessage id="Common.Home.Text" defaultMessage="Home" />,
            url: `/${store_code}/`,
        },
    ];
    if (product && product.breadcrumb) {
        breadCrumbs.push({
            name: product.breadcrumb.name || 'no Name',
            url: `/${store_code}/${product.breadcrumb.url_key}`,
        });
        breadCrumbs.push({
            name: product.name,
        });
    }

    return (
        <div className="main-container">
            {!loading && product && (
                <>
                    <SimpleBreadcrumbs breadCrumbs={breadCrumbs} />

                    <Grid container spacing={4}>
                        <Grid item xs={12} md={7}>
                            <MagnifierContainer>
                                <ProdMainImg product={product} />
                            </MagnifierContainer>
                        </Grid>
                        <ProdBasicInfo product={product} />
                    </Grid>

                    <div className="pdp-you-might-also-like">
                        {/* <Grid item md={12}>
                            <h4>You might also like</h4>
                            <MightLike productData={data} />
                        </Grid> */}
                    </div>
                    <div className="pdp-you-might-also-like">
                        {/* <Grid item md={12}>
                            <h4>Customer also bought</h4>
                            <MightLike productData={data} />
                        </Grid> */}
                    </div>
                </>
            )}
            {loading && <Loader />}
        </div>
    );
};

export default PDP;

import React from 'react';
import { Dialog, Grid, IconButton } from '@material-ui/core';
import { IoCloseCircle } from 'react-icons/io5';
import Slide from '@material-ui/core/Slide';
import facebookIcon from '../../../../assets/pdp/facebook.png';
import instagramIcon from '../../../../assets/pdp/instagram.png';
import twitterIcon from '../../../../assets/pdp/twitter.png';
import whatsappIcon from '../../../../assets/pdp/whatsapp.png';
import copyIcon from '../../../../assets/pdp/copy.png';
import { FacebookShareButton, WhatsappShareButton, TwitterShareButton } from 'react-share';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { toast } from 'react-toastify';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const ShareModal = (props) => {
    const { open } = props;

    const shareUrl = window.location.href;

    const handleClickOpen = () => {
        props.press(true);
    };

    const handleClose = () => {
        props.press(false);
    };

    return (
        <Dialog
            open={open}
            TransitionComponent={Transition}
            keepMounted
            onClose={handleClickOpen}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
            className="size_chart_pop auto-width"
        >
            <Grid container>
                <Grid item xs={12}>
                    <Grid container justify="space-between" alignItems="center">
                        <label className="title"> Share</label>
                        <IconButton onClick={() => handleClose()} size="small">
                            <IoCloseCircle />
                        </IconButton>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Grid container justify="space-around">
                        <div className="social_image">
                            <FacebookShareButton url={shareUrl} style={{ cursor: 'pointer' }}>
                                <div>
                                    <img src={facebookIcon} alt="facebook" />
                                </div>
                                <label>Facebook</label>
                            </FacebookShareButton>
                        </div>
                        <div className="social_image">
                            <TwitterShareButton url={shareUrl} style={{ cursor: 'pointer' }}>
                                <div>
                                    <img src={twitterIcon} alt="facebook" />
                                </div>
                                <label>Twitter</label>
                            </TwitterShareButton>
                        </div>
                        <div className="social_image">
                            <div>
                                <img src={instagramIcon} alt="facebook" />
                            </div>
                            <label>Instagram</label>
                        </div>
                        <div className="social_image">
                            <WhatsappShareButton title={'WhatsApp'} url={shareUrl} style={{ cursor: 'pointer' }}>
                                <div>
                                    <img src={whatsappIcon} alt="facebook" />
                                </div>
                                <label>Whatsapp</label>
                            </WhatsappShareButton>
                        </div>
                        <div
                            className="social_image"
                            onClick={() => document.getElementById('copy_text_value').click()}
                        >
                            <div>
                                <img src={copyIcon} alt="facebook" />
                            </div>
                            <label>Copy link</label>
                        </div>
                        <CopyToClipboard
                            id="copy_text_value"
                            text={shareUrl}
                            onCopy={() => toast.success('URL copy successfully!')}
                            style={{ display: 'none' }}
                        >
                            <span>copy</span>
                        </CopyToClipboard>
                    </Grid>
                </Grid>
            </Grid>
        </Dialog>
    );
};

export default ShareModal;

import { Dialog, Grid, IconButton } from '@material-ui/core';
import React, { useMemo } from 'react';
import Slide from '@material-ui/core/Slide';
import { IoCloseCircle } from 'react-icons/io5';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const SizeChartModel = ({ product, open, setOpen }) => {
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const header = useMemo(() => {
        const size = (product && product.sizemaster && JSON.parse(product.sizemaster)) || [];
        let list = [];
        if (size && size.length > 0) {
            Object.keys(size[0]).forEach((key) => {
                list.push(key);
            });
        }
        return list;
    }, [product]);

    const size = useMemo(() => {
        const size = (product && product.sizemaster && JSON.parse(product.sizemaster)) || [];
        return size;
    }, [product]);

    return (
        <Dialog
            open={open}
            TransitionComponent={Transition}
            keepMounted
            onClose={handleClickOpen}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
            className="size_chart_pop"
        >
            <Grid container>
                <Grid item xs={12}>
                    <Grid container justify="space-between" alignItems="center">
                        <label className="title"> Size Chart</label>
                        <IconButton onClick={() => handleClose()} size="small">
                            <IoCloseCircle />
                        </IconButton>
                    </Grid>
                </Grid>
                <Grid item xs={12} className="container">
                    <div className="material_table">
                        <table>
                            <thead>
                                <tr>
                                    {header &&
                                        header.map((key) => (
                                            <th key={`header_${key}`} align="center">
                                                {key}
                                            </th>
                                        ))}
                                </tr>
                            </thead>
                            <tbody>
                                {size &&
                                    size.map((s, i) => (
                                        <tr key={i} style={{ animationDelay: `${Math.random() / 2}s` }}>
                                            {header &&
                                                header.map((key, index) => (
                                                    <td align="center" key={`row_${index}_${key}_${i}`}>
                                                        {s[key]}
                                                    </td>
                                                ))}
                                        </tr>
                                    ))}
                            </tbody>
                        </table>
                    </div>
                </Grid>
            </Grid>
        </Dialog>
    );
};

export default SizeChartModel;

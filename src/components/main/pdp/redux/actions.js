import { createAction } from 'redux-actions';

const GET_PDP_DATA_REQ = 'UJ/GET_PDP_DATA_REQ';
const GET_PDP_DATA_REQ_SUCCESS = 'UJ/GET_PDP_DATA_REQ_SUCCESS';
const GET_PDP_DATA_REQ_FAILED = 'UJ/GET_PDP_DATA_REQ_FAILED';

const ADD_TO_CART_REQ = 'UJ/ADD_TO_CART_REQ';
const ADD_TO_CART_REQ_SUCCESS = 'UJ/ADD_TO_CART_REQ_SUCCESS';
const ADD_TO_CART_REQ_FAILED = 'UJ/ADD_TO_CART_REQ_FAILED';

const getPDPDataReq = createAction(GET_PDP_DATA_REQ);
const getPDPDataReqSuccess = createAction(GET_PDP_DATA_REQ_SUCCESS);
const getPDPDataReqFailed = createAction(GET_PDP_DATA_REQ_FAILED);

const addToCartReq = createAction(ADD_TO_CART_REQ);
const addToCartReqSuccess = createAction(ADD_TO_CART_REQ_SUCCESS);
const addToCartReqFailed = createAction(ADD_TO_CART_REQ_FAILED);

export const actions = {
    getPDPDataReq,
    getPDPDataReqSuccess,
    getPDPDataReqFailed,

    addToCartReq,
    addToCartReqSuccess,
    addToCartReqFailed,
};

export const types = {
    GET_PDP_DATA_REQ,
    GET_PDP_DATA_REQ_SUCCESS,
    GET_PDP_DATA_REQ_FAILED,

    ADD_TO_CART_REQ,
    ADD_TO_CART_REQ_SUCCESS,
    ADD_TO_CART_REQ_FAILED,
};

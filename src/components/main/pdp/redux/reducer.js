import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandlers = {
    [types.GET_PDP_DATA_REQ]: (state) => ({
        ...state,
        loading: true,
    }),
    [types.GET_PDP_DATA_REQ_FAILED]: (state) => ({
        ...state,
        loading: false,
        product: null,
    }),
    [types.GET_PDP_DATA_REQ_SUCCESS]: (state, { payload }) => ({
        ...state,
        loading: false,
        product: payload || {},
    }),
    [types.ADD_TO_CART_REQ]: (state) => ({
        ...state,
        cartLoading: true,
    }),
    [types.ADD_TO_CART_REQ_SUCCESS]: (state) => ({
        ...state,
        cartLoading: false,
    }),
    [types.ADD_TO_CART_REQ_FAILED]: (state) => ({
        ...state,
        cartLoading: false,
    }),
};

export default handleActions(actionHandlers, {
    loading: false,
    product: {},
    cartLoading: false,
});

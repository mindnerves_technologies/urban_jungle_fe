import { call, put, takeLatest } from 'redux-saga/effects';
import { actions, types } from './actions';
import { toast } from 'react-toastify';
import APIList from '../../../../api';

const getPDPDataReq = function* getPDPDataReq({ payload }) {
    try {
        const { data } = yield call(APIList.getPDPData, payload);
        if (data.status) {
            yield put(actions.getPDPDataReqSuccess(data.product && data.product[0]));
        } else {
            yield put(actions.getPDPDataReqFailed());
            toast.error(data.message || 'Something wrong, Please try after some time');
        }
    } catch (err) {
        yield put(actions.getPDPDataReqFailed());
        toast.error(
            (err.response && err.response.data && err.response.data.error) ||
                'Something wrong, Please try after some time',
        );
    }
};

const addToCartReq = function* addToCartReq({ payload }) {
    try {
        const { payloadData, guestId, loginUser } = payload;
        if (!loginUser) {
            const { data } = yield call(APIList.addToCartGuestReq, payloadData, guestId);
            if (data) {
                toast.success(`Add To cart Successfully!`);
            }
            yield put(actions.addToCartReqSuccess());
        } else {
            const { data } = yield call(APIList.addToCartLoginReq, payloadData);
            if (data) {
                toast.success(`Add To cart Successfully!`);
            }
            yield put(actions.addToCartReqSuccess());
        }
    } catch (err) {
        yield put(actions.addToCartReqFailed());
        toast.error(
            (err.response && err.response.data && err.response.data.error) ||
                (err.response && err.response.data && err.response.data.error && err.response.data.error.message) ||
                'Something wrong, Please try after some time',
        );
    }
};

export default function* sagas() {
    yield takeLatest(types.GET_PDP_DATA_REQ, getPDPDataReq);
    yield takeLatest(types.ADD_TO_CART_REQ, addToCartReq);
}

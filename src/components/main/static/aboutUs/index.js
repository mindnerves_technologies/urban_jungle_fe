import React, { useLayoutEffect } from 'react';
import ReactHtmlParser from 'react-html-parser';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import SimpleBreadcrumbs from '../../../common/Breadcrumbs';
import Loader from '../../../common/Loader';
import { actions as staticActions } from '../redux/actions';

const AboutUs = () => {
    const dispatch = useDispatch();

    const store_id = useSelector((state) => state && state.storeInfo && state.storeInfo.store_id);
    const store_code = useSelector((state) => state && state.storeInfo && state.storeInfo.store_code);
    const aboutUs = useSelector((state) => state && state.staticData && state.staticData.aboutUs);
    const aboutUsLoading = useSelector((state) => state && state.staticData && state.staticData.aboutUsLoading);

    useLayoutEffect(() => {
        dispatch(
            staticActions.getAboutUsData({
                store_id: store_id,
            }),
        );
    }, [dispatch, store_id]);

    const styleAboutUs = {
        margin: '5px 1.5em 1.5em',
    };

    const breadCrumbs = [
        {
            name: <FormattedMessage id="Common.Home.Text" defaultMessage="Home" />,
            url: `/${store_code}/`,
        },

        {
            name: 'About Us',
        },
    ];

    return (
        <>
            <div style={{ marginLeft: '1.5em' }}>
                <SimpleBreadcrumbs breadCrumbs={breadCrumbs} />
            </div>
            {!aboutUsLoading && <div style={styleAboutUs}>{ReactHtmlParser(aboutUs.content)}</div>}
            {aboutUsLoading && <Loader />}
        </>
    );
};

export default AboutUs;

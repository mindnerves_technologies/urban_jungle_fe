import React from 'react';
import { Typography } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Shipping from '../../../../assets/shipping.png';
import cartIcon from '../../../../assets/cartIcon.png';
import fileIcon from '../../../../assets/fileIcon.png';
import facebook from '../../../../assets/facebook.png';
import instagram from '../../../../assets/instagram.png';
import { MdKeyboardArrowRight, MdEmail, MdCall } from 'react-icons/md';
import { IoChatboxEllipsesSharp } from 'react-icons/io5';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';

const ContactUs = () => {
    const history = useHistory();

    const store_code = useSelector((state) => state && state.storeInfo && state.storeInfo.store_code);
    const contactus = useSelector((state) => state && state.staticData && state.staticData.contactus);

    const redirectToTermsAndCondition = () => {
        return history.push(`/${store_code}/terms-and-conditions`);
    };

    const redirectToShippingAndReturn = () => {
        return history.push(`/${store_code}/shipping-and-return`);
    };

    const onClickSocialLogin = (url) => {
        window.open(url, '_blank');
    };

    return (
        <div className="contact-us">
            <Typography className="heading-title">Contact us</Typography>
            <div className="first-section">
                <Grid container spacing={3}>
                    <Grid item xs={12} sm={12} md={4}>
                        <div className="message">
                            <ul>
                                <li>
                                    <MdEmail />
                                </li>
                                <li>
                                    {' '}
                                    <Typography className="lebel-text">Have questions & suggestions?</Typography>
                                    <Typography className="bold-text">
                                        <a href={`mailto:${contactus.contact_email}`} className="bold-text">
                                            {contactus.contact_email}
                                        </a>
                                    </Typography>
                                </li>
                            </ul>
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={12} md={4}>
                        <div className="message">
                            <ul>
                                <li>
                                    <MdCall />
                                </li>
                                <li>
                                    {' '}
                                    <Typography className="lebel-text">Need Assistance? Give us a call</Typography>
                                    <Typography className="bold-text">{contactus.contactnumber_it}</Typography>
                                </li>
                            </ul>
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={12} md={4}>
                        <div className="message">
                            <ul>
                                <li>
                                    <IoChatboxEllipsesSharp />
                                </li>
                                <li>
                                    <Typography className="bold-text">Live Chat</Typography>
                                </li>
                            </ul>
                        </div>
                    </Grid>
                </Grid>
            </div>
            <div className="second-section">
                <Typography className="Second-heading">What do you wish to contact us about?</Typography>
                <Grid container spacing={3}>
                    <Grid item xs={12} sm={12} md={4}>
                        <div>
                            <div className="box">
                                <ul onClick={() => redirectToShippingAndReturn()}>
                                    <li>
                                        <img src={Shipping} alt="shipping" />
                                    </li>
                                    <li>
                                        <Typography className="bold-text">Shipping & Returns</Typography>
                                    </li>
                                    <li className="rightIcon">
                                        <MdKeyboardArrowRight />
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={12} md={4}>
                        <div>
                            <div className="box">
                                <ul>
                                    <li>
                                        <img src={cartIcon} alt="cart" />
                                    </li>
                                    <li>
                                        <Typography className="bold-text">My Orders</Typography>
                                    </li>
                                    <li className="rightIcon">
                                        <MdKeyboardArrowRight />
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={12} md={4}>
                        <div>
                            <div className="box">
                                <ul onClick={redirectToTermsAndCondition}>
                                    <li>
                                        <img src={fileIcon} alt="file" />
                                    </li>
                                    <li>
                                        <Typography className="bold-text">Terms & Conditions</Typography>
                                    </li>
                                    <li className="rightIcon">
                                        <MdKeyboardArrowRight />
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </Grid>
                </Grid>
            </div>
            <div className="social-section">
                <Typography className="heading">Follow us on social media</Typography>
                <div className="">
                    <ul>
                        <li>
                            <div onClick={() => onClickSocialLogin(contactus.facebook)}>
                                <img src={facebook} alt="facebook" style={{ cursor: 'pointer' }} />
                            </div>
                        </li>
                        <li>
                            <div onClick={() => onClickSocialLogin(contactus.instagram)}>
                                <img src={instagram} alt="instagram" style={{ cursor: 'pointer' }} />
                            </div>
                        </li>
                    </ul>
                </div>
                <div className="reach-us">
                    <Typography className="heading">Reach us</Typography>
                    <Typography className="address-text"> {contactus.contact_address}</Typography>
                </div>
            </div>
        </div>
    );
};
export default ContactUs;

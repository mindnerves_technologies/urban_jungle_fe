import React, { useLayoutEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';
import Collapsible from 'react-collapsible';
import Plus from '../../../../assets/plus.png';
import Minus from '../../../../assets/minus.png';
import { useDispatch, useSelector } from 'react-redux';
import Loader from '../../../common/Loader';
import { actions as staticActions } from '../redux/actions';
import { FormattedMessage } from 'react-intl';
import SimpleBreadcrumbs from '../../../common/Breadcrumbs';

const FAQ = () => {
    const dispatch = useDispatch();

    const loading = useSelector((state) => state && state.staticData.faqLoading);
    const faqData = useSelector((state) => state && state.staticData.faqData);
    const store_id = useSelector((state) => state && state.storeInfo && state.storeInfo.store_id);
    const store_code = useSelector((state) => state && state.storeInfo && state.storeInfo.store_code);

    useLayoutEffect(() => {
        dispatch(
            staticActions.getFaqDataReq({
                store_id: store_id,
            }),
        );
    }, [dispatch, store_id]);

    const closeOther = (value) => {
        let collapses = document.getElementsByClassName('open');
        for (let i = 0; i < collapses.length; i++) {
            collapses[i].click();
        }
    };

    const breadCrumbs = [
        {
            name: <FormattedMessage id="Common.Home.Text" defaultMessage="Home" />,
            url: `/${store_code}/`,
        },

        {
            name: 'FAQ',
        },
    ];

    return (
        <div className="faq-section">
            <SimpleBreadcrumbs breadCrumbs={breadCrumbs} />
            {!loading && (
                <>
                    <Grid item xs={12}>
                        <Typography className="maintitle">FAQ's</Typography>
                    </Grid>
                    {faqData &&
                        faqData.map((faq, index) => (
                            <Grid container key={`faq_${faq.category}_${faq.id}_${index}`}>
                                <Grid item xs={12}>
                                    <Typography className="title">{faq.category}</Typography>
                                </Grid>
                                <Grid item xs={12} className="CollapsibleOuter">
                                    <Collapsible
                                        trigger={
                                            <div onClick={() => closeOther(0)} className="Collapsible_text_container">
                                                <div className="Collapsible_text footerHeading">{faq.que}</div>
                                                <div className="Collapsible_arrow_container">
                                                    <img className="Icon" src={Plus} alt="" />
                                                </div>
                                            </div>
                                        }
                                        triggerWhenOpen={
                                            <div className="Collapsible_text_container open">
                                                <div className="Collapsible_text footerHeading">{faq.que}</div>
                                                <div className="Collapsible_arrow_container">
                                                    <img src={Minus} alt="" className="Icon reverse" />
                                                </div>
                                            </div>
                                        }
                                    >
                                        <div className="answer-text">{faq.ans}</div>
                                    </Collapsible>
                                </Grid>
                            </Grid>
                        ))}
                </>
            )}
            {loading && <Loader />}
        </div>
    );
};

export default FAQ;

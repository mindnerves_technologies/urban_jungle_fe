import React, { useLayoutEffect } from 'react';
import ReactHtmlParser from 'react-html-parser';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import SimpleBreadcrumbs from '../../../common/Breadcrumbs';
import Loader from '../../../common/Loader';
import { actions as staticActions } from '../redux/actions';

const Privacy = () => {
    const dispatch = useDispatch();

    const store_id = useSelector((state) => state && state.storeInfo && state.storeInfo.store_id);
    const store_code = useSelector((state) => state && state.storeInfo && state.storeInfo.store_code);
    const privacy = useSelector((state) => state && state.staticData && state.staticData.privacy);
    const privacyLoading = useSelector((state) => state && state.staticData && state.staticData.privacyLoading);

    useLayoutEffect(() => {
        dispatch(
            staticActions.getPrivacyData({
                store_id: store_id,
            }),
        );
    }, [dispatch, store_id]);

    const stylePrivacy = {
        margin: '5px 1.5em 1.5em',
    };

    const breadCrumbs = [
        {
            name: <FormattedMessage id="Common.Home.Text" defaultMessage="Home" />,
            url: `/${store_code}/`,
        },

        {
            name: 'Privacy policy',
        },
    ];

    return (
        <>
            <div style={{ marginLeft: '1.5em' }}>
                <SimpleBreadcrumbs breadCrumbs={breadCrumbs} />
            </div>
            {!privacyLoading && <div style={stylePrivacy}>{ReactHtmlParser(privacy.content)}</div>}
            {privacyLoading && <Loader />}
        </>
    );
};

export default Privacy;

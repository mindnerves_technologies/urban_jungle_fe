import React, { useLayoutEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ReactHtmlParser from 'react-html-parser';
import Loader from '../../../common/Loader';
import { actions as staticActions } from '../redux/actions';
import { FormattedMessage } from 'react-intl';
import SimpleBreadcrumbs from '../../../common/Breadcrumbs';
import Collapsible from 'react-collapsible';
import { CgMathMinus, CgMathPlus } from 'react-icons/cg';

const ShippingAndReturn = () => {
    const dispatch = useDispatch();

    const [activeTab, setActiveTab] = useState('shippingAndDelivery');

    //const contactus = useSelector((state) => state && state.staticData && state.staticData.contactus);
    const loading = useSelector((state) => state && state.staticData.cmsPageLoading);
    const language = useSelector((state) => state && state.storeInfo && state.storeInfo.language);
    const store_id = useSelector((state) => state && state.storeInfo && state.storeInfo.store_id);
    const store_code = useSelector((state) => state && state.storeInfo && state.storeInfo.store_code);
    const cmsData = useSelector((state) => state && state.staticData && state.staticData.cmsData);

    useLayoutEffect(() => {
        dispatch(
            staticActions.getCmsPageDataReq({
                page: 'shipping_return',
                lan: language,
                store_id: store_id,
            }),
        );
    }, [dispatch, language, store_id]);

    useLayoutEffect(() => {
        if (cmsData && cmsData.left_section && cmsData.left_section[1]) {
            setActiveTab(cmsData.left_section[1].title);
        }
    }, [cmsData]);

    const onClicklink = (name) => {
        const divPosition = document.getElementById(name);
        setActiveTab(name);
        window.scrollTo({
            top: divPosition && divPosition.offsetTop,
            behavior: 'smooth',
        });
    };

    const breadCrumbs = [
        {
            name: <FormattedMessage id="Common.Home.Text" defaultMessage="Home" />,
            url: `/${store_code}/`,
        },

        {
            name: 'Shipping and Return',
        },
    ];

    return (
        <div className="main-container">
            <SimpleBreadcrumbs breadCrumbs={breadCrumbs} />
            {!loading && (
                <div className="s-n-r">
                    {cmsData && cmsData.top_section && ReactHtmlParser(cmsData.top_section)}
                    <div className="snr-main">
                        <div className="snr-tab-container">
                            <div className="snr-tab">
                                <ul>
                                    {cmsData &&
                                        cmsData.left_section &&
                                        Object.keys(cmsData.left_section).map((key, index) => (
                                            <li
                                                key={`${cmsData.left_section[key].title}_${index}`}
                                                onClick={() => onClicklink(cmsData.left_section[key].title)}
                                                className={`${
                                                    activeTab === cmsData.left_section[key].title && 'active_tab'
                                                }`}
                                            >
                                                {cmsData.left_section[key].title}
                                            </li>
                                        ))}
                                </ul>
                            </div>
                        </div>
                        <div className="snr-data">
                            {cmsData &&
                                cmsData.left_section &&
                                Object.keys(cmsData.left_section).map((key, index) => (
                                    <div
                                        className="snd"
                                        id={cmsData.left_section[key].title}
                                        key={`containt_${cmsData.left_section[key].title}_${index}`}
                                    >
                                        <h2>{cmsData.left_section[key].title}</h2>
                                        {cmsData.left_section[key].content &&
                                            ReactHtmlParser(cmsData.left_section[key].content)}
                                        <hr className="line" />
                                    </div>
                                ))}
                        </div>

                        <div className="snr-data1">
                            {cmsData &&
                                cmsData.left_section &&
                                Object.keys(cmsData.left_section).map((key, index) => {
                                    return (
                                        <div
                                            className="tnc-collapsible"
                                            index={`${index}_mobile_${cmsData.left_section[key].title}`}
                                        >
                                            <Collapsible
                                                trigger={
                                                    <div className="tnc-col-flex">
                                                        <div className="collapse-heading">
                                                            {cmsData.left_section[key].title}
                                                        </div>
                                                        <div className="collapse-heading">
                                                            <CgMathPlus />
                                                        </div>
                                                    </div>
                                                }
                                                triggerWhenOpen={
                                                    <div className="tnc-col-flex">
                                                        <div className="collapse-heading">
                                                            {cmsData.left_section[key].title}
                                                        </div>
                                                        <div className="collapse-heading">
                                                            <CgMathMinus />
                                                        </div>
                                                    </div>
                                                }
                                            >
                                                <div className="collapse-data">
                                                    {cmsData.left_section[key].content &&
                                                        ReactHtmlParser(cmsData.left_section[key].content)}
                                                </div>
                                            </Collapsible>
                                        </div>
                                    );
                                })}
                        </div>
                    </div>
                </div>
            )}
            {loading && <Loader />}
        </div>
    );
};

export default ShippingAndReturn;

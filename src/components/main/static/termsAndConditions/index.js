import React, { useLayoutEffect, useState } from 'react';
import Collapsible from 'react-collapsible';
import { CgMathMinus, CgMathPlus } from 'react-icons/cg';
import { useDispatch, useSelector } from 'react-redux';
import Loader from '../../../common/Loader';
import { actions as staticActions } from '../redux/actions';
import ReactHtmlParser from 'react-html-parser';
import { FormattedMessage } from 'react-intl';
import SimpleBreadcrumbs from '../../../common/Breadcrumbs';

const TermsAndConditions = () => {
    const dispatch = useDispatch();

    const loading = useSelector((state) => state && state.staticData.cmsPageLoading);
    const language = useSelector((state) => state && state.storeInfo && state.storeInfo.language);
    const store_id = useSelector((state) => state && state.storeInfo && state.storeInfo.store_id);
    const store_code = useSelector((state) => state && state.storeInfo && state.storeInfo.store_code);
    const cmsData = useSelector((state) => state && state.staticData && state.staticData.cmsData);

    useLayoutEffect(() => {
        dispatch(
            staticActions.getCmsPageDataReq({
                page: 'terms_condition',
                lan: language,
                store_id: store_id,
            }),
        );
    }, [dispatch, language, store_id]);

    useLayoutEffect(() => {
        if (cmsData && cmsData.left_section && cmsData.left_section[1]) {
            setActiveTab(cmsData.left_section[1].title);
        }
    }, [cmsData]);

    const [activeTab, setActiveTab] = useState('Scope');

    const handleScroll = (name) => {
        const divPosition = document.getElementById(name);
        setActiveTab(name);
        window.scrollTo({
            top: divPosition && divPosition.offsetTop,
            behavior: 'smooth',
        });
    };

    const breadCrumbs = [
        {
            name: <FormattedMessage id="Common.Home.Text" defaultMessage="Home" />,
            url: `/${store_code}/`,
        },

        {
            name: 'Terms and Conditions',
        },
    ];
    return (
        <div className="main-container">
            <SimpleBreadcrumbs breadCrumbs={breadCrumbs} />
            {!loading && (
                <div className="t-n-c">
                    {cmsData && cmsData.top_section && ReactHtmlParser(cmsData.top_section)}

                    <hr className="line" />

                    <div className="tnc-main">
                        <div className="tnc-tabs">
                            <div className="sticky">
                                {cmsData &&
                                    cmsData.left_section &&
                                    Object.keys(cmsData.left_section).map((key, index) => {
                                        return (
                                            <small
                                                className={`${
                                                    activeTab === cmsData.left_section[key].title && 'activeTab'
                                                }`}
                                                onClick={() => handleScroll(cmsData.left_section[key].title)}
                                                key={`${cmsData.left_section[key].title}_${index}`}
                                            >
                                                {cmsData.left_section[key].title}
                                            </small>
                                        );
                                    })}
                            </div>
                        </div>
                        <div className="tnc-data">
                            {cmsData &&
                                cmsData.left_section &&
                                Object.keys(cmsData.left_section).map((key, index) => (
                                    <div
                                        className="damages"
                                        key={`content_${cmsData.left_section[key].title}_${index}`}
                                        id={cmsData.left_section[key].title}
                                    >
                                        <h2>
                                            {index + 1}.{cmsData.left_section[key].title}
                                        </h2>
                                        {cmsData.left_section[key].content &&
                                            ReactHtmlParser(cmsData.left_section[key].content)}

                                        <hr className="line" />
                                    </div>
                                ))}
                        </div>
                        <div className="tnc-data1">
                            {cmsData &&
                                cmsData.left_section &&
                                Object.keys(cmsData.left_section).map((key, index) => {
                                    return (
                                        <div
                                            className="tnc-collapsible"
                                            index={`${index}_mobile_${cmsData.left_section[key].title}`}
                                        >
                                            <Collapsible
                                                trigger={
                                                    <div className="tnc-col-flex">
                                                        <div className="collapse-heading">
                                                            {cmsData.left_section[key].title}
                                                        </div>
                                                        <div className="collapse-heading">
                                                            <CgMathPlus />
                                                        </div>
                                                    </div>
                                                }
                                                triggerWhenOpen={
                                                    <div className="tnc-col-flex">
                                                        <div className="collapse-heading">
                                                            {cmsData.left_section[key].title}
                                                        </div>
                                                        <div className="collapse-heading">
                                                            <CgMathMinus />
                                                        </div>
                                                    </div>
                                                }
                                            >
                                                <div className="collapse-data">
                                                    {cmsData.left_section[key].content &&
                                                        ReactHtmlParser(cmsData.left_section[key].content)}
                                                </div>
                                            </Collapsible>
                                        </div>
                                    );
                                })}
                        </div>
                    </div>
                </div>
            )}
            {loading && <Loader />}
        </div>
    );
};

export default TermsAndConditions;

import api from '../../../api/apiServices';

const getProducts = (payload) => api.post('/index.php/rest/V1/app/productlisting/', payload);

const getPLPSearch = (payload) => api.post(`/index.php/rest/V1/app/searchresult`, payload);

let apis = {
    getProducts,
    getPLPSearch
};

export default apis
import React from 'react';
import { Hidden, Grid } from '@material-ui/core';
import SortByFilter from '../SortBy';
import './applyFilter.css';
import { IoClose } from 'react-icons/io5';

export default function ApplyFilters({ applyFilters, setApplyFilter, sortBy, setSortBy }) {

    const clearAllFilter = () => {
        setApplyFilter({});
    }

    const removeFilter = (value, code) => {
        if (applyFilters && applyFilters[code] && applyFilters[code].findIndex(x => x.value === value) !== -1) {
            const index = applyFilters[code].findIndex(x => x.value === value);
            applyFilters[code].splice(index, 1);
            setApplyFilter(applyFilters);
        }
    }

    return (
        <Grid container className="selectedFilters">
            <Grid item md={9}>
                <Hidden only={['xs', 'sm']}>
                    <Grid style={{ display: 'flex', paddingLeft: '1rem' }}>
                        <span
                            onClick={() => clearAllFilter()}
                            style={{ fontSize: '0.7rem', paddingRight: '1.4rem', cursor: "pointer" }}
                        >
                            <u>Clear All</u>
                        </span>
                        {applyFilters && Object.keys(applyFilters).map((aF) => (
                            applyFilters[aF].map((filter, index) => {
                                return (<span className="applied-filters">
                                    {filter.name} &nbsp;{' '}
                                    <IoClose className="cancel-filter" onClick={() => removeFilter(filter.value, filter.code)} />
                                </span>)
                            })
                        ))}
                    </Grid>
                </Hidden>
            </Grid>

            <Grid item md={3} container justify="space-between" alignItems="center">
                <div>
                    <Hidden only={['xs', 'sm']}>
                        <SortByFilter sortBy={sortBy} setSortBy={setSortBy} />
                    </Hidden>
                </div>
            </Grid>
        </Grid>
    )
}
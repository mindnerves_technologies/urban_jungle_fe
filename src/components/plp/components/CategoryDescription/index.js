import React from 'react';
import { Grid, Typography, Collapse } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import './categoryDescription.css';

export default function CategoryDescription({ categoryDes }) {

    const [isMore, setMore] = React.useState(false);
    
    return (
        <Grid container>
            {categoryDes && categoryDes.length > 140 &&
                    <>
                        {!isMore && <Typography className="PLPCategroyDescription-text">
                        {categoryDes.substring(0, 140)}....
                            <span onClick={() => setMore(true)}><FormattedMessage id="PDP.ReadMore" defaultMessage="read more" /></span>
                        </Typography>}
                        {isMore && <Collapse in={isMore}>
                            <Typography className="PLPCategroyDescription-text">
                                {categoryDes}&nbsp;
                                <span onClick={() => setMore(false)}><FormattedMessage id="PDP.ReadLess" defaultMessage="read less" /></span>
                            </Typography>
                        </Collapse>}
                    </>
                }
                {categoryDes && categoryDes.length <= 140 && 
                    <Typography className="PLPCategroyDescription-text">
                        {categoryDes}
                    </Typography>
                } 
        </Grid>
    )
}
import React from 'react';
import { Grid, Button } from '@material-ui/core';
import { useParams } from 'react-router-dom';
import { getUrlKey } from '../../utils';
import { useSelector } from 'react-redux';

const EmptyStatePLP = () => {
    let urlKey = useParams();
    let url_key = getUrlKey();
    const query = new URLSearchParams(window.location.search);
    const q = query.get('query');
    const state = useSelector((state) => state);
    const store_locale = state && state.storeInfo && state.storeInfo.store_code;

    return (
        <Grid className="empty-state-plp">
            {url_key === "search" && <Grid className="search-box">
                You searched for "{q}"
            </Grid>}

            <Grid className="result-message">
                <Grid>
                    0 results found for "{url_key === "search" ? q : urlKey.category}"
                </Grid>
                <a href={`/${store_locale}/home`}>
                    <Button className="continue-shopping-button">
                        Continue Shopping
                    </Button>
                </a>
            </Grid>
        </Grid>
    )
}

export default EmptyStatePLP;
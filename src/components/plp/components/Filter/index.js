import React from 'react';
import Collapsible from 'react-collapsible';
import {
    FormGroup,
    FormControlLabel,
    Checkbox,
    Hidden,
    Grid,
    Typography,
    IconButton,
} from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import { IoMdArrowDropdown, IoMdArrowDropup } from 'react-icons/io';
import './filter.css';
import MobileFilterList from './mobileFilterList';
import { IoClose } from 'react-icons/io5';

export default function PLPFilter({ filters, applyFilters, setApplyFilter, setPage, setAnchor, sortBy, setSortBy }) {


    const [isShowFilterList, setShowFilterList] = React.useState(false);
    const [selectedFilterName, setSelectedFilterName] = React.useState('');

    const clearAllFilter = () => {
        setApplyFilter({});
        setSelectedFilterName('');
    }

    const removeFilter = (value, code) => {
        if (applyFilters && applyFilters[code] && applyFilters[code].findIndex(x => x.value === value) !== -1) {
            const index = applyFilters[code].findIndex(x => x.value === value);
            applyFilters[code].splice(index, 1);
            setApplyFilter(applyFilters);
        }
    }

    const onChangeFilter = (code, value, name) => {
        let set = false;
        if (!applyFilters) {
            applyFilters[code].push({
                value: value,
                name: name,
                code: code,
            });
            set = true;
        }
        if (applyFilters && !applyFilters[code] && !set) {
            applyFilters[code] = [];
            applyFilters[code].push({
                value: value,
                name: name,
                code: code,
            });
            set = true;
        }
        if (
            applyFilters &&
            applyFilters[code] &&
            applyFilters[code].findIndex((x) => x.value === value) !== -1 &&
            !set
        ) {
            const index = applyFilters[code].findIndex((x) => x.value === value);
            applyFilters[code].splice(index, 1);
            set = true;
        }
        if (
            applyFilters &&
            applyFilters[code] &&
            applyFilters[code].findIndex((x) => x.value === value) === -1 &&
            !set
        ) {
            applyFilters[code].push({
                value: value,
                name: name,
                code: code,
            });
            set = true;
        }

        setApplyFilter(applyFilters);
        setPage(1);
    };


    const commonHtmlMobile = (filterItem, index) => {
        return (
            <Collapsible
                key={`filter_${index}`}
                open={false}
                trigger={
                    <Grid container justify="space-between" alignItems="center" className="Collapsible_text_container">
                        <div className="Collapsible_text">{filterItem}</div>
                        <div className="Collapsible_arrow_container">
                            <IoMdArrowDropdown className="Icon" />
                        </div>
                    </Grid>
                }
                triggerWhenOpen={
                    <Grid container justify="space-between" alignItems="center" className="Collapsible_text_container">
                        <div className="Collapsible_text">{filterItem}</div>
                        <div className="Collapsible_arrow_container">
                            <IoMdArrowDropup className="Icon" />
                        </div>
                    </Grid>
                }
            >
                {index !== 100 ? (
                    <div className="filterpanal scroll-plp-filter">
                        {/* <TextField
                            variant="outlined"
                            label="Search"
                            InputProps={{
                                endAdornment: (
                                    <InputAdornment>
                                        <IconButton>
                                            <GrFormSearch />
                                        </IconButton>
                                    </InputAdornment>
                                ),
                            }}
                        /> */}
                        <FormGroup row={true}>
                            {filters[filterItem] &&
                                filters[filterItem].map((valueItemx, indexV) => (
                                    <Grid item xs={6} md={12} key={`filter_value_${index}_${indexV}`}>
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    checked={
                                                        (applyFilters &&
                                                            applyFilters[valueItemx.code] &&
                                                            applyFilters[valueItemx.code].findIndex(
                                                                (x) => x.value === valueItemx.value,
                                                            ) !== -1) ||
                                                        false
                                                    }
                                                    name="gilad"
                                                    className="greeCheckbox"
                                                    onChange={() =>
                                                        onChangeFilter(
                                                            valueItemx.code,
                                                            valueItemx.value,
                                                            valueItemx.name,
                                                        )
                                                    }
                                                />
                                            }
                                            label={
                                                <Typography className="checkboxText">
                                                    {valueItemx.name}
                                                </Typography>
                                            }
                                        />
                                    </Grid>
                                ))}
                        </FormGroup>
                    </div>
                ) : (
                    <table>
                        <tr className="size-heading">
                            <th>US</th>
                            <th>EU</th>
                            <th>UK</th>
                            <th>CM</th>
                        </tr>
                        <tr className="size-data">
                            <td>1.1</td>
                            <td>2.2</td>
                            <td>3.3</td>
                            <td>4.4</td>
                        </tr>
                        <tr className="size-data">
                            <td>1.1</td>
                            <td>2.2</td>
                            <td>3.3</td>
                            <td>4.4</td>
                        </tr>
                    </table>

                    //   <div style={{margin: '1rem 1rem'}}>
                    //       <Slider
                    //           value={value}
                    //           onChange={handleChange}
                    //           valueLabelDisplay="auto"
                    //           aria-labelledby="range-slider"
                    //           getAriaValueText={valuetext}
                    //       />
                    //       F
                    //   </div>
                )}
            </Collapsible>
        );
    };

    const commonHtml = (filterItem, index) => {
        return (
            <Collapsible
                key={`filter_${index}`}
                open={true}
                trigger={
                    <Grid container justify="space-between" alignItems="center" className="Collapsible_text_container">
                        <div className="Collapsible_text">{filterItem}</div>
                        <div className="Collapsible_arrow_container">
                            <IoMdArrowDropdown className="Icon" />
                        </div>
                    </Grid>
                }
                triggerWhenOpen={
                    <Grid container justify="space-between" alignItems="center" className="Collapsible_text_container">
                        <div className="Collapsible_text">{filterItem}</div>
                        <div className="Collapsible_arrow_container">
                            <IoMdArrowDropup className="Icon" />
                        </div>
                    </Grid>
                }
            >
                {index !== 100 ? (
                    <div className="filterpanal scroll-plp-filter">
                        {/* <TextField
                            variant="outlined"
                            label="Search"
                            InputProps={{
                                endAdornment: (
                                    <InputAdornment>
                                        <IconButton>
                                            <GrFormSearch />
                                        </IconButton>
                                    </InputAdornment>
                                ),
                            }}
                        /> */}
                        <FormGroup row={true}>
                            {filters[filterItem] &&
                                filters[filterItem].map((valueItemx, indexV) => (
                                    <Grid item xs={6} md={12} key={`filter_value_${index}_${indexV}`}>
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    checked={
                                                        (applyFilters &&
                                                            applyFilters[valueItemx.code] &&
                                                            applyFilters[valueItemx.code].findIndex(
                                                                (x) => x.value === valueItemx.value,
                                                            ) !== -1) ||
                                                        false
                                                    }
                                                    name="gilad"
                                                    className="greeCheckbox"
                                                    onChange={() =>
                                                        onChangeFilter(
                                                            valueItemx.code,
                                                            valueItemx.value,
                                                            valueItemx.name,
                                                        )
                                                    }
                                                />
                                            }
                                            label={
                                                <Typography className="checkboxText">
                                                    {valueItemx.name}
                                                </Typography>
                                            }
                                        />
                                    </Grid>
                                ))}
                        </FormGroup>
                    </div>
                ) : (
                    <table>
                        <tr className="size-heading">
                            <th>US</th>
                            <th>EU</th>
                            <th>UK</th>
                            <th>CM</th>
                        </tr>
                        <tr className="size-data">
                            <td>1.1</td>
                            <td>2.2</td>
                            <td>3.3</td>
                            <td>4.4</td>
                        </tr>
                        <tr className="size-data">
                            <td>1.1</td>
                            <td>2.2</td>
                            <td>3.3</td>
                            <td>4.4</td>
                        </tr>
                    </table>

                    //   <div style={{margin: '1rem 1rem'}}>
                    //       <Slider
                    //           value={value}
                    //           onChange={handleChange}
                    //           valueLabelDisplay="auto"
                    //           aria-labelledby="range-slider"
                    //           getAriaValueText={valuetext}
                    //       />
                    //       F
                    //   </div>
                )}
            </Collapsible>
        );
    };

    return (
        <div>
            <Hidden only={['xs', 'sm']}>
                <div className="WebFilter">
                    <div className="FilterAndClear">
                        <span className="f_Brand">
                            <FormattedMessage id="PLP.Filters" defaultMessage="Filters" />
                        </span>
                    </div>
                    <div className="FilterBrands">
                        {filters && Object.keys(filters).map((filterItem, index) => commonHtml(filterItem, index))}
                    </div>
                </div>
            </Hidden>

            <Hidden only={['lg', 'xl', 'md']}>
                <div style={{ width: '100%', height: '100vh' }}>
                    <Grid item xs={12} className="FileterAndSortContainer">
                        <FormattedMessage id="PLP.Filters" defaultMessage="Filters" />
                        <span
                            className="clear-filter-button-mobile"
                            onClick={() => clearAllFilter()}
                        >
                            Clear All
                        </span>
                        <div className="close-filter-button-mobile">
                            <Grid container justify="flex-end" alignItems="center">
                                <IconButton
                                    onClick={() => {
                                        setAnchor(false);
                                        setApplyFilter({});
                                    }}
                                >
                                    <IoClose />
                                </IconButton>
                            </Grid>
                        </div>
                    </Grid>
                    {!isShowFilterList && (
                        <Grid container className="mobile-filter-container">
                            <Grid item xs={12} className="MobileApplyFilter selectedAndCloseContainer">
                                <Grid container justify="space-between" alignItems="center">
                                    <Grid item xs={12} className="ar-text-right">
                                        {applyFilters && Object.keys(applyFilters).length === 0 ? (
                                            <span className="noFilterSelected">No Filter Selected</span>
                                        ) :
                                            <Grid style={{ maxWidth: "95%", overflowY: "auto" }}>
                                                {applyFilters && Object.keys(applyFilters).map((aF) => (
                                                    applyFilters[aF].map((filter, index) => {
                                                        return (<span className="applied-filters">
                                                            {filter.name} &nbsp;{' '}
                                                            <IoClose className="cancel-filter" onClick={() => removeFilter(filter.value, filter.code)} />
                                                        </span>)
                                                    })
                                                ))}
                                            </Grid>}
                                    </Grid>

                                </Grid>
                            </Grid>

                            {/* <Grid item xs={12} className="FileterAndSortContainer">
                                <FormattedMessage id="PLP.SortBy" defaultMessage="Sort By" />
                            </Grid> */}

                            {/* <Grid item xs={12}>
                                <SortByFilter sortBy={sortBy} setSortBy={setSortBy} />
                            </Grid> */}



                            <Grid item xs={12} className="mobile_filter_text_container">
                                {filters && Object.keys(filters).map((filterItem, index) => commonHtmlMobile(filterItem, index))}

                                {/* {filters &&
                                    Object.keys(filters).map((filterItem, index) => (
                                        <Grid
                                            container
                                            key={`mobile_filter_${index}`}
                                            justify="space-between"
                                            alignItems="center"
                                            className="border-bottom"
                                            onClick={() => mobileFilterList(filterItem)}
                                        >
                                            <div>
                                                <Typography className="mobileFilter-text">{filterItem}</Typography>
                                            </div>
                                            <div>
                                                <IconButton size="small">
                                                    {language === 'en' ? (
                                                        <MdKeyboardArrowRight />
                                                    ) : (
                                                        <MdKeyboardArrowLeft />
                                                    )}
                                                </IconButton>
                                            </div>
                                        </Grid>
                                    ))} */}
                            </Grid>

                            <Grid item xs={12}>
                                <Grid container className="mobileFilterButtonConatiner" justify="space-between">
                                    <div
                                        className="mobileFilterButton mobileClearFilterButton"
                                        onClick={() => {
                                            setApplyFilter({});
                                        }}
                                    >
                                        <FormattedMessage id="PLP.ClearALL" defaultMessage="Clear All" />
                                    </div>
                                    <div
                                        className="mobileFilterButton mobileApplyFilterButton"
                                        onClick={() => setAnchor(false)}
                                    >
                                        <FormattedMessage id="PLP.ApplyFilter" defaultMessage="Apply Filter" />
                                    </div>
                                </Grid>
                            </Grid>
                        </Grid>
                    )}

                    {isShowFilterList && (
                        <Grid container className="mobile-filter-container">
                            <MobileFilterList
                                applyFilters={applyFilters}
                                filters={filters}
                                filterItem={selectedFilterName}
                                onChangeFilter={onChangeFilter}
                                setShowFilterList={setShowFilterList}
                                setAnchor={setAnchor}
                                setApplyFilter={setApplyFilter}
                            />
                        </Grid>
                    )}
                </div>
            </Hidden>
        </div>
    );
}

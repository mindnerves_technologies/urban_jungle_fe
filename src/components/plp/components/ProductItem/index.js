import React, {useState} from 'react';
import { useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Typography, Grid } from '@material-ui/core';
import { showDiscountPrice } from '../../utils';
import './productItem.css';
import { FiCopy } from 'react-icons/fi';
import CardPopup from '../../../main/home/components/productCard/CardPopup';

function ProductItem({ item, history }) {
    const state = useSelector((state) => state);
    const [isOpen, setIsOpen] = useState(false);

    const store_locale = state && state.storeInfo && state.storeInfo.store_code;
    const json = item;

    // const gotoPDP = (json) => {
    //     return history.push(`/${store_locale}/products-details/${json && json.url_key}`);
    // };


    return (
        <Grid item xs={6} md={4} lg={4} className="PLPItem">
            <a href={`/${store_locale}/${json && json.url_key}`}>
                <div className="productsItem" >

                    <img src={json.productImageUrl && json.productImageUrl.primaryimage[0]} className="p_img" alt={''} />

                    {/* <Hidden only={['sm', 'xs']}>
                        <Typography className="p_months">{getAge(item)}</Typography>
                    </Hidden> */}
                    {/* <Button className="addToBasket" style={{ marginTop: 0 }} onClick={() => gotoPDP(json)}>
                    <FormattedMessage id="Product.AddToBasket" defaultMessage="Add to basket" />
                </Button> */}
                </div>
            </a>
            <div onClick={() => setIsOpen(true)} className="cpy">
                <FiCopy />
            </div>
            <Typography className="product-title">{json.name}</Typography>
            {/* <Typography className="product-subtitle">Jordan delta mid</Typography> */}
            <Typography className="product-title">{showDiscountPrice(json)}</Typography>
            <CardPopup isOpen={isOpen} productDetail={json} setIsOpen={setIsOpen} />
        </Grid>
    );
}

export default withRouter(ProductItem);

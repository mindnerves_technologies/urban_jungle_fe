import React from 'react';
import {
    Grid,
    Hidden,
    MenuItem,
    TextField,
    FormControl,
    RadioGroup,
    FormControlLabel,
    Radio,
    Typography,
} from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import './sortBy.css';

export default function SortByFilter({ sortBy, setSortBy }) {
    return (
        <Grid container>
            <Grid item xs={12} className="sortByC ar-text-right">
                <Hidden only={['sm', 'xs']}>
                    <Grid container direction="row" alignItems="center">
                        <Typography className="sort-label">Sort By : &nbsp;&nbsp;</Typography>
                        <TextField select lable="" value={sortBy} onChange={(e) => setSortBy(e.target.value)}>
                            <MenuItem value="relevance">
                                <span className="boldText">
                                    <FormattedMessage id="PLP.New In" defaultMessage="New In" />
                                </span>
                            </MenuItem>
                            <MenuItem value="price_asc">
                                <span className="boldText">
                                    <FormattedMessage id="PLP.PriceLowtoHigh" defaultMessage="Price - Low to High" />
                                </span>
                            </MenuItem>
                            <MenuItem value="price_desc">
                                <span className="boldText">
                                    <FormattedMessage id="PLP.PriceHightoLow" defaultMessage="Price - High to Low" />
                                </span>
                            </MenuItem>
                            <MenuItem value="price_desc">
                                <span className="boldText">
                                    <FormattedMessage id="PLP.Best Selling" defaultMessage="Best Selling" />
                                </span>
                            </MenuItem>
                        </TextField>
                    </Grid>
                </Hidden>
                <Hidden only={['md', 'lg', 'xl']}>
                    <FormControl component="fieldset" className="mobileSortByFilter">
                        <RadioGroup
                            aria-label="gender"
                            name="gender1"
                            value={sortBy}
                            onChange={(e) => setSortBy(e.target.value)}
                        >
                            <FormControlLabel
                                value="relevance"
                                control={<Radio />}
                                label={
                                    <span className={sortBy === 'relevance' ? 'sortbyActiveText' : 'sortbyNormalText'}>
                                        <FormattedMessage id="PLP.New In" defaultMessage="New In" />
                                    </span>
                                }
                            />
                            <FormControlLabel
                                value="price_asc"
                                control={<Radio />}
                                label={
                                    <span className={sortBy === 'price_asc' ? 'sortbyActiveText' : 'sortbyNormalText'}>
                                        <FormattedMessage
                                            id="PLP.PriceLowtoHigh"
                                            defaultMessage="Price - Low to High"
                                        />
                                    </span>
                                }
                            />
                            <FormControlLabel
                                value="price_desc"
                                control={<Radio />}
                                label={
                                    <span className={sortBy === 'price_desc' ? 'sortbyActiveText' : 'sortbyNormalText'}>
                                        <FormattedMessage
                                            id="PLP.PriceHightoLow"
                                            defaultMessage="Price - High to Low"
                                        />
                                    </span>
                                }
                            />
                            <FormControlLabel
                                value="price_desc"
                                control={<Radio />}
                                label={
                                    <span className={sortBy === 'price_desc' ? 'sortbyActiveText' : 'sortbyNormalText'}>
                                        <FormattedMessage
                                            id="PLP.Best Selling"
                                            defaultMessage="Best Selling"
                                        />
                                    </span>
                                }
                            />
                        </RadioGroup>
                    </FormControl>
                </Hidden>
            </Grid>
        </Grid>
    );
}

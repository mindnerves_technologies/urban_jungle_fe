import React, { lazy, Suspense } from 'react';
import './css/PLP.css';
import 'bootstrap/dist/css/bootstrap.css';
import { useDispatch, useSelector } from 'react-redux';
import { actions as PLPActions } from './redux/actions';
import { Grid, Hidden, Drawer, Typography, Button } from '@material-ui/core';
import Loader from '../common/Loader/index';
import { filterData, sortByValue, getUrlKey } from './utils';
import { FormattedMessage } from 'react-intl';
import productData from './data.json';
import renderHTML from 'react-render-html';
import { IoFilterOutline } from 'react-icons/io5';
import { BiSort } from 'react-icons/bi';
import SortByFilter from './components/SortBy';
import { useParams } from 'react-router-dom';
import EmptyStatePLP from './components/EmptyState';

const ProductItem = lazy(() => import('./components/ProductItem'));
const CategoryCategory = lazy(() => import('./components/CategoryBanner'));
const ApplyFilters = lazy(() => import('./components/ApplyFilters'));
const PLPFilter = lazy(() => import('./components/Filter'));
// const SimpleBreadcrumbs = lazy(() => import('commonComponet/Breadcrumbs'));
// const CategoryDescription = lazy(() => import('./components/CategoryDescription'));

export default function PLP() {
    let url_key = getUrlKey();
    let urlKey = useParams();
    const store_id = useSelector((state) => state && state.storeInfo && state.storeInfo.store_id);

    const query = new URLSearchParams(window.location.search);
    const q = query.get('query');

    const dispatch = useDispatch();
    const state = useSelector((state) => state);

    let loading = state.plpData.loader;

    const [page, setPage] = React.useState(1);
    const [anchor, setAnchor] = React.useState(false);
    const [rowsPerPage] = React.useState(12);
    const [sortBy, setSortBy] = React.useState('relevance');
    const [clickName, setClickName] = React.useState('');
    const [applyFilters, setApplyFilter] = React.useState({});
    // const [categoryDes, setCategroyDes] = React.useState(null);
    const [productdata, setProductData] = React.useState(
        (state && state.plpData && state.plpData.products && state.plpData.products.product_data) || {},
    );

    const { filters, product_data } = productdata;

    // const handleChange = (event, value) => {
    //     setPage(value);
    // };

    const toggleDrawer = (open, name) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
        setAnchor(open);
        setClickName(name);
    };

    React.useEffect(() => {
        try {
            if (url_key !== 'search') {
                dispatch(
                    PLPActions.getAllProductRequest({
                        url_key: urlKey.category,
                        storeId: store_id,
                    }),
                );
            } else {
                dispatch(
                    PLPActions.getSearchPLPProductRequest({
                        q: q,
                        storeId: store_id,
                    }),
                );
            }
            setApplyFilter({});
        } catch (err) {}
    }, [url_key, urlKey, dispatch, q, store_id]);

    React.useEffect(() => {
        try {
            setProductData(state.plpData.products);
        } catch (err) {}
    }, [state.plpData]);

    const global = state && state.auth;

    let productList = product_data && product_data.filter((item) => item.stock !== 0);
    productList = filterData(productList, applyFilters);

    productList = sortByValue(productList, sortBy, product_data);

    //BreadCrumbs
    const breadCrumbs = [
        {
            name: <FormattedMessage id="Common.Home.Text" defaultMessage="Home" />,
            url: `/${global && global.store_locale}/`,
        },
        {
            name: (productData.data && productData.data.category_name) || q,
        },
    ];

    if (productData && productData.data && productData.data.breadcrumb && productData.data.breadcrumb.url_key !== '') {
        breadCrumbs.splice(1, 0, {
            name:
                productData &&
                productData.data.category_path &&
                productData.data.category_path.split('/') &&
                productData.data.category_path.split('/')[0],
            url: `/${global && global.store_locale}/products/${productData.data.breadcrumb.url_key}`,
        });
    }
    //End

    //Category Description html convert to string
    // React.useEffect(() => {
    //     try {
    //         if (products.category_description2) {
    //             setCategroyDes(parse(products.category_description2).props.children);
    //         }
    //     } catch (err) {}
    // }, [products.category_description2]);

    if(loading){
        return <Loader />
    }
    return (
        <Suspense fallback={<Loader />}>
            <Hidden only={['xs', 'sm']}>
                { url_key !== 'search' && (
                    <div className="apparels">
                        <div className="apparel-cnt1">
                            <div style={{ width: 'inherit' }}>
                                <Typography className="apparel-title">{productdata.category_name}</Typography>
                            </div>
                            <div className="apparels-list">
                                {['T-Shirts', 'SwearShirts', 'Jackets', 'TrackSuits', 'Shorts'].map(
                                    (apparel, index) => {
                                        return <span key={index}>{apparel}</span>;
                                    },
                                )}
                            </div>
                            <div>
                                {productdata.category_description2 && renderHTML(productdata.category_description2)}
                            </div>
                        </div>
                        <div className="apparel-cnt2">
                            <img src={productdata.category_image} alt="category-banner" />
                        </div>
                    </div>
                )}
            </Hidden>
            <Hidden only={['lg', 'md', 'xl']}>
                {url_key !== 'search' && (
                    <div className="apparels">
                        <div className="">
                            <img src={productdata.category_image} alt="category-banner" />
                        </div>
                        <div>{productdata.category_description2 && renderHTML(productdata.category_description2)}</div>
                    </div>
                )}
            </Hidden>
            <div style={{ marginTop: '10px' }}>
                <React.Fragment key={'top'}>
                    <Drawer anchor={'bottom'} open={anchor} onClose={toggleDrawer(false, false)} className="filter-pop">
                        {clickName === 'filter' && (
                            <PLPFilter
                                filters={productData && productData.data && productData.data.filters}
                                setApplyFilter={setApplyFilter}
                                applyFilters={{ ...applyFilters }}
                                setPage={setPage}
                                setAnchor={setAnchor}
                                sortBy={sortBy}
                                setSortBy={setSortBy}
                            />
                        )}
                        {clickName === 'sort' && (
                            <Grid>
                                <Grid item xs={12} className="FileterAndSortContainer">
                                    <FormattedMessage id="PLP.SortBy" defaultMessage="Sort By" />
                                </Grid>

                                <Grid item xs={12}>
                                    <SortByFilter sortBy={sortBy} setSortBy={setSortBy} />
                                </Grid>
                            </Grid>
                        )}
                    </Drawer>
                </React.Fragment>

                <Grid container justify="center" className="mainPlp">
                    <Grid item xs={11}>
                        {productData && product_data && product_data.length > 0 ? (
                            <>
                                {/** Breadcrumb */}
                                <Grid container justify="center">
                                    <Grid item xs={11} md={12}>
                                        {/* <SimpleBreadcrumbs breadCrumbs={breadCrumbs} /> */}
                                    </Grid>
                                </Grid>
                                {/** End Breadcrumb */}

                                <Grid container>
                                    {/** Web Filter */}
                                    <Hidden only={['xs', 'sm']}>
                                        <Grid item md={3}>
                                            <PLPFilter
                                                filters={productData && productData.data && filters}
                                                setApplyFilter={setApplyFilter}
                                                applyFilters={{ ...applyFilters }}
                                                setPage={setPage}
                                                setAnchor={setAnchor}
                                            />
                                        </Grid>
                                    </Hidden>
                                    {/** End Web Filter */}

                                    {/** Mobile Filter */}
                                    <Hidden only={['lg', 'xl', 'md']}>
                                        <Grid container>
                                            <Grid item md={6} xs={6}>
                                                <IoFilterOutline onClick={toggleDrawer(true, 'filter')} />
                                            </Grid>
                                            <Grid item md={6} xs={6} style={{ textAlign: 'right' }}>
                                                <BiSort onClick={toggleDrawer(true, 'sort')} />
                                            </Grid>
                                        </Grid>
                                    </Hidden>
                                    {/** End Mobile Filter */}

                                    <Grid item xs={12} md={9} className="PLP">
                                        <Grid container justify="center">
                                            {/** Category Image */}
                                            <Grid item xs={11}>
                                                {productData &&
                                                    productData.data.category_Image &&
                                                    productData.data.category_Image !== '' && (
                                                        <CategoryCategory
                                                            categoryImage={productData.data.category_Image}
                                                        />
                                                    )}
                                            </Grid>
                                            {/** End Category Image */}

                                            {/** Product Name */}
                                            {/* <Grid item xs={11} className="ProductName">
                                                <Typography>{productData && productData.data && productData.data.category_name}</Typography>
                                            </Grid> */}
                                            {/** End Product Name */}

                                            {/** Category Desc */}
                                            <Grid item xs={11}>
                                                {/* {categoryDes && <CategoryDescription categoryDes={categoryDes} />} */}
                                            </Grid>
                                            {/** end Category Desc */}

                                            {/** Apply filters  */}
                                            <Grid item xs={12} className="">
                                                <Grid container justify="space-between" alignItems="center">
                                                    <ApplyFilters
                                                        applyFilters={{ ...applyFilters }}
                                                        setApplyFilter={setApplyFilter}
                                                        sortBy={sortBy}
                                                        setSortBy={setSortBy}
                                                    />
                                                </Grid>
                                            </Grid>
                                            {/** Apply filters */}

                                            {/** Product List */}
                                            <Grid item xs={12} md={12}>
                                                <Grid container>
                                                    {productList &&
                                                        Object.keys(productList)
                                                            .splice(0, page * rowsPerPage)
                                                            .map((item, index) => {
                                                                return (
                                                                    <ProductItem
                                                                        key={`PLP_${index}`}
                                                                        item={productList[item]}
                                                                    />
                                                                );
                                                            })}
                                                </Grid>
                                            </Grid>
                                            {/** End Product List */}
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </>
                        ) : <EmptyStatePLP />}

                        {/** Spinner */}
                        {state && state.PLP && state.PLP.loader && (
                            <Grid container justify="center" alignItems="center">
                                <Loader />
                            </Grid>
                        )}
                        {(page * rowsPerPage) < productList.length && <Grid className="load-more-container">
                            <Button onClick={() => setPage(page + 1)} className="load-more-button">Load More</Button>
                        </Grid>}
                        {/** End Spinner */}
                    </Grid>
                </Grid>
            </div>
        </Suspense>
    );
}

import { createAction } from 'redux-actions';

//Actions types
const GET_ALL_PRODUCTS_REQUEST = 'ELC@OROB/GET_ALL_PRODUCTS_REQUEST';
const GET_ALL_PRODUCTS_REQUEST_SUCCESS = 'ELC@OROB/GET_ALL_PRODUCTS_REQUEST_SUCCESS';
const GET_ALL_PRODUCTS_REQUEST_FAILED = 'ELC@OROB/GET_ALL_PRODUCTS_REQUEST_FAILED';

const GET_SEARCH_PLP_PRODUCT_REQUEST = 'ELC@OROB/GET_SEARCH_PLP_PRODUCT_REQUEST';

//actions methods
const getAllProductRequest = createAction(GET_ALL_PRODUCTS_REQUEST);
const getAllProductRequestSuccess = createAction(GET_ALL_PRODUCTS_REQUEST_SUCCESS);
const getAllProductRequestFailed = createAction(GET_ALL_PRODUCTS_REQUEST_FAILED);

const getSearchPLPProductRequest = createAction(GET_SEARCH_PLP_PRODUCT_REQUEST);

export const actions = {
    getAllProductRequest,
    getSearchPLPProductRequest,

    getAllProductRequestSuccess,
    getAllProductRequestFailed
};

export const types = {
    GET_ALL_PRODUCTS_REQUEST,
    GET_SEARCH_PLP_PRODUCT_REQUEST,
    
    GET_ALL_PRODUCTS_REQUEST_SUCCESS,
    GET_ALL_PRODUCTS_REQUEST_FAILED
};
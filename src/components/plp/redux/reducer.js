import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionsHandler = {
    [types.GET_ALL_PRODUCTS_REQUEST]: state => ({
        ...state,
        loader: true
    }),
    [types.GET_SEARCH_PLP_PRODUCT_REQUEST]: state => ({
        ...state,
        loader: true
    }),

    [types.GET_ALL_PRODUCTS_REQUEST_FAILED]: state => ({
        ...state,
        loader: false,
        products: {}
    }),
    [types.GET_ALL_PRODUCTS_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        loader: false,
        products: (payload) || {}
    })
};

export default handleActions(actionsHandler, {
    loader: false,
    products: {}
});
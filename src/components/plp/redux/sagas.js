import { call, put, takeLatest } from 'redux-saga/effects';
import { actions, types } from './actions';
import api from '../api';

const getAllProductsReq = function* getAllProductsReq({ payload }) {
    try {
        const { url_key, storeId } = payload;
        const data = yield call(api.getProducts, {
            "url_key": url_key,
            "storeid": storeId         
        });
        if(data && data.status){
            yield put(actions.getAllProductRequestSuccess(data.data.data));
        } else {
            yield put(actions.getAllProductRequestFailed())
        }
    } catch (err) {
        yield put(actions.getAllProductRequestFailed())
    }
}

const getSearchProductReq = function* getSearchProductReq({ payload }) {
    try {
        const { q, storeId } = payload;
        const data = yield call(api.getPLPSearch, {
            "q": q,
            "storeid": storeId         
        });
        if(data && data.status){
            yield put(actions.getAllProductRequestSuccess(data.data.data));
        } else {
            yield put(actions.getAllProductRequestFailed())
        }
    } catch (err) {
        yield put(actions.getAllProductRequestFailed())
    }
}

export default function* sagas() {
    yield takeLatest(types.GET_ALL_PRODUCTS_REQUEST, getAllProductsReq)
    yield takeLatest(types.GET_SEARCH_PLP_PRODUCT_REQUEST, getSearchProductReq)
}
import React from 'react';
let _ = require('lodash');

export const getAge = (data) => {
    if (data && data.json && data.json.filtersdata && data.json.filtersdata.age) {
        const age = data && data.json && data.json.filtersdata && data.json.filtersdata.age;
        for (let data in age) {
            return age[data]
        }
    }
    return "";
}

export const showDiscountPrice = (data) => {
    // const offerData = data && data.json && data.json.offers && data.json.offers.data;
    // const offerstatus = data && data.json && data.json.offers && data.json.offers.status;
    // if (offerstatus === 1) {
    //     if (Object.keys(offerData).length === 1) {
    //         for (let value in offerData) {
    //             if (value === '1') {
    //                 return (<><span>{`${data.currency} ${offerData[value]}`}</span>
    //                     <span className="amt_strike">{`${data.currency} ${data && data.json && data.json.price}`}</span></>)
    //             } else {
    //                 return <span>{`${data.currency} ${data && data.json && data.json.price}`}</span>
    //             }
    //         }
    //     } else {
    //         return <span>{`${data.currency} ${data && data.json && data.json.price}`}</span>
    //     }
    // } else {
    //     return <span>{`${data.currency} ${data && data.json && data.json.price}`}</span>
    // }

    if(data && data.price && data.price.specialPrice){
        return <span>{`${data.currency} ${data && data.price && data.price.specialPrice}`}</span>
    }
    else{
        return <span>{`${data.currency} ${data && data.price && data.price.price}`}</span>
    }
}

export const filterData = (products,filters) => {
    const filterKeys = Object.keys(filters);
    products = _.values(products);
    return products.filter(product => {
        if (!filterKeys.length) return true;
        return filterKeys.every(key => {
            if (!filters[key].length) return true;
            return filters[key].some((item) => {
                
                return product.filtersdata[key] && Object.keys(product.filtersdata[key]).some((productfilterdata, index) => {
                    let filterarray = [];
                    filterarray = productfilterdata.split(',');
                    if (filterarray.length > 0) {
                        if (filterarray.indexOf(item.value) !== -1) {
                            return true;
                        }
                    }
                    return false;
                });
                
            });
        });
    });
}

export const sortByValue = (products, value, original) => {
    let sortedData = {}
    if (value === "price_desc") {
        sortedData = _.values(products).sort((a, b) => b.price.price - a.price.price);
    } else if (value === 'price_asc') {
        sortedData = _.values(products).sort((a, b) => a.price.price - b.price.price);
    } else if (value === 'relevance') {
        sortedData = products;
    } else if (value === 'a-z') {
        sortedData = _.values(products).sort((a, b) => a.json.name.localeCompare(b.json.name));
    } else if (value === 'z-a') {
        sortedData = _.values(products).sort((a, b) => b.json.name.localeCompare(a.json.name));
    }

    return sortedData;
}

export const getUrlKey = () => {
    const pathArray = window.location.pathname.includes('/search');
    
    return pathArray ? "search" : "PLP"
}
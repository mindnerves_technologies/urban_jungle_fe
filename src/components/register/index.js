import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import { Button, Radio, Typography, TextField, FormHelperText, CircularProgress, Checkbox } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { isEmail } from '../login/utils';
import { Link, useHistory } from 'react-router-dom';
import { actions as RegisterActions } from './redux/actions';
import { FormattedMessage } from 'react-intl';
import SimpleBreadcrumbs from '../common/Breadcrumbs';

const Register = () => {
    const dispatch = useDispatch();
    const history = useHistory();

    const loading = useSelector((state) => state.register && state.register.loading);
    const store_code = useSelector((state) => state && state.storeInfo && state.storeInfo.store_code);

    const [data, setData] = useState({
        firstName: undefined,
        lastName: undefined,
        email: undefined,
        password: undefined,
        confirmpassword: undefined,
    });
    const [isValidate, setIsValidate] = useState(false);

    const [agree, setAgree] = useState(false);
    const [processPolicy, setProcessPolicy] = useState(false);
    const [newsletter, setNewsletter] = useState(false);

    const onChangeInput = (name, event) => {
        setData({ ...data, [name]: event.target.value });
    };

    const redirectToLogin = () => {
        history.push(`/${store_code}/sign-in`);
    };

    const onClickRegister = () => {
        setIsValidate(true);
        const { firstName, lastName, email, password, confirmpassword } = data;
        if (
            !firstName ||
            firstName === '' ||
            !lastName ||
            lastName === '' ||
            !isEmail(email) ||
            !password ||
            password === '' ||
            password.length < 6 ||
            !confirmpassword ||
            confirmpassword === '' ||
            password !== confirmpassword ||
            !firstName.match(/^[a-zA-Z]+$/) ||
            !lastName.match(/^[a-zA-Z]+$/)
        ) {
            return;
        }
        setIsValidate(false);
        dispatch(
            RegisterActions.registerUserRequest({
                firstname: firstName,
                lastname: lastName,
                email: email,
                password: password,
                confirmpassword: confirmpassword,
                store_id: 1,
                quest_quote: '',
                subscribe_to_newsletter: newsletter ? 1 : 0,
            }),
        );
    };

    const breadCrumbs = [
        {
            name: <FormattedMessage id="Common.Home.Text" defaultMessage="Home" />,
            url: `/${store_code}`,
        },

        {
            name: 'Register',
        },
    ];

    return (
        <Grid container justify="center">
            <Grid item xs={11}>
                <div className="signin">
                    <SimpleBreadcrumbs breadCrumbs={breadCrumbs} />
                    <Grid container direction="row" justify="space-between" alignItems="center" className="mainTitle">
                        <Grid item xs={12} md={12}>
                            <Typography>Register</Typography>
                        </Grid>
                    </Grid>

                    <Grid container direction="row" justify="center" alignItems="center">
                        <Grid item xs={12} md={4} className="signinCode">
                            <div className="titleGroup" style={{ borderBottom: 'none' }}>
                                <div>
                                    <Radio
                                        checked={false}
                                        value="a"
                                        name="radio-button-demo"
                                        inputProps={{ 'aria-label': 'A' }}
                                        onClick={() => redirectToLogin()}
                                    />
                                    <Grid container>
                                        <Typography className="title">Sign In</Typography>
                                        <Grid item xs={12}>
                                            <Typography className="info">Checkout faster with saved details</Typography>
                                        </Grid>
                                    </Grid>
                                </div>
                            </div>
                        </Grid>
                    </Grid>

                    <Grid container direction="row" justify="center" alignItems="center">
                        <Grid item xs={12} md={4} className="signinCode">
                            <div className="titleGroup">
                                <div>
                                    <Radio
                                        checked={true}
                                        value="a"
                                        name="radio-button-demo"
                                        inputProps={{ 'aria-label': 'A' }}
                                    />
                                    <Grid container>
                                        <Typography className="title">Register</Typography>
                                        <Grid item xs={12}>
                                            <Typography className="info">
                                                Register with us to track and manage your orders
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </div>
                            </div>
                            <div className="descGroup">
                                <Grid container spacing={2}>
                                    <Grid item xs={6}>
                                        <Typography className="titleInput">First Name*</Typography>
                                        <TextField
                                            variant="outlined"
                                            value={data.firstName || ''}
                                            onChange={(e) => {
                                                onChangeInput('firstName', e);
                                            }}
                                            error={isValidate && (!data.firstName || data.firstName === '')}
                                        />
                                        {isValidate && (!data.firstName || data.firstName === '') && (
                                            <FormHelperText className="error_message">
                                                {' '}
                                                Please enter first name
                                            </FormHelperText>
                                        )}
                                        {isValidate &&
                                            data.firstName &&
                                            data.firstName.trim() !== '' &&
                                            !data.firstName.match(/^[a-zA-Z]+$/) && (
                                                <FormHelperText className="error_message">
                                                    {' '}
                                                    Please enter valid first name
                                                </FormHelperText>
                                            )}
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Typography className="titleInput">Last Name*</Typography>
                                        <TextField
                                            variant="outlined"
                                            value={data.lastName || ''}
                                            onChange={(e) => {
                                                onChangeInput('lastName', e);
                                            }}
                                            error={isValidate && (!data.lastName || data.lastName === '')}
                                        />
                                        {isValidate && (!data.lastName || data.lastName.trim() === '') && (
                                            <FormHelperText className="error_message">
                                                {' '}
                                                Please enter last name
                                            </FormHelperText>
                                        )}
                                        {isValidate &&
                                            data.lastName &&
                                            data.lastName.trim() !== '' &&
                                            !data.lastName.match(/^[a-zA-Z]+$/) && (
                                                <FormHelperText className="error_message">
                                                    {' '}
                                                    Please enter valid last name
                                                </FormHelperText>
                                            )}
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Typography className="titleInput">Email*</Typography>
                                        <TextField
                                            variant="outlined"
                                            value={data.email}
                                            onChange={(e) => onChangeInput('email', e)}
                                            error={isValidate && !isEmail(data.email)}
                                        />
                                        {isValidate && !isEmail(data.email) && (
                                            <FormHelperText className="error_message">
                                                {' '}
                                                Please enter valid email
                                            </FormHelperText>
                                        )}
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Typography className="titleInput">Password*</Typography>
                                        <TextField
                                            variant="outlined"
                                            type="password"
                                            value={data.password}
                                            onChange={(e) => onChangeInput('password', e)}
                                            error={isValidate && (!data.password || data.password === '')}
                                        />
                                        {isValidate && (!data.password || data.password === '') && (
                                            <FormHelperText className="error_message">
                                                Please enter password
                                            </FormHelperText>
                                        )}
                                        {isValidate && data.password && data.password.length < 6 && (
                                            <FormHelperText className="error_message">
                                                Please enter password at least 6 characters
                                            </FormHelperText>
                                        )}
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Typography className="titleInput">Confirm Password*</Typography>
                                        <TextField
                                            variant="outlined"
                                            type="password"
                                            value={data.confirmpassword}
                                            onChange={(e) => onChangeInput('confirmpassword', e)}
                                            error={
                                                isValidate &&
                                                (!data.confirmpassword ||
                                                    data.confirmpassword === '' ||
                                                    data.password !== data.confirmpassword)
                                            }
                                        />
                                        {isValidate && (!data.confirmpassword || data.confirmpassword === '') && (
                                            <FormHelperText className="error_message">
                                                Please enter confirm password
                                            </FormHelperText>
                                        )}
                                        {isValidate &&
                                            data.confirmpassword &&
                                            data.confirmpassword !== '' &&
                                            data.password !== data.confirmpassword && (
                                                <FormHelperText className="error_message">
                                                    Password and confirm password not same
                                                </FormHelperText>
                                            )}
                                    </Grid>

                                    <Grid container alignItems="center" className="register_checkbox">
                                        <Grid item xs={1}>
                                            <Checkbox checked={agree} onChange={(e) => setAgree(e.target.checked)} />
                                        </Grid>
                                        <Grid item xs={11}>
                                            <label>
                                                I agree to the&nbsp;
                                                <Link to={`/${store_code}/privacy-policy`}>Privacy Policy</Link>
                                            </label>
                                        </Grid>
                                    </Grid>

                                    <Grid container alignItems="center" className="register_checkbox">
                                        <Grid item xs={1}>
                                            <Checkbox
                                                checked={processPolicy}
                                                onChange={(e) => setProcessPolicy(e.target.checked)}
                                            />
                                        </Grid>
                                        <Grid item xs={11}>
                                            <label>
                                                Consent to the processing of personal data for marketing purposes&nbsp;
                                                <Link to={`/${store_code}/privacy-policy`}>Privacy Policy</Link>
                                            </label>
                                        </Grid>
                                    </Grid>

                                    <Grid container alignItems="center" className="register_checkbox">
                                        <Grid item xs={1}>
                                            <Checkbox
                                                checked={newsletter}
                                                onChange={(e) => setNewsletter(e.target.checked)}
                                            />
                                        </Grid>
                                        <Grid item xs={11}>
                                            <label>Sign Up for Newsletter</label>
                                        </Grid>
                                    </Grid>

                                    <Button
                                        className="signin-button"
                                        variant="contained"
                                        onClick={() => {
                                            if (!loading) {
                                                onClickRegister();
                                            }
                                        }}
                                        startIcon={loading ? <CircularProgress size={30} /> : null}
                                        disabled={!agree}
                                    >
                                        {!loading && `Register`}
                                    </Button>
                                </Grid>
                            </div>
                        </Grid>
                    </Grid>
                </div>
            </Grid>
        </Grid>
    );
};

export default Register;

import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandlers = {
    [types.REGISTER_USER_REQUEST]: (state) => ({
        ...state,
        loading: true,
    }),
    [types.REGISTER_USER_REQUEST_FAILED]: (state) => ({
        ...state,
        loading: false,
    }),
    [types.REGISTER_USER_REQUEST_SUCCESS]: (state) => ({
        ...state,
        loading: false,
    }),
};

export default handleActions(actionHandlers, {
    loading: false,
});

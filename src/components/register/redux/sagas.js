import { call, put, takeLatest } from 'redux-saga/effects';
import APIList from '../../../api';
import { actions, types } from './actions';
import { toast } from 'react-toastify';
import { push } from 'connected-react-router';

const registerUserReq = function* registerUserReq({ payload }) {
    try {
        const { data } = yield call(APIList.registerUser, payload);
        if (data.status) {
            toast.success(`User registered successfully!`);
            yield put(push(`/en/home`));
            yield put(actions.registerUserRequestSuccess());
        } else {
            toast.error(data.message || 'Something wrong, Please try after some time');
            yield put(actions.registerUserRequestFailed());
        }
    } catch (err) {
        yield put(actions.registerUserRequestFailed());
        toast.error(
            (err.response && err.response.data && err.response.data.error) ||
                'Something wrong, Please try after some time',
        );
    }
};

export default function* sagas() {
    yield takeLatest(types.REGISTER_USER_REQUEST, registerUserReq);
}

import { createAction } from 'redux-actions';

const GET_STORE_INFO_REQUEST = 'UJ/GET_STORE_INFO_REQUEST';
const GET_STORE_INFO_REQUEST_SUCCESS = 'UJ/SET_STORE_INFO_REQUEST_SUCCESS';
const GET_STORE_INFO_REQUEST_FAILED = 'UJ/GET_STORE_INFO_REQUEST_FAILED';

const GET_GUEST_CART_REQUEST = 'UJ/GET_GUEST_CART_REQUEST';
const GET_GUEST_CART_REQUEST_SUCCESS = 'UJ/GET_GUEST_CART_REQUEST_SUCCESS';
const GET_GUEST_CART_REQUEST_FAILED = 'UJ/GET_GUEST_CART_REQUEST_FAILED';

const SET_LOGIN_USER_DETAILS = 'UJ/SET_LOGIN_USER_DETAILS';

const getStoreInfoRequest = createAction(GET_STORE_INFO_REQUEST);
const getStoreInfoRequestSuccess = createAction(GET_STORE_INFO_REQUEST_SUCCESS);
const getStoreInfoRequestFailed = createAction(GET_STORE_INFO_REQUEST_FAILED);

const getGuestCartRequest = createAction(GET_GUEST_CART_REQUEST);
const getGuestCartRequestSuccess = createAction(GET_GUEST_CART_REQUEST_SUCCESS);
const getGuestCartRequestFailed = createAction(GET_GUEST_CART_REQUEST_FAILED);

const setLoginUserDetails = createAction(SET_LOGIN_USER_DETAILS);

export const actions = {
    getStoreInfoRequest,
    getStoreInfoRequestSuccess,
    getStoreInfoRequestFailed,

    getGuestCartRequest,
    getGuestCartRequestSuccess,
    getGuestCartRequestFailed,

    setLoginUserDetails,
};

export const types = {
    GET_STORE_INFO_REQUEST,
    GET_STORE_INFO_REQUEST_SUCCESS,
    GET_STORE_INFO_REQUEST_FAILED,

    GET_GUEST_CART_REQUEST,
    GET_GUEST_CART_REQUEST_SUCCESS,
    GET_GUEST_CART_REQUEST_FAILED,

    SET_LOGIN_USER_DETAILS,
};

import { handleActions } from 'redux-actions';
import { types } from './actions';

const actionHandlers = {
    [types.GET_STORE_INFO_REQUEST]: (state) => ({
        ...state,
        loading: true,
    }),
    [types.GET_STORE_INFO_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        loading: false,
    }),
    [types.GET_STORE_INFO_REQUEST_FAILED]: (state, { payload }) => ({
        ...state,
        loading: false,
    }),
    [types.GET_GUEST_CART_REQUEST_SUCCESS]: (state, { payload }) => ({
        ...state,
        guest_user: {
            guestId: payload || null,
        },
    }),
    [types.GET_GUEST_CART_REQUEST_FAILED]: (state) => ({
        ...state,
        guest_user: {
            guestId: null,
        },
    }),
    [types.SET_LOGIN_USER_DETAILS]: (state, { payload }) => ({
        ...state,
        loginUser: payload || null,
        guest_user: {
            guestId: null,
        },
    }),
};

export default handleActions(actionHandlers, {
    loading: false,
    store_id: 1,
    language: 'en',
    store_code: 'en',
    country: 'it',
    customer: null,
    guest_user: null,
    loginUser: null,
});

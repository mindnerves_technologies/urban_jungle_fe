import { call, put, takeLatest } from 'redux-saga/effects';
import { actions, types } from './actions';
import api from '../../../api';

const guestCartReq = function* guestCartReq({ payload }) {
    try {
        const { data } = yield call(api.getGuestCard, payload);
        yield put(actions.getGuestCartRequestSuccess(data));
    } catch (err) {
        yield put(actions.getGuestCartRequestFailed());
    }
};

export default function* sagas() {
    yield takeLatest(types.GET_GUEST_CART_REQUEST, guestCartReq);
}

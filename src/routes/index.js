import React, { useEffect } from 'react';
import { IntlProvider, addLocaleData } from 'react-intl';
import en from 'react-intl/locale-data/en';
import it from 'react-intl/locale-data/it';
import messages_it from '../translations/it.json';
import messages_en from '../translations/en.json';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Route } from 'react-router-dom';
import { actions as staticActions } from '../components/main/static/redux/actions';

import HomePage from '../components/main/home';
import Login from '../components/login';
import NavBar from '../components/common/NavigationBar';
import Footer from '../components/common/Footer';
import ContactUs from '../components/main/static/contactUs';
import FAQ from '../components/main/static/faq';
import Register from '../components/register';
import Middleware from '../components/main/MiddleWare';
import TermsAndConditions from '../components/main/static/termsAndConditions';
import ShippingAndReturn from '../components/main/static/shippingAndReturn';
import CookiePolicy from '../components/main/static/cookiePolicy';
import { useDispatch, useSelector } from 'react-redux';
import Privacy from '../components/main/static/privacy';
import AboutUs from '../components/main/static/aboutUs';
import NotFoundPage from '../components/main/notFound';
import ScrollToTop from '../components/common/ScrollToTop';
import Basket from '../components/main/cart/Basket';
import PLP from '../components/plp/index';
import { actions as storeInfoAction } from '../components/storeInfo/redux/actions';
import Brands from 'components/brands';

const AppRouters = () => {
    const dispatch = useDispatch();

    const store_id = useSelector((state) => state && state.storeInfo && state.storeInfo.store_id);
    const guest_user = useSelector((state) => state && state.storeInfo && state.storeInfo.guest_user);
    const loginUser = useSelector((state) => state && state.storeInfo && state.storeInfo.loginUser);

    const language = 'en';

    addLocaleData([...en, ...it]);
    const messages = {
        it: messages_it,
        en: messages_en,
    };

    useEffect(() => {
        dispatch(
            staticActions.getContactUsData({
                store_id: store_id,
            }),
        );
    }, [store_id, dispatch]);

    // guest cart
    useEffect(() => {
        try {
            if (!loginUser) {
                if (!guest_user || !guest_user.guestId || guest_user.guestId === '') {
                    dispatch(storeInfoAction.getGuestCartRequest({}));
                }
            }
        } catch (err) {}
    }, [dispatch, loginUser, guest_user]);

    return (
        <div>
            <IntlProvider
                locale={language}
                messages={messages[language]}
                onError={(err) => {
                    if (err && err.code === 'MISSING_TRANSLATION') {
                        console.warn('Missing translation', err && err.message);
                        return;
                    }
                }}
            >
                <>
                    <ToastContainer position="top-right" autoClose={5000} hideProgressBar={false} />
                    <NavBar />
                    <div>
                        <ScrollToTop>
                            {/** Home */}
                            <Route path="/home" component={HomePage} />
                            <Route exact path="/" component={HomePage} />
                            <Route exact path="/:locale" component={HomePage} />
                            <Route path="/:locale(it|en|uk|mt|int_en)/:category" component={Middleware} />

                            <Route exact path="/:locale(it|en|uk|mt|int_en)/sign-in" component={Login} />
                            <Route exact path="/:locale(it|en|uk|mt|int_en)/sign-up" component={Register} />

                            {/**Static Pages */}
                            <Route exact path="/:locale(it|en|uk|mt|int_en)/contact-us" component={ContactUs} />
                            <Route exact path="/:locale(it|en|uk|mt|int_en)/faq" component={FAQ} />
                            <Route
                                exact
                                path="/:locale(it|en|uk|mt|int_en)/terms-and-conditions"
                                component={TermsAndConditions}
                            />
                            <Route
                                exact
                                path="/:locale(it|en|uk|mt|int_en)/shipping-and-return"
                                component={ShippingAndReturn}
                            />
                            <Route exact path="/:locale(it|en|uk|mt|int_en)/cookie-policy" component={CookiePolicy} />
                            <Route exact path="/:locale(it|en|uk|mt|int_en)/privacy" component={Privacy} />
                            <Route exact path="/:locale(it|en|uk|mt|int_en)/about-us" component={AboutUs} />
                            {/** End Static Pages */}

                            <Route exact path="/:locale(it|en|uk|mt|int_en)/not-found" component={NotFoundPage} />
                            {/* Cart */}
                            <Route exact path="/:locale(it|en|uk|mt|int_en)/cart" component={Basket} />
                            {/* End Cart */}
                            <Route path="/:locale(it|en|uk|mt|it-en)/search" component={PLP} />

                            {/* Brands */}
                            <Route exact path="/:locale(it|en|uk|mt|it-en)/brands" component={Brands} />

                        </ScrollToTop>
                    </div>
                    <Footer />
                </>
            </IntlProvider>
        </div>
    );
};

export default AppRouters;

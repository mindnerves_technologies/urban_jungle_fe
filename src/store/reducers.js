import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import menuReducer from '../components/common/NavigationBar/redux/reducer';
import homepageReducer from '../components/main/home/redux/reducer';
import loginReducer from '../components/login/redux/reducer';
import registerReducer from '../components/register/redux/reducer';
import storeInfoReducer from '../components/storeInfo/redux/reducer';
import staticReducer from '../components/main/static/redux/reducer';
import PDPReducer from '../components/main/pdp/redux/reducer';
import cartReducer from '../components/main/cart/Basket/redux/reducer';
import plpReducer from '../components/plp/redux/reducer';
import brandReducer from '../components/brands/redux/reducer';

const createRootReducer = (history) =>
    combineReducers({
        router: connectRouter(history),
        menu: menuReducer,
        home: homepageReducer,
        login: loginReducer,
        storeInfo: storeInfoReducer,
        staticData: staticReducer,
        PDP: PDPReducer,
        plpData: plpReducer,
        register: registerReducer,
        cart: cartReducer,
        brand: brandReducer,
    });

export default createRootReducer;

import { select, takeEvery, all, fork } from 'redux-saga/effects';
import menuSagas from '../components/common/NavigationBar/redux/sagas';
import homepageSagas from '../components/main/home/redux/sagas';
import loginSagas from '../components/login/redux/sagas';
import registerSagas from '../components/register/redux/sagas';
import staticSagas from '../components/main/static/redux/sagas';
import PDPSagas from '../components/main/pdp/redux/sagas';
import StoreInfoSagas from '../components/storeInfo/redux/sagas';
import plpSagas from '../components/plp/redux/sagas';
import cartSagas from '../components/main/cart/Basket/redux/sagas';
import brandSaga from '../components/brands/redux/sagas';

function* watchAndLog() {
    yield takeEvery('*', function* logger(action) {
        const state = yield select();
        console.debug('action', action);
        console.debug('state after', state);
    });
}

export default function* root() {
    yield all([
        fork(watchAndLog),
        fork(menuSagas),
        fork(homepageSagas),
        fork(loginSagas),
        fork(staticSagas),
        fork(PDPSagas),
        fork(StoreInfoSagas),
        fork(plpSagas),
        fork(registerSagas),
        fork(cartSagas),
        fork(brandSaga),
    ]);
}
